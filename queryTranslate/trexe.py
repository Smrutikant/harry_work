from google.cloud import bigquery
from google.oauth2 import service_account



def main():
    client = bigquery.Client()

    table_name = 'metrocodes'
    limit = 700

    # query_params = [bigquery.ScalarQueryParameter('t_b', 'STRING', table_name)]
    query_params = [bigquery.ScalarQueryParameter('lmt', 'INT64', limit)]

    query = """
    create table harry_dataset.mlk as
    SELECT *
    FROM opinmind_prod.metrocodes
    LIMIT 10
    """

    # query = """
    # SELECT *
    # FROM opinmind_prod.metrocodes
    # LIMIT @lmt
    # """

    job_config = bigquery.QueryJobConfig()

    job_config.query_parameters = query_params

    query_job = client.query(query, job_config=job_config)

    results = query_job.result()

    # rows = list(results)
    # print rows

    # print results

    print type(results)

    # x = list(results.copy())
    print results.schema
    print results.total_rows

    for row in results:
        # print row.id
        print("{} : {} : {}".format(row.id, row.name, row.longitude))
        # print row


    '''
    credentials = service_account.Credentials.from_service_account_file('/Users/harrychasparis/licenses/gcp_key.json')

    project_id = 'adara-data-master'

    client = bigquery.Client(credentials=credentials, project=project_id)

    query_job = client.query("""SELECT *
    FROM opinmind_prod.metrocodes
    LIMIT 10""")
    results = query_job.result()  # Waits for job to complete.

    print 'here 1'

    print results

    print 'here 2'
    '''


if __name__== "__main__":
    main()