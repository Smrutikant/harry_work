from google.cloud import bigquery
from google.oauth2 import service_account


def dataset_exists(client, dataset_id):
    # check if dataset exists and if not
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def create_table_query(client_to,
                       query_string,
                       table_ref,
                       create_disposition='CREATE_IF_NEEDED',
                       write_disposition='WRITE_TRUNCATE',
                       use_batch_mode=False):

    job_config = bigquery.QueryJobConfig()
    # job_config.query_parameters = query_params
    job_config.destination = table_ref
    job_config.create_disposition = create_disposition
    job_config.write_disposition = write_disposition

    # submit API request to start the query
    query_job = client_to.query(
        query_string, location='US', job_config=job_config)

    print 'here 1'
    query_job.result()
    print 'here 2'
    print('Query results loaded to table {}'.format(table_ref.path))

    '''

    print('Job {} is currently in state {}'.format(query_job.job_id, query_job.state))

    
    print('query parameters:')
    for qp in query_job.query_parameters:
        logger.info(qp)
    

    if not use_batch_mode:
        # `query_job.result()` starts job & waits for it to finish.
        #  - Upon completion, job returns Iterator of row data.
        #  - Errors here result in messages such as `ERROR - 404 Not found`
        #    or `BadRequest: 400 Syntax error`
        #  - Such errors will result in Airflow reporting status in log as
        #    'Marking task as FAILED', and job will end without proceeding
        #    to the remaining tasks in the DAG.

        print(query_job.result().schema)

        # Check on the progress by getting the job's updated state. Once state
        # is `DONE`, the results are ready.
        query_job = client_from.get_job(query_job.job_id, location='US')

        print('Job {} is in state {}'.format(
            query_job.job_id, query_job.state))
        
        if query_job.errors or query_job.exception():
            print('Exceptions: {}'.format(query_job.exception()))
            print('Errors: {}'.format(query_job.errors))
        else:
            print('Results loaded to table {}'.format(table_ref.path))
            print("Row count: %d" % self.bq_client.get_table(table_ref).num_rows)
            print('{0:.5g} GBs billed'.format(query_job.total_bytes_billed/1e9))
        
        '''


def main():

    client_from = bigquery.Client(project='adara-data-master')
    client_to = bigquery.Client(project='adara-machinelearning')

    dataset_id = 'harry_dataset'

    ret = dataset_exists(client_to, dataset_id)    # check if dataset already exists

    if ret:
        print 'Dataset exists'
    else:
        dataset_ref = client_to.dataset(dataset_id)

        dataset = bigquery.Dataset(dataset_ref)
        dataset.location = 'US'

        # raises google.api_core.exceptions.AlreadyExists
        # if already present
        client_to.create_dataset(dataset)

    # ----------- Actual Queries ----------- #
    table_name = 'test_1'
    table_ref = client_to.dataset(dataset_id).table(table_name)

    query_string = """
    SELECT *
    FROM `adara-data-master.opinmind_prod.metrocodes`
    LIMIT 10
    """

    create_table_query(client_to, query_string, table_ref, '', '', True)


if __name__== "__main__":
    main()