from google.cloud import bigquery
from google.cloud.exceptions import NotFound


def main():

    db_id = 'adara-market-monitor-qa'
    # client = bigquery.Client(project='adara-machinelearning')
    client = bigquery.Client(project=db_id)

    dataset_id = 'harry_dataset'

    # table_id_list = ['test_1', 'test_2']

    dataset_ref = client.dataset(dataset_id)

    tables = list(client.list_tables(dataset_ref))

    # for table_id in table_id_list:
    for table in tables:
        table_ref = client.dataset(dataset_id).table(table.table_id)

        try:
            client.delete_table(table_ref)  # API request
            print('Table {}:{} deleted.'.format(dataset_id, table.table_id))
        except NotFound:
            print('Table {}:{} not exist.'.format(dataset_id, table.table_id))


if __name__ == "__main__":
    main()
