from google.cloud import bigquery
from google.oauth2 import service_account

from time import sleep


def dataset_exists(client, dataset_id):
    # check if dataset exists and if not
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def create_table_query(client_to,
                       query_string,
                       table_ref,
                       create_disposition='CREATE_IF_NEEDED',
                       write_disposition='WRITE_TRUNCATE',
                       use_batch_mode=False):

    job_config = bigquery.QueryJobConfig()
    # job_config.query_parameters = query_params
    job_config.destination = table_ref
    # job_config.create_disposition = create_disposition
    # job_config.write_disposition = write_disposition

    # submit API request to start the query
    query_job = client_to.query(
        query_string, location='US', job_config=job_config)

    # print 'here 1'
    query_job.result()
    # print 'here 2'
    print('Query results loaded to table {}'.format(table_ref.path))


def query_with_return(client_to, query_string):
    job_config = bigquery.QueryJobConfig()

    # submit API request to start the query
    query_job = client_to.query(
        query_string, location='US', job_config=job_config)

    results = query_job.result()
    results_list = list(results)

    # for row in results:
    #     # print row.id
    #     print("{}".format(row.market_id))

    return results_list


def create_table_schema(client_to, schema, table_ref):
    table = bigquery.Table(table_ref, schema=schema)
    client_to.create_table(table)
    print('Table {} has been created'.format(table_ref.path))


def search_queries(account_id, db_id, dataset_id, mm_tables_dataset_id, client_to):
    # Create schema to insert data
    table_name = 'percentile_checking_Acc_382'
    table_ref = client_to.dataset(dataset_id).table(table_name)
    schema = [
        bigquery.SchemaField('x', 'INTEGER', mode='REQUIRED'),
        bigquery.SchemaField('y', 'INTEGER', mode='REQUIRED'),
    ]
    create_table_schema(client_to, schema, table_ref)

    market_ids_sql_texts = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # Insert data
    for row in market_ids_sql_texts:
        sleep(1)
        rows_to_insert = []
        for i in range(0, 1):
            temp_row = (row, i)
            rows_to_insert.append(temp_row)
        table = client_to.get_table(table_ref)
        client_to.insert_rows(table, rows_to_insert)
    print('Data inserted into table {}'.format(table_ref.path))


def main():

    print "Let's start!"

    db_id = 'adara-market-monitor-qa'
    mm_tables_dataset_id = 'mm_qa'
    # client_from = bigquery.Client(project='adara-data-master')
    # client_to = bigquery.Client(project='adara-machinelearning')
    client_to = bigquery.Client(project=db_id)

    dataset_id = 'harry_dataset'

    # ----------- Dataset actions ----------- #
    ret = dataset_exists(client_to, dataset_id)    # check if dataset already exists

    if ret:
        print 'Dataset exists'
    else:

        dataset_ref = client_to.dataset(dataset_id)

        dataset = bigquery.Dataset(dataset_ref)
        dataset.location = 'US'

        # raises google.api_core.exceptions.AlreadyExists
        # if already present
        client_to.create_dataset(dataset)
        print 'Dataset created'

    # ----------- Actual Queries ----------- #
    account_id = 382
    search_queries(account_id, db_id, dataset_id, mm_tables_dataset_id, client_to)

    print "Bye!"


if __name__ == "__main__":
    main()