from google.cloud import bigquery
from google.oauth2 import service_account


def dataset_exists(client, dataset_id):
    # check if dataset exists and if not
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def create_table_query(client_to,
                       query_string,
                       table_ref,
                       create_disposition='CREATE_IF_NEEDED',
                       write_disposition='WRITE_TRUNCATE',
                       use_batch_mode=False):

    job_config = bigquery.QueryJobConfig()
    # job_config.query_parameters = query_params
    job_config.destination = table_ref
    # job_config.create_disposition = create_disposition
    # job_config.write_disposition = write_disposition

    # submit API request to start the query
    query_job = client_to.query(
        query_string, location='US', job_config=job_config)

    # print 'here 1'
    query_job.result()
    # print 'here 2'
    print('Query results loaded to table {}'.format(table_ref.path))


def query_with_return(client_to, query_string):
    job_config = bigquery.QueryJobConfig()

    # submit API request to start the query
    query_job = client_to.query(
        query_string, location='US', job_config=job_config)

    results = query_job.result()
    results_list = list(results)

    # for row in results:
    #     # print row.id
    #     print("{}".format(row.market_id))

    return results_list


def create_table_schema(client_to, schema, table_ref):
    table = bigquery.Table(table_ref, schema=schema)
    client_to.create_table(table)
    print('Table {} has been created'.format(table_ref.path))


def search_queries(account_id, db_id, dataset_id, mm_tables_dataset_id, client_to):
    # Create table with pairs of market_id and their sql_text
    table_name = 'mm_markets_sql_text_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)

    query_string = """
    with T1 as (
        select id as primary_market_id
        from `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_d`
        where account_id =  """ + str(account_id) + """ and status_id = 1),
    T2 as (
        select competitive_market_id id
        from T1
        join `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_hierarchy`
            using (primary_market_id)
        where status_id = 1
        union all
        select primary_market_id id from T1)
    select id market_id, sql_text
    from `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_d` as m
    join T2
        using (id)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # Get a list of all the market_id for this account
    query_string = """
    select market_id, sql_text
    from `""" + db_id + """.""" + dataset_id + """.""" + table_name + """`
    """
    market_ids_sql_texts = query_with_return(client_to, query_string)

    # for row in market_ids_sql_texts:
    #     # print row
    #     print("{} : {}".format(row.market_id, row.sql_text))

    # Create schema to insert data
    table_name = 'mm_market_hotel_mapping_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    schema = [
        bigquery.SchemaField('market_id', 'INTEGER', mode='REQUIRED'),
        bigquery.SchemaField('str_number', 'INTEGER', mode='REQUIRED'),
    ]
    create_table_schema(client_to, schema, table_ref)

    # Insert data
    for row in market_ids_sql_texts:
        rows_to_insert = []
        for x in range(0, 3):
            temp_row = (row.market_id, x)
            rows_to_insert.append(temp_row)
        table = client_to.get_table(table_ref)
        client_to.insert_rows(table, rows_to_insert)
    print('Data inserted into table {}'.format(table_ref.path))

    # for row in market_ids_sql_texts:
    #     # Take all str_number for each market
    #     query_string = """
    #     select str_number
    #     from `adara-data-master.opinmind_prod..inf_hotel_str_d`
    #     where """ + "{}".format(row.sql_text) + """
    #     """
    #     str_numbers_list = query_with_return(client_to, query_string)
    #
    #     # Create insert statement
    #     rows_to_insert = []
    #     for str_number in market_ids_sql_texts:
    #         # print row
    #         print("{}".format(str_number.str_number))
    #         temp_row = (row.market_id, str_number.str_number)
    #         rows_to_insert.append(temp_row)
    #
    #     table = client_to.get_table(table_ref)  # API request
    #     client_to.insert_rows(table, rows_to_insert)

    # d
    table_name = 'mm_market_city_mapping_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_hotel_mapping_Acc_' + str(account_id)
    t2 = 'mm_markets_of_interest_Acc_' + str(account_id)

    query_string = """
    select distinct m.market_id,
        d.name as market,
        lower(h.city) as city,
        lower(h.state) as state_code,
        case
            when lower(h.mailing_country) = 'united states' then 'us'
            else 'n/a'
        end as country_code
        from `adara-data-master.opinmind_prod.inf_hotel_str_d` as h
        join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as m
            using (str_number)
        join `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_d` as d
            on m.market_id = d.id
        join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mi
            on m.market_id = mi.market_id
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_us_properties_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)

    query_string = """
    select p.id,
        p.advertiser_id,
        lower(trim(p.external_id)) as external_id,
        p.brand_id,
        p.external_brand_id,
        p.company_id,
        p.external_company_id,
        lower(trim(p.name)) as name,
        p.lat,
        p.lon,
        lower(trim(p.address_1)) as address_1,
        lower(trim(p.address_2)) as address_2,
        lower(trim(p.city)) as city,
        case
            when length(lower(trim(p.state))) > 2 and usl.code is not null then lower(trim(usl.code))
            when lower(trim(p.state)) = 'new york state' then 'ny'
            when lower(trim(p.state)) = 'washington d.c.' then 'dc'
            when lower(trim(p.state)) = 'd.c.' then 'dc'
            when lower(trim(p.state)) = 'island of hawaii' then 'hi'
            else lower(trim(p.state))
            end state,
        'us' as country,
        p.zip,
        p.airport_code
    from `adara-data-master.opinmind_prod.properties` as p
    left join `adara-data-master.opinmind_prod.mm_us_state_lookup` as usl
        on (lower(trim(p.state)) = lower(trim(usl.name)))
    where lower(trim(p.country)) in ('us','united states','usa')
        and (
        (p.airport_code is not null and p.external_id != p.airport_code)
        or (p.airport_code is null)
        )
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_properties_base_staging_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_us_properties_Acc_' + str(account_id)
    t2 = 'mm_market_city_mapping_Acc_' + str(account_id)

    query_string = """
    select distinct
        a.dp_id,
        p.name as hotel_name,
        lower(trim(p.external_id)) as hotel_code,
        p.lat as latitude,
        p.lon as longitude,
        mm.city,
        mm.state_code,
        mm.country_code,
        mm.market
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as p
        join `adara-data-master-qa.harry_test.advertisers_COPY` as a
            on (p.advertiser_id = a.id)
        join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mm
            on if(mm.city is not null, (p.city = mm.city), null)
            and if(mm.state_code is not null, (p.state = mm.state_code), null)
            and if(mm.country_code is not null, (p.country = mm.country_code), null)
            where a.category_id = 1
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # SELECT REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(TRIM(REGEXP_REPLACE(
    # lower("abc@_2   FXerfget 1234 _ @@@12df   "), r"[^a-zA-Z0-9_]", ' ')), '  ', ' X'), 'X ', ''), 'X', '')
    # d
    table_name = 'mm_property_distance_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties_base_staging_Acc_' + str(account_id)

    query_string = """
    with p_id as (
        select  p.hotel_name,
            row_number() over (order by null) as id 
        from    `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as p
        group by 1
    )
    select  distinct
        p.hotel_code,
        p.dp_id as p_dp_id,
        p_id.id as p_id,
        REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(TRIM(REGEXP_REPLACE(
            lower(p.hotel_name), r"[^a-zA-Z0-9_]", ' ')), '  ', ' X'), 'X ', ''), 'X', '') as p_hotel_name,
        p.latitude as p_latitude,
        p.longitude as p_longitude,
        hd.parent_company,
        case 
            -- is this list exhaustive? when should we update it?
            when hd.parent_company = 'Wyndham Worldwide' then 970
            when hd.parent_company = 'Choice Hotels International' then 1040
            when hd.parent_company = 'Marriott International' then 1057
            when hd.parent_company = 'Intercontinental Hotels Group' then 1168
            when hd.parent_company = 'Hilton Worldwide' then 820
            when hd.parent_company = 'Accor Company' then 1221
            when hd.parent_company = 'Best Western Company' then 1837
            when hd.parent_company = 'Best Western Hotels & Resorts' then 1837
            when hd.parent_company = 'Hyatt' then 1195
            when hd.parent_company = 'Shangri-La Hotels' then 2043
            when hd.parent_company = 'Starwood Hotels & Resorts' then 1062
            when hd.parent_company = 'LQ Management LLC' then 1090
            when hd.parent_company = 'Extended Stay Hotels' then 1306
            end hd_dp_id,
        hd.str_number as hd_id,
        REGEXP_REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(TRIM(REGEXP_REPLACE(
            lower(hd.hotel_name), r"[^a-zA-Z0-9_]", ' ')), '  ', ' X'), 'X ', ''), 'X', '') as hd_hotel_name,
        hd.latitude as hd_latitude,
        hd.longitude as hd_longitude,
        (2 * 3961 *
            asin(
                sqrt(
                    (sin(radians((p.latitude - hd.latitude) / 2))) ^ 2 + 
                    (cos(radians(hd.latitude))) * 
                    (cos(radians(p.latitude))) * 
                    (sin(radians((p.longitude - hd.longitude) / 2))) ^ 2
                )
            )
        ) as distance_miles,
        row_number() over(
            partition by p.hotel_code
            order by distance_miles desc
        ) as distance_rank
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as p
    join p_id 
    on (p.hotel_name = p_id.hotel_name)
    join `adara-data-master.opinmind_prod.data_providers` as dp 
    on (p.dp_id = dp.id),
        `adara-data-master.opinmind_prod.inf_hotel_str_d` as hd
    where distance_miles < 1.0
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_property_distance_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance_base_Acc_' + str(account_id)

    query_string = """
    select row_number() over (order by null) as id,
        pd.*
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pd
    where p_dp_id = hd_dp_id
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_property_word_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance_Acc_' + str(account_id)

    query_string = """
    with p as (
        select distinct
            pd.id,
            pd.p_id,
            pd.hd_id,
            trim(v.value) as word
            from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pd
        cross join 
            table (regexp_split_rows(pd.p_hotel_name, ' ')) as v),
    hd as (
        select distinct
            pd.id,
            pd.p_id,
            pd.hd_id,
            trim(v.value) as word
            from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pd
        cross join 
            table (regexp_split_rows(pd.hd_hotel_name, ' ')) as v),
    pw as (
        select id,
            p.p_id,
            hd.hd_id,
            p.word as p_word,
            hd.word as hd_word,
            p_word = hd_word as match,
            row_number() over (
                partition by p.id, p.p_id, p.word 
                order by match desc) as rank
        from p 
        left join 
            hd using (id, p_id, hd_id)
    )
    select pw.id,
        pw.p_id,
        pw.hd_id,
        pw.p_word,
        pw.hd_word
    from pw
    where pw.rank = 1
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # NZ: decode(expr, bool, val1, val2) ==> BQ: select IF("a" = "b", 1, 0)
    # d
    table_name = 'mm_property_match_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_word_Acc_' + str(account_id)
    t2 = 'mm_property_distance_Acc_' + str(account_id)

    query_string = """
    with a as (
        select pw.id,
            pw.p_id,
            pw.hd_id,
            sum(if(pw.p_word = pw.hd_word,1,0)) as match,
            count(*) as records
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pw
        group by 1,2,3
    ),
    b as (
        select *,
            (1.0 * match / records) as match_rate
        from a
    )
    select b.id,
        pd.hotel_code,
        pd.p_dp_id,
        b.p_id,
        pd.p_hotel_name,
        pd.hd_dp_id,
        b.hd_id,
        pd.hd_hotel_name,
        b.match,
        b.records,
        b.match_rate,
        pd.distance_miles,
        row_number() over (
            partition by b.p_id 
            order by b.match_rate desc, pd.distance_miles) as match_rank_p
    from b 
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as pd using (id)
    where b.match_rate != 0
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_property_match_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match_base_Acc_' + str(account_id)

    query_string = """
    with pm as (
        select pwa.*,
            row_number() over (
                partition by pwa.hd_id 
                order by pwa.match_rate desc) as match_rank_hd
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pwa
        where pwa.match_rank_p = 1
    )
    select  pm.id,
        pm.hotel_code,
        pm.p_dp_id,
        pm.p_id,
        pm.p_hotel_name,
        pm.hd_dp_id,
        pm.hd_id,
        pm.hd_hotel_name,
        pm.match,
        pm.records,
        pm.match_rate,
        pm.distance_miles
    from pm
    where pm.match_rank_hd = 1
        and pm.match > 1 
        and pm.records > 1
        and pm.p_dp_id = pm.hd_dp_id
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # select CASE
    #    WHEN "U" = "Y" THEN 1
    #    WHEN "U" = "N" THEN 0
    #    ELSE null
    #    END
    # select PARSE_DATE('%E4Y%m',  CAST (hd.open_date as STRING))
    # d
    table_name = 'mm_properties_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match_Acc_' + str(account_id)
    t2 = 'mm_properties_base_staging_Acc_' + str(account_id)

    query_string = """
    select pm.hotel_code,
        p.dp_id,
        p.hotel_name,
        p.latitude,
        p.longitude,
        p.city,
        p.state_code,
        p.country_code,
        p.market,
        -- add hotel attributes from STR data
        to_date(hd.open_date,'YYYYMM') as open_date,
        hd.rooms as number_of_rooms,
        hd.chain_scale,
        hd.floors,
        hd.location,
        decode(hd.indoor_corridors,'Y',1,'N',0) as has_indoor_corridors,
        decode(hd.restaurant,'Y',1,'N',0) as has_restaurant,
        decode(hd.convention,'Y',1,'N',0) as has_convention,
        decode(hd.conference,'Y',1,'N',0) as has_conference,
        decode(hd.spa,'Y',1,'N',0) as has_spa,
        decode(hd.largest_meeting_space = hd.total_meeting_space and 
            hd.total_meeting_space != 0,'t',1,0) as has_single_meeting_space,
        hd.largest_meeting_space,
        hd.total_meeting_space,
        decode(hd.resort,'Y',1,'N',0) as is_resort,
        decode(hd.ski,'Y',1,'N',0) as is_ski_resort,
        decode(hd.golf,'Y',1,'N',0) as is_golf_resort,
        decode(hd.all_suites,'Y',1,'N',0) as is_all_suites,
        decode(hd.casino,'Y',1,'N',0) as is_casino,
        hd.price,
        hd.single_low_rate,
        hd.single_high_rate,
        hd.double_low_rate,
        hd.double_high_rate,
        hd.suite_low_rate,
        hd.suite_high_rate
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as pm
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as p 
    on (pm.hotel_code = p.hotel_code)
        and (pm.p_dp_id = p.dp_id)
    join `adara-data-master.opinmind_prod.inf_hotel_str_d` as hd 
    on (pm.hd_id = hd.str_number)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_markets_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_city_mapping_Acc_' + str(account_id)

    query_string = """
    select mm.market,
        count(*) as hotels,
        sum(hd.rooms) as number_of_rooms
    from `adara-data-master.opinmind_prod.inf_hotel_str_d` as hd 
    join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mm 
    on (lower(trim(hd.city)) = mm.city)
        and (lower(trim(hd.state)) = mm.state_code)
    where hd.country = 'United States'
    group by 1
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_hotel_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties_Acc_' + str(account_id)
    t2 = 'mm_market_city_mapping_Acc_' + str(account_id)

    query_string = """
    select  distinct
        h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        decode(h.activity_type in ('click','search'),'t','search','book') as activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        nvl(p.market, mm.market) as market,
        nvl(h.hotel_country, p.country_code) as hotel_country,
        nvl(h.hotel_state, p.state_code) as hotel_state,
        nvl(h.hotel_city, p.city) as hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `adara-data-master.opinmind_prod.ekv_hotel_all` as h 
    left join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as p
        using (dp_id, hotel_code)
    left join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mm 
        on (h.hotel_city = mm.city)
        and (h.hotel_state = mm.state_code)
        and (h.hotel_country = mm.country_code)
    where h.activity_type in ('book','search','click')
        and h.event_ts < current_date
        and h.event_ts >= timestamp('2014-10-01 00:00:00')
        and h.checkin_date is not null 
        and h.checkout_date >= date(2016,01,01)
        and (
            (p.hotel_code is not null and p.dp_id is not null) or 
            (mm.city is not null or 
            mm.state_code is not null or 
            mm.country_code is not null
            )
        )
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_hotel_book_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base_Acc_' + str(account_id)

    query_string = """
    with h as (
        select  h.*,
            row_number() over (
                partition by h.cookie_id,
                    h.dp_id, 
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date, 
                    h.checkout_date
                order by h.event_ts desc) as ix
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
        where h.activity_type = 'book'
    )
    select h.event_id,
        h.cookie_id, 
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where h.ix = 1
    and h.checkin_date >= date(h.event_ts)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_hotel_search_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base_Acc_' + str(account_id)

    query_string = """
    with h as (
        select h.*,
            extract(epoch from h.event_ts - lag(h.event_ts, 1) over (
                partition by h.cookie_id,
                    h.dp_id,
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date,
                    h.checkout_date
                order by h.event_ts
                )
            ) as lag_seconds
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
        where h.activity_type = 'search'
    )
    select  h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where (h.lag_seconds > 300 or h.lag_seconds is null)
    and h.checkin_date >= date(h.event_ts)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_hotel_staging_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_search_Acc_' + str(account_id)
    t2 = 'mm_ekv_hotel_book_Acc_' + str(account_id)

    query_string = """
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    union all 
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_hotel_dirty_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_staging_Acc_' + str(account_id)

    query_string = """
    select  h.*,
        decode(
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) > 0 and 
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) < 1000,
            't',
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd),
            null
        ) as avg_daily_rate_usd
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    left join `adara-data-master.opinmind_prod.currencies` as cu 
        on (lower(h.currency_type) = lower(cu.currency_code))
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ihg_alos_map_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty_Acc_' + str(account_id)

    query_string = """
    select  h.market,
        extract(month from h.checkin_date) as checkin_month,
        extract(dow from h.checkin_date) as checkin_dow,
        round((1.0 * sum(h.trip_duration) / count(*)), 1) as alos,
        floor(alos) as alos_whole,
        (alos - alos_whole) as alos_decimal
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    where h.dp_id = 1168
        and h.activity_type = 'book'
        and h.event_ts < timestamp('2018-04-12 00:00:00')
        and h.checkin_date < timestamp('2018-04-12 00:00:00')
        and h.checkin_date >= date('2018-04-12') - (365 * 2)
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ihg_ekv_hotel_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty_Acc_' + str(account_id)

    query_string = """
    select  h.*,
        extract(month from h.checkin_date) as checkin_month,
        extract(dow from h.checkin_date) as checkin_dow,
        row_number() over (
            partition by checkin_month, checkin_dow, h.market 
            order by random()) as ix
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    where h.dp_id = 1168 
        and h.activity_type = 'book'
        and h.event_ts >= timestamp('2018-04-12 00:00:00')
        and h.event_ts < timestamp('2018-06-27 00:00:00')
        and h.checkin_date = h.checkout_date
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ihg_ekv_hotel_staging_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_base_Acc_' + str(account_id)
    t2 = 'mm_ihg_alos_map_Acc_' + str(account_id)

    query_string = """
    with a as (
        select h.checkin_month,
            h.checkin_dow,
            h.market,
            max(h.ix) as max_ix
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
        group by 1,2,3
    ),
    b as (
        select checkin_month,
            checkin_dow,
            market,
            round(a.max_ix * am.alos_decimal) as ix_boundary
        from a
        join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as am 
        using (checkin_month, checkin_dow, market)
    )
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        case 
            when h.ix <= b.ix_boundary then (am.alos_whole + 1) + h.checkin_date
            else am.alos_whole + h.checkin_date
            end new_checkout_date,
        (new_checkout_date - h.checkin_date) as new_trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate,
        h.checkin_month,
        h.checkin_dow
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h 
    join b 
        using (checkin_month, checkin_dow, market)
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as am 
        using (checkin_month, checkin_dow, market)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # equal with the deletion on line 1015 of NZ
    # d
    table_name = 'mm_ekv_hotel_mid_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty_Acc_' + str(account_id)
    t2 = 'mm_ihg_ekv_hotel_staging_Acc_' + str(account_id)

    query_string = """
    select *
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    where event_id not in (
        select event_id
        from `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
    )
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ihg_ekv_hotel_insert_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_staging_Acc_' + str(account_id)

    query_string = """
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.new_checkout_date as checkout_date,
        h.new_trip_duration as trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # equal with the insertion on line 1050 of NZ
    # d
    table_name = 'mm_ekv_hotel_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_mid_Acc_' + str(account_id)
    t2 = 'mm_ihg_ekv_hotel_insert_Acc_' + str(account_id)

    query_string = """
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    union all
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_market_airport_mapping_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_markets_of_interest_Acc_' + str(account_id)

    query_string = """
    select  m.name as market,
        a.market_id, 
        a.airport_code 
    from `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_airport_map` as a 
    join `""" + db_id + """.""" + mm_tables_dataset_id + """.mm_market_d` as m 
        on a.market_id = m.id
    join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as i 
        on a.market_id = i.market_id
    where a.status_id = 1
        and m.status_id = 1
    order by market, airport_code;
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_destination_airport_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_airport_mapping_Acc_' + str(account_id)

    query_string = """
    select ac.airport_code,
        ac.airport_name,
        ac.airport_type,
        ac.city_name,
        ac.dma_code,
        ac.region_code,
        ac.country_code,
        mm.market,
        ac.latitude,
        ac.longitude,
        ac.elevation,
        ac.modification_ts
    from `adara-data-master-qa.harry_test.ekv_lookup_airport_code_COPY` as ac
    join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mm 
        using (airport_code)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_distinct_destination_airport_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_destination_airport_Acc_' + str(account_id)

    query_string = """
    select distinct dac.airport_code
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as dac
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_flight_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_distinct_destination_airport_Acc_' + str(account_id)

    query_string = """
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        decode(f.activity_type in ('click','search'),'t','search','book') as activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `adara-data-master.opinmind_prod.ekv_flight_all` as f
    join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as da 
        on (f.destination_airport = da.airport_code)
    where f.activity_type in ('click','search','book')
        and f.event_ts < current_date
        and f.event_ts >= timestamp('2014-10-01 00:00:00')   -- current_date - interval '3 year' - interval '3 month'
        and f.departure_date is not null
        and f.return_date >= date('2016-01-01')   -- current_date - interval '3 year'
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_flight_book_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base_Acc_' + str(account_id)

    query_string = """
    with f as (
        select f.*,
            row_number() over(
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts desc
            ) as ix 
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as f
        where f.activity_type = 'book'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where f.ix = 1
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_flight_search_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base_Acc_' + str(account_id)

    query_string = """
    with f as (
        select f.*,
            TIMESTAMP_DIFF(f.event_ts, lag(f.event_ts, 1) over (
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts
                ), SECOND
            ) as lag_seconds
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as f
        where f.activity_type = 'search'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where (f.lag_seconds is null or f.lag_seconds > 300)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_flight_staging_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_book_Acc_' + str(account_id)
    t2 = 'mm_ekv_flight_search_Acc_' + str(account_id)

    query_string = """
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    union all 
    select * from `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_ekv_flight_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_staging_Acc_' + str(account_id)
    t2 = 'mm_destination_airport_Acc_' + str(account_id)

    query_string = """
    select  f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        dac.market,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as f
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as dac 
        on (f.destination_airport = dac.airport_code)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_base_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_Acc_' + str(account_id)
    t2 = 'mm_properties_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        d.cal_date stay_date,
        date(event_ts) statistic_date,
        count(1) bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
        using (hotel_code, dp_id)
    join `adara-data-master.opinmind_prod.inf_day_d` as d
        on (d.cal_date between h.checkin_date and h.checkout_date - 1)
    where d.cal_date >= '2016-01-01'
        and event_ts >= d.cal_date - 365
        and activity_type = 'book'
    group by 1,2,3,4
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_base_t2_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t1_Acc_' + str(account_id)

    query_string = """
    with T1 as (
        select hotel_code,
            dp_id,
            stay_date,
            sum(bookings) as bookings,
            min(statistic_date) as min_book_date,
            max(statistic_date) as max_book_date
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
        group by 1,2,3
    ),
    T2 as(
        select hotel_code,
            dp_id,
            min(stay_date) as min_stay_date,
            max(stay_date) as max_stay_date
        from T1 
        group by 1,2
    ),
    T3 as (
        select hotel_code,
            dp_id,
            d.cal_date as stay_date
        from T2
        cross join `adara-data-master.opinmind_prod.inf_day_d` as d
        where d.cal_date between min_stay_date and max_stay_date
    )
    select hotel_code,
        dp_id,
        stay_date,
        nvl(bookings,0) as bookings
    from T3
    left outer join T1
        using (hotel_code, dp_id, stay_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_base_t3_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t2_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        stay_date,
        d.cal_date as statistic_date,
        bookings as total_bookings_target
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t 
    join `adara-data-master.opinmind_prod.inf_day_d` as d
        on (d.cal_date between DATE_SUB(t.stay_date, INTERVAL 1 YEAR) and t.stay_date and d.cal_date <= current_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t3_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_base_t1_Acc_' + str(account_id)
    t3 = 'mm_properties_Acc_' + str(account_id)

    query_string = """
    with T1 as (
        select hotel_code,
        dp_id,
        stay_date,
        statistic_date,
        ifnull(bookings,0) as bookings_today,
        sum(ifnull(bookings,0)) over (
            partition by hotel_code, dp_id, stay_date
            order by statistic_date) as cumulative_bookings_target,
        total_bookings_target
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
        left outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
            using (hotel_code, dp_id, stay_date, statistic_date)
    )
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        least(T1.cumulative_bookings_target, p.number_of_rooms) as cumulative_bookings_target,
        case
            when stay_date >= current_date then null
            else least(T1.total_bookings_target, p.number_of_rooms)
            end total_bookings_target
    from T1
    join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as p
        using (hotel_code, dp_id)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_metric_calculation_base_staging_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_Acc_' + str(account_id)

    query_string = """
    select h.market,
        l.metrocode,
        date_trunc('month', h.checkin_date) as stay_month,
        date_trunc('month', date(h.event_ts)) as statistic_month,
        h.activity_type,
        count(*) as events,
        sum(h.avg_daily_rate_usd * h.trip_duration) as adr_numerator,
        sum(decode(h.avg_daily_rate_usd is not null and 
            h.trip_duration is not null, 
            't', 
            h.trip_duration)) as adr_denominator,
        sum(h.trip_duration) as alos_numerator,
        sum(nvl2(h.trip_duration, 1, 0)) as alos_denominator,
        sum(h.number_of_travelers) as anot_numerator,
        sum(nvl2(h.number_of_travelers, 1, 0)) as anot_denominator
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    join `adara-data-master.opinmind_prod.location` as l 
        on (h.location_id = l.id)
    join `adara-data-master.opinmind_prod.metrocodes` as m 
        on (l.metrocode = m.id)
    group by 1,2,3,4,5
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_market_coordinates_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties_Acc_' + str(account_id)

    query_string = """
    select  m.market,
        (180 * atan2(m.zeta, m.xi) / pi()) as avg_longitude,
        m.avg_latitude
    from (
        select p.market,
            avg(sin(pi() * p.longitude / 180)) as zeta,
            avg(cos(pi() * p.longitude / 180)) as xi,
            avg(p.longitude) as avg_longitude,
            avg(p.latitude) as avg_latitude
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as p
        group by 1
    ) as m
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_coordinates_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates_Acc_' + str(account_id)

    query_string = """
    select (180 * atan2(c.zeta, c.xi) / pi()) as avg_longitude,
        c.avg_latitude
    from (
        select avg(sin(pi() * mc.avg_longitude / 180)) as zeta,
            avg(cos(pi() * mc.avg_longitude / 180)) as xi,
            avg(mc.avg_longitude) as avg_longitude,
            avg(mc.avg_latitude) as avg_latitude
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mc
    ) as c
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_metro_coordinates_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)

    query_string = """
    select  id as metrocode,
        longitude as avg_longitude, 
        latitude as avg_latitude
    from `adara-data-master.opinmind_prod.metrocodes`
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_metro_market_distance_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates_Acc_' + str(account_id)
    t2 = 'mm_metric_metro_coordinates_Acc_' + str(account_id)

    query_string = """
    select m.market,
        mc.metrocode,
        (2 * 3961 * 
            asin(
                sqrt(
                    (sin(radians((m.avg_latitude - mc.avg_latitude) / 2))) ^ 2 + 
                    (cos(radians(mc.avg_latitude))) * 
                    (cos(radians(m.avg_latitude))) * 
                    (sin(radians((m.avg_longitude - mc.avg_longitude) / 2))) ^ 2
                )
            )
        ) as distance_miles       
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as m,
        `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mc
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_metro_distance_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_coordinates_Acc_' + str(account_id)
    t2 = 'mm_metric_metro_coordinates_Acc_' + str(account_id)

    query_string = """
    select mc.metrocode,
        (2 * 3961 * 
            asin(
                sqrt(
                    (sin(radians((m.avg_latitude - mc.avg_latitude) / 2))) ^ 2 + 
                    (cos(radians(mc.avg_latitude))) * 
                    (cos(radians(m.avg_latitude))) * 
                    (sin(radians((m.avg_longitude - mc.avg_longitude) / 2))) ^ 2
                )
            )
        ) as distance_miles       
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as m,
        `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mc
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_base_metrocode_map_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging_Acc_' + str(account_id)
    t2 = 'mm_metric_metro_distance_Acc_' + str(account_id)

    query_string = """
    with a as (
        -- segment bookings to target & competitor markets by the origin
        -- metrocode, and calculate each metrocodes share of bookings to
        -- to these markets
        select  mc.metrocode,
            sum(mc.events) as metrocode_bookings,
            sum(metrocode_bookings) over() as overall_bookings,
            (1.0 * metrocode_bookings / overall_bookings) as metrocode_booking_share
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mc
        where mc.activity_type = 'book'
        group by 1
    ),
    b as (
        -- for origin metrocodes accounting for < 0.01 of bookings to target
        -- and competitor markets, set their metrocode_group to -1; otherwise
        -- set the metrocode_group to the metrocode as-is
        select a.metrocode,
            case 
                when a.metrocode_booking_share >= 0.01 then a.metrocode 
                else -1
                end metrocode_group,
            a.metrocode_bookings as bookings,
            a.metrocode_booking_share as booking_share
        from a
    ),
    c as (
        -- from `mm_metric_metro_distance`, get distance between the center
        -- of each metrocode and the center of the target & comp MM markets;
        -- rank each low-share origin metrocodes (with metrocode_group = -1)
        -- by their distance from the cross-market center of the target & 
        -- competitor markets
        select  b.*,
            md.distance_miles,
            row_number() over (order by md.distance_miles) as distance_rank
        from `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as md 
        join b 
        using (metrocode)
        where b.metrocode_group = -1
    ),
    p as (
        -- get 20%, 40%, 60%, 80% percentiles of distance between the low-share
        -- origin metrocodes and the center of the target/competitor markets
        select PERCENTILE_CONT(c.distance_miles, 0.2) OVER() AS p_20,
            PERCENTILE_CONT(c.distance_miles, 0.4) OVER() AS p_40,
            PERCENTILE_CONT(c.distance_miles, 0.6) OVER() AS p_60,
            PERCENTILE_CONT(c.distance_miles, 0.8) OVER() AS p_80
        from c
    )
    -- classify low-share origin metrocodes into 5 metrocode_group segments
    -- based on their distance to center of the target/competitor markets:
    --  * if distance < 20th percentile (closer), set group = -1
    --  * else if distance < 40th percentile, set group = -2; etc.
    --  * note that metrocodes not flagged as low share, i.e. those whose
    --    original metrocode_group != -1, will be in their own group
    select b.metrocode,
        case 
            when b.metrocode_group = -1 and c.distance_miles <  p.p_20 then -1
            when b.metrocode_group = -1 and c.distance_miles >= p.p_20 and c.distance_miles < p.p_40 then -2
            when b.metrocode_group = -1 and c.distance_miles >= p.p_40 and c.distance_miles < p.p_60 then -3 
            when b.metrocode_group = -1 and c.distance_miles >= p.p_60 and c.distance_miles < p.p_80 then -4 
            when b.metrocode_group = -1 and c.distance_miles >= p.p_80 then -5
            else b.metrocode
            end metrocode_group,
        c.distance_miles,
        b.bookings,
        b.booking_share
    from b
    left join c
        using (metrocode), p
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_metric_calculation_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging_Acc_' + str(account_id)
    t2 = 'mm_metric_base_metrocode_map_Acc_' + str(account_id)

    query_string = """
    select  mc.market,
        mm.metrocode_group,
        mc.stay_month,
        mc.statistic_month,
        mc.activity_type,
        sum(mc.events) as events,
        sum(mc.adr_numerator) as adr_numerator,
        sum(mc.adr_denominator) as adr_denominator,
        sum(mc.alos_numerator) as alos_numerator,
        sum(mc.alos_denominator) as alos_denominator,
        sum(mc.anot_numerator) as anot_numerator,
        sum(mc.anot_denominator) as anot_denominator
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mc
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mm 
    using (metrocode)
    -- keep only if statistic month is within 12-15 months of stay month
    where case 
        when mc.stay_month = date('2016-01-01') 
            then mc.statistic_month >= DATE_SUB(mc.stay_month, interval 15 month)
            else mc.statistic_month >= DATE_SUB(mc.stay_month, interval 1 year)
            end 
        and mc.statistic_month <= mc.stay_month
        and mc.stay_month >= date('2016-01-01') 
    group by 1,2,3,4,5
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_metric_calculation_point_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_Acc_' + str(account_id)

    query_string = """
    with s as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,
            
            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mc
        where mc.activity_type = 'search'
    ),
    b as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,
            
            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mc
        where mc.activity_type = 'book'
    )
    select market,
        metrocode_group, 
        stay_month,
        statistic_month,
        
        s.events as search_events,
        
        s.adr_numerator as search_adr_numerator,
        s.adr_denominator as search_adr_denominator,
        (1.0 * s.adr_numerator / s.adr_denominator) as search_adr,
        
        s.alos_numerator as search_alos_numerator,
        s.alos_denominator as search_alos_denominator,
        (1.0 * s.alos_numerator / s.alos_denominator) as search_alos,
        
        s.anot_numerator as search_anot_numerator,
        s.anot_denominator as search_anot_denominator,
        (1.0 * s.anot_numerator / s.anot_denominator) as search_anot,
        
        b.events as book_events,
        b.adr_numerator as book_adr_numerator,
        b.adr_denominator as book_adr_denominator,
        (1.0 * b.adr_numerator / b.adr_denominator) as book_adr,
        
        b.alos_numerator as book_alos_numerator,
        b.alos_denominator as book_alos_denominator,
        (1.0 * b.alos_numerator / b.alos_denominator) as book_alos,
        
        b.anot_numerator as book_anot_numerator,
        b.anot_denominator as book_anot_denominator,
        (1.0 * b.anot_numerator / b.anot_denominator) as book_anot
    from s 
    full outer join b 
    using (market, metrocode_group, stay_month, statistic_month)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_metric_calculation_cumulative_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point_Acc_' + str(account_id)

    query_string = """
    select a.market,
        a.metrocode_group,
        a.stay_month,
        a.statistic_month,
        sum(b.search_events) as search_events,
        
        (1.0 * sum(b.search_adr_numerator) / sum(b.search_adr_denominator)) as search_adr,
        (1.0 * sum(b.search_alos_numerator) / sum(b.search_alos_denominator)) as search_alos,
        (1.0 * sum(b.search_anot_numerator) / sum(b.search_anot_denominator)) as search_anot,
        
        sum(b.book_events) as book_events,
        
        (1.0 * sum(b.book_adr_numerator) / sum(b.book_adr_denominator)) as book_adr,
        (1.0 * sum(b.book_alos_numerator) / sum(b.book_alos_denominator)) as book_alos,
        (1.0 * sum(b.book_anot_numerator) / sum(b.book_anot_denominator)) as book_anot
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as a 
    join `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    on (a.market = b.market)
        and (a.metrocode_group = b.metrocode_group)
        and (a.stay_month = b.stay_month)
        and (b.statistic_month <= a.statistic_month)
    group by 1,2,3,4
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_target_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point_Acc_' + str(account_id)

    query_string = """
    select mcp.market,
        mcp.metrocode_group,
        mcp.stay_month,
        sum(mcp.book_events) as bookings_target,
        round(1.0 * sum(mcp.book_adr_numerator) / sum(mcp.book_adr_denominator)) as adr_target,
        round((1.0 * sum(mcp.book_alos_numerator) / sum(mcp.book_alos_denominator)),2) as alos_target,
        round((1.0 * sum(mcp.book_anot_numerator) / sum(mcp.book_anot_denominator)),2) as anot_target,
        round(1.0 * sum(mcp.search_events) / sum(mcp.book_events)) as stob_target
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mcp
    where mcp.stay_month < date(date_trunc('month', current_date))
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_feature_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point_Acc_' + str(account_id)
    t2 = 'mm_metric_dataset_base_metric_calculation_cumulative_Acc_' + str(account_id)

    query_string = """
    select market,
        metrocode_group,
        stay_month,
        statistic_month,
        mcp.search_events as ps_events,
        round(mcp.search_adr) as ps_adr,
        round(mcp.search_alos, 2) as ps_alos,
        round(mcp.search_anot, 2) as ps_anot,
        
        mcp.book_events as pb_events,
        round(mcp.book_adr) as pb_adr,
        round(mcp.book_alos, 2) as pb_alos,
        round(mcp.book_anot, 2) as pb_anot,
        
        mcc.search_events as cs_events,
        round(mcc.search_adr) as cs_adr,
        round(mcc.search_alos, 2) as cs_alos,
        round(mcc.search_anot, 2) as cs_anot,
        
        mcc.book_events as cb_events,
        round(mcc.book_adr) as cb_adr,
        round(mcc.book_alos, 2) as cb_alos,
        round(mcc.book_anot, 2) as cb_anot
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as mcp 
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as mcc 
    using (market, metrocode_group, stay_month, statistic_month)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_metric_dataset_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_feature_Acc_' + str(account_id)
    t2 = 'mm_metric_dataset_base_target_Acc_' + str(account_id)

    query_string = """
    select market, 
        metrocode_group,
        stay_month,
        dbf.statistic_month,
        ((date(stay_month) - date(dbf.statistic_month)) / 30) as stay_window,
        dbt.bookings_target,
        -- features at (market, metrocode_group, stay_month) level
        dbt.adr_target,
        dbt.alos_target,
        dbt.anot_target,
        dbt.stob_target,

        -- counts & averages for the (stay month, stat month)
        dbf.ps_events,
        dbf.ps_adr,
        dbf.ps_alos,
        dbf.ps_anot,
        
        dbf.pb_events,
        dbf.pb_adr,
        dbf.pb_alos,
        dbf.pb_anot,
        
        dbf.cs_events,
        dbf.cs_adr,
        dbf.cs_alos,
        dbf.cs_anot,
        
        dbf.cb_events,
        dbf.cb_adr,
        dbf.cb_alos,
        dbf.cb_anot
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as dbf
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as dbt
    using (market, metrocode_group, stay_month)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_hotel_summary_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_Acc_' + str(account_id)

    query_string = """
    select h.hotel_code,
        h.dp_id,
        h.market,
        h.activity_type,
        h.checkin_date,
        h.checkout_date,
        date(h.event_ts) as event_date,
        count(*) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h
    group by 1,2,3,4,5,6,7
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_hotel_summary_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary_base_Acc_' + str(account_id)

    query_string = """
    select hs.hotel_code,
        hs.dp_id,
        hs.market,
        hs.activity_type,
        d.cal_date as stay_date,
        hs.event_date,
        sum(hs.events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as hs
    join `adara-data-master-qa.harry_test.inf_day_d_COPY` as d 
    on (d.cal_date between hs.checkin_date and hs.checkout_date - 1)
    group by 1,2,3,4,5,6
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_hc_search_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_booking_hotel_summary_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs 
    on (b.stay_date = hs.stay_date)
        and (hs.event_date >= DATE_SUB(b.statistic_date, interval 14 day))
        and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'search'
        and b.hotel_code = hs.hotel_code
        and b.dp_id = hs.dp_id
    group by 1,2,3,4,5
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_search_by_market_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    where activity_type = 'search'
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        statistic_date
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_search_by_market_t3_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t1
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t2
        using (market, stay_date)
    where t1.event_date between t2.statistic_date - 14 and t2.statistic_date - 1 
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_search_by_market_t4_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        event_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_stay_stat_d_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        statistic_date
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_search_by_market_t6_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t4_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t4
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t5
    using (stay_date)
    where t4.event_date between t5.statistic_date - 14 and t5.statistic_date - 1 
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_m_search_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_hotel_search_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_cm_search_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_m_search_base_Acc_' + str(account_id)
    t2 = 'mm_hotel_search_by_market_t6_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        t.total_hotel_searches - m.hotel_market_searches as hotel_competitive_market_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as m 
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t
    using (stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_hc_book_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_booking_hotel_summary_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs 
        on b.stay_date = hs.stay_date
            and b.hotel_code = hs.hotel_code
            and b.dp_id = hs.dp_id
            and (hs.event_date >= DATE_SUB(b.statistic_date, interval 14 day))
            and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'book'
    group by 1,2,3,4,5
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_book_by_market_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    where activity_type = 'book'
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_book_by_market_t3_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t1
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t2
        using (market, stay_date)
    where t1.event_date between t2.statistic_date - 14 and t2.statistic_date - 1 
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_book_by_market_t4_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_hotel_book_by_market_t6_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t4_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t4
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t5
        using (stay_date)
    where t4.event_date between t5.statistic_date - 14 and t5.statistic_date - 1 
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    ##
    table_name = 'mm_booking_dataset_hotel_m_book_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_hotel_book_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs 
    using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_cm_book_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_hotel_book_by_market_t6_Acc_' + str(account_id)
    t3 = 'mm_hotel_book_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_hotel_bookings - nvl(m.events,0) as hotel_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t
        using (stay_date, statistic_date)
    left outer join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as m
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_hotel_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_hc_search_base_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_hotel_m_search_base_Acc_' + str(account_id)
    t3 = 'mm_booking_dataset_hotel_cm_search_base_Acc_' + str(account_id)
    t4 = 'mm_booking_dataset_hotel_hc_book_base_Acc_' + str(account_id)
    t5 = 'mm_booking_dataset_hotel_m_book_base_Acc_' + str(account_id)
    t6 = 'mm_booking_dataset_hotel_cm_book_base_Acc_' + str(account_id)

    query_string = """
    select  hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        
        hotel_code_searches,
        hotel_market_searches,
        hotel_competitive_market_searches,
        
        hotel_code_bookings,
        hotel_market_bookings,
        hotel_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as hcs
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as ms 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as cms 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t4 + """` as hcb 
        using (hotel_code, dp_id, market, stay_date, statistic_date) 
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t5 + """` as mb 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t6 + """` as cmb 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_flight_summary_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_Acc_' + str(account_id)

    query_string = """
    select  f.destination_airport,
        f.market,
        f.activity_type,
        f.departure_date,
        f.return_date,
        date(f.event_ts) as event_date,
        count(*) as events 
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as f 
    group by 1,2,3,4,5,6
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_flight_summary_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base_Acc_' + str(account_id)

    query_string = """
    with T1 as (
        select departure_date,
            return_date 
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
        group by 1,2)
    select departure_date,
        return_date,
        d.cal_date stay_date
    from T1
    join `adara-data-master-qa.harry_test.inf_day_d_COPY` as d
        on (d.cal_date between T1.departure_date and T1.return_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_flight_summary_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base_Acc_' + str(account_id)
    t2 = 'mm_booking_flight_summary_t1_Acc_' + str(account_id)

    query_string = """
    select fs.destination_airport,
        fs.market,
        fs.activity_type,
        t1.stay_date,
        fs.event_date,
        sum(fs.events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as fs
        join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t1
            using (departure_date, return_date)
    group by 1,2,3,4,5
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_search_by_market_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    where activity_type = 'search'
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_search_by_market_t3_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t1
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t2
        using (market, stay_date)
    where t1.event_date between t2.statistic_date - 14 and t2.statistic_date - 1 
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_flight_m_search_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_flight_search_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_search_by_market_t4_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_search_by_market_t6_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t4_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t4
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t5
        using (stay_date)
    where t4.event_date between t5.statistic_date - 14 and t5.statistic_date - 1 
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_flight_cm_search_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_flight_search_by_market_t6_Acc_' + str(account_id)
    t3 = 'mm_flight_search_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_searches - nvl(m.events,0) as flight_competitive_market_searches
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b 
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t
        using (stay_date, statistic_date)
    left outer join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as m 
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_book_by_market_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    where activity_type = 'book'
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_book_by_market_t3_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_market_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t1
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t2
        using (market, stay_date)
    where t1.event_date between t2.statistic_date - 14 and t2.statistic_date - 1 
    group by 1,2,3
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_book_by_market_t4_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_flight_book_by_market_t6_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t4_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_stay_stat_d_Acc_' + str(account_id)

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t4 
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t5
        using (stay_date)
    where t4.event_date between t5.statistic_date - 14 and t5.statistic_date - 1 
    group by 1,2
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_flight_m_book_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_flight_book_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_flight_cm_book_base_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_flight_book_by_market_t6_Acc_' + str(account_id)
    t3 = 'mm_flight_book_by_market_t3_Acc_' + str(account_id)

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_bookings - nvl(m.events,0) as flight_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b 
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as t
        using (stay_date, statistic_date)
    left outer join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as m
        using (market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_flight_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight_m_search_base_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_flight_cm_search_base_Acc_' + str(account_id)
    t3 = 'mm_booking_dataset_flight_m_book_base_Acc_' + str(account_id)
    t4 = 'mm_booking_dataset_flight_cm_book_base_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,

        flight_market_searches,
        flight_competitive_market_searches,
        
        flight_market_bookings,
        flight_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as ms
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as cms 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as mb 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `""" + db_id + """.""" + dataset_id + """.""" + t4 + """` as cmb 
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_holidays_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)

    query_string = """
    with d as (
        select distinct b.stay_date
        from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as b
    )
    select d.stay_date,
        max(b.holiday_date) as last_holiday_date,
        min(a.holiday_date) as next_holiday_date
    from d,
        `adara-data-master-qa.harry_test.mm_holiday_dates_COPY` as b,
        `adara-data-master-qa.harry_test.mm_holiday_dates_COPY` as a
    where b.holiday_date < d.stay_date
        and a.holiday_date > d.stay_date
    group by 1
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_t1_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_Acc_' + str(account_id)
    t2 = 'mm_booking_holidays_Acc_' + str(account_id)
    t3 = 'mm_markets_Acc_' + str(account_id)
    t4 = 'mm_properties_Acc_' + str(account_id)

    query_string = """
    select db.hotel_code,
       db.dp_id,
       db.market,
       db.stay_date,
       (db.stay_date - floor((db.stay_date - db.statistic_date) / 7.0) * 7) as statistic_week,
       
       max(db.cumulative_bookings_target) as cumulative_bookings_target,
       max(db.total_bookings_target) as total_bookings_target,
       
       min((db.stay_date - statistic_week) / 7) as stay_window,
       max(extract(month from db.stay_date)) as stay_month,
       max(extract(dow from db.stay_date)) as stay_dow,
       
       min(extract(month from db.statistic_date)) as min_statistic_month,
       max(extract(month from db.statistic_date)) as max_statistic_month,
       
       min(db.stay_date - hs.last_holiday_date) as min_days_since_holiday,
       max(db.stay_date - hs.last_holiday_date) as max_days_since_holiday,
       avg(db.stay_date - hs.last_holiday_date) as avg_days_since_holiday,
       min(hs.next_holiday_date - db.stay_date) as min_days_until_holiday,
       min(hs.next_holiday_date - db.stay_date) as max_days_until_holiday,
       avg(hs.next_holiday_date - db.stay_date) as avg_days_until_holiday,
       min((db.statistic_date - p.open_date) / 7) as min_weeks_open,
       max((db.statistic_date - p.open_date) / 7) as max_weeks_open,
       avg((db.statistic_date - p.open_date) / 7) as avg_weeks_open,
       stddev((db.statistic_date - p.open_date) / 7) as stddev_weeks_open
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as db
    left join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as hs
        on (db.stay_date = hs.stay_date)
    left join `adara-data-master-qa.harry_test.mm_holiday_dates_COPY` as hd
        on (db.stay_date = hd.holiday_date)
    left join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as m
        on (db.market = m.market)
    left join `""" + db_id + """.""" + dataset_id + """.""" + t4 + """` as p
        on (db.hotel_code = p.hotel_code) and (db.dp_id = p.dp_id)
    where db.stay_date >= '2016-01-01'::date
    group by 1,2,3,4,5
    having count(db.statistic_date) = 7
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_t2_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t1_Acc_' + str(account_id)
    t2 = 'mm_markets_Acc_' + str(account_id)
    t3 = 'mm_properties_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        t.market,
        stay_date,
        statistic_week,
        cumulative_bookings_target,
        total_bookings_target,
        stay_window,
        stay_month,
        stay_dow,
        min_statistic_month,
        max_statistic_month,
        min_days_since_holiday,
        max_days_since_holiday,
        avg_days_since_holiday,
        min_days_until_holiday,
        max_days_until_holiday,
        
        avg_days_until_holiday,
        p.number_of_rooms,
        1.0 * p.number_of_rooms / nullif(m.number_of_rooms,0) as room_share_market,
        
        min_weeks_open,
        max_weeks_open,
        avg_weeks_open,
        stddev_weeks_open,
        replace(lower(trim(regexp_replace(chain_scale, '[^a-zA-Z\d\s:]',''))),' ','_') as chain_scale,
        floors,
        replace(lower(trim(regexp_replace(p.location, '[^a-zA-Z\d\s:]',''))),' ','_') as location,
        
        has_indoor_corridors,
        has_restaurant,
        has_convention,
        has_conference,
        has_spa,
        has_single_meeting_space,
        largest_meeting_space,
        total_meeting_space,
        is_resort,
        is_ski_resort,
        is_golf_resort,
        is_all_suites,
        is_casino,
        
        replace(lower(trim(regexp_replace(p.price, '[^a-zA-Z\d\s:]',''))),' ','_') as price,
        single_low_rate,
        single_high_rate,
        double_low_rate,
        double_high_rate,
        suite_low_rate,
        suite_high_rate
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as t
    join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as m
        using (market)
    join `""" + db_id + """.""" + dataset_id + """.""" + t3 + """` as p
        using (hotel_code, dp_id)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_t3_hotel_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        (stay_date - floor((stay_date - statistic_date) / 7.0) * 7) as statistic_week,
        
        max(nvl(h.hotel_code_searches, 0)) as max_hotel_code_searches,
        min(nvl(h.hotel_code_searches, 0)) as min_hotel_code_searches,
        avg(nvl(h.hotel_code_searches, 0)) as avg_hotel_code_searches,
        
        max(nvl(h.hotel_market_searches, 0)) as max_hotel_market_searches,
        min(nvl(h.hotel_market_searches, 0)) as min_hotel_market_searches,
        avg(nvl(h.hotel_market_searches, 0)) as avg_hotel_market_searches,
        
        max(nvl(h.hotel_competitive_market_searches, 0)) as max_hotel_competitive_market_searches,
        min(nvl(h.hotel_competitive_market_searches, 0)) as min_hotel_competitive_market_searches,
        avg(nvl(h.hotel_competitive_market_searches, 0)) as avg_hotel_competitive_market_searches,
        
        max(nvl(h.hotel_code_bookings, 0)) as max_hotel_code_bookings,
        min(nvl(h.hotel_code_bookings, 0)) as min_hotel_code_bookings,
        avg(nvl(h.hotel_code_bookings, 0)) as avg_hotel_code_bookings,
        
        max(nvl(h.hotel_market_bookings, 0)) as max_hotel_market_bookings,
        min(nvl(h.hotel_market_bookings, 0)) as min_hotel_market_bookings,
        avg(nvl(h.hotel_market_bookings, 0)) as avg_hotel_market_bookings,
        
        max(nvl(h.hotel_competitive_market_bookings, 0)) as max_hotel_competitive_market_bookings,
        min(nvl(h.hotel_competitive_market_bookings, 0)) as min_hotel_competitive_market_bookings,
        avg(nvl(h.hotel_competitive_market_bookings, 0)) as avg_hotel_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as h 
    where stay_date >= '2016-01-01'::date
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_t4_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t2_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_t3_hotel_Acc_' + str(account_id)

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    left outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_t5_flight_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight_Acc_' + str(account_id)

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        (stay_date - floor((stay_date - statistic_date) / 7.0) * 7) as statistic_week,
        
        max(nvl(f.flight_market_searches, 0)) as max_flight_market_searches,
        min(nvl(f.flight_market_searches, 0)) as min_flight_market_searches,
        avg(nvl(f.flight_market_searches, 0)) as avg_flight_market_searches,
        
        max(nvl(f.flight_competitive_market_searches, 0)) as max_flight_competitive_market_searches,
        min(nvl(f.flight_competitive_market_searches, 0)) as min_flight_competitive_market_searches,
        avg(nvl(f.flight_competitive_market_searches, 0)) as avg_flight_competitive_market_searches,
        
        max(nvl(f.flight_market_bookings, 0)) as max_flight_market_bookings,
        min(nvl(f.flight_market_bookings, 0)) as min_flight_market_bookings,
        avg(nvl(f.flight_market_bookings, 0)) as avg_flight_market_bookings,
        
        max(nvl(f.flight_competitive_market_bookings, 0)) as max_flight_competitive_market_bookings,
        min(nvl(f.flight_competitive_market_bookings, 0)) as min_flight_competitive_market_bookings,
        avg(nvl(f.flight_competitive_market_bookings, 0)) as avg_flight_competitive_market_bookings
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as f 
    where stay_date >= '2016-01-01'::date
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t4_Acc_' + str(account_id)
    t2 = 'mm_booking_dataset_t5_flight_Acc_' + str(account_id)

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings,
        max_flight_market_searches, min_flight_market_searches,
        avg_flight_market_searches, max_flight_competitive_market_searches,
        min_flight_competitive_market_searches, avg_flight_competitive_market_searches,
        max_flight_market_bookings, min_flight_market_bookings, avg_flight_market_bookings,
        max_flight_competitive_market_bookings, min_flight_competitive_market_bookings,
        avg_flight_competitive_market_bookings  
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """`
    left outer join `""" + db_id + """.""" + dataset_id + """.""" + t2 + """`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)

    # d
    table_name = 'mm_booking_dataset_full_Acc_' + str(account_id)
    table_ref = client_to.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_Acc_' + str(account_id)
    t2 = 'mm_markets_Acc_' + str(account_id)

    query_string = """
    select a.*,
        b.NUMBER_OF_ROOMS as market_total_rooms
    from `""" + db_id + """.""" + dataset_id + """.""" + t1 + """` as a, 
        `""" + db_id + """.""" + dataset_id + """.""" + t2 + """` as b
    where a.market = b.market
    """
    create_table_query(client_to, query_string, table_ref, '', '', True)





def main():

    print "Let's start!"

    db_id = 'adara-market-monitor-qa'
    mm_tables_dataset_id = 'mm_qa'
    # client_from = bigquery.Client(project='adara-data-master')
    # client_to = bigquery.Client(project='adara-machinelearning')
    client_to = bigquery.Client(project=db_id)

    dataset_id = 'harry_dataset'

    # ----------- Dataset actions ----------- #
    ret = dataset_exists(client_to, dataset_id)    # check if dataset already exists

    if ret:
        print 'Dataset {} on {} exists'.format(dataset_id, db_id)
    else:

        dataset_ref = client_to.dataset(dataset_id)

        dataset = bigquery.Dataset(dataset_ref)
        dataset.location = 'US'

        # raises google.api_core.exceptions.AlreadyExists
        # if already present
        client_to.create_dataset(dataset)
        print 'Dataset created'

    # ----------- Actual Queries ----------- #
    account_id = 382
    search_queries(account_id, db_id, dataset_id, mm_tables_dataset_id, client_to)

    print "Bye!"


if __name__ == "__main__":
    main()
