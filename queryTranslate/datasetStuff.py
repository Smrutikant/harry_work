from google.cloud import bigquery
from google.cloud.exceptions import NotFound
import datetime


def dataset_exists(client, dataset_id):
    print '@ dataset_exists function'

    # check if dataset exists and if not
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def delete_tables_from_dataset(client, dataset_id):
    print '@ delete_tables_from_dataset function'

    dataset_ref = client.dataset(dataset_id)

    tables = list(client.list_tables(dataset_ref))

    for table in tables:
        table_ref = client.dataset(dataset_id).table(table.table_id)

        try:
            # API request
            client.delete_table(table_ref)
            print('Table {}:{} deleted.'.format(dataset_id, table.table_id))
        except NotFound:
            print('Table {}:{} not exist.'.format(dataset_id, table.table_id))


def create_dataset(client, account_id):
    print '@ create_dataset function'

    cur_tms = str(datetime.datetime.now()).split('.')[0]
    # print str(datetime.datetime.now()).split('.')[0]
    dataset_id = "Acc" + str(account_id) + "_" + cur_tms.split(' ')[0].replace("-", "") \
                 + "t" + cur_tms.split(' ')[1].replace(":", "")
    # print dataset_id

    # Check if dataset already exists
    ret = dataset_exists(client, dataset_id)

    if ret:
        print 'Dataset' + dataset_id + 'exists'
        delete_tables_from_dataset(client, dataset_id)
    else:
        # Creating a DatasetReference using a chosen dataset ID
        dataset_ref = client.dataset(dataset_id)

        # Construct a full Dataset object to send to the API
        dataset = bigquery.Dataset(dataset_ref)

        # Specify the geographic location where the dataset should reside
        dataset.location = 'US'

        # Send the dataset to the API for creation
        # Raises google.api_core.exceptions.Conflict if the Dataset already exists within the project
        dataset = client.create_dataset(dataset)
        # print dataset # del
        print 'Dataset ' + dataset_id + ' created'


def main():

    account_id = 382

    db_id = 'adara-market-monitor-qa'

    client = bigquery.Client(project=db_id)

    create_dataset(client, account_id)


if __name__ == "__main__":
    main()
