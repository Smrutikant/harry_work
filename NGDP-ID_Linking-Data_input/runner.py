import os
import logging
import sys
import subprocess

from google.cloud import pubsub
from google.cloud import storage
from google.auth import compute_engine

import time
import datetime

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
credentials = compute_engine.Credentials()
subscriber = pubsub.SubscriberClient(credentials=credentials)

# in seconds
# timeout = 10

counter_global = 0
# file_counter = 0

input_file_queue = []
output_file_queue = []

last_run = None     # will store the timestamp of the latest successful run
allow_to_run = None     # boolean - will store if we are allowed to call graph build or not

max_file_size = 10
bucket_name = 'harry_test_storage'
output_folder_name = 'test_queue'


def init_queue():
    logging.info(u"@ init_queue")

    prefix = output_folder_name + '/'
    delimiter = '/'

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)

    # Note: Client.list_blobs requires at least package version 1.17.0.
    blobs = bucket.list_blobs(
        prefix=prefix,
        delimiter=delimiter
    )

    print("Blobs:")
    for blob in blobs:
        print(blob.name)
        if blob.name != prefix:
            # gs://bucket_name/file_name.csv
            file_path = 'gs://' + bucket_name + '/' + prefix + blob.name
            temp_dict = {'file_path': file_path, 'retries': 0, 'size': blob.size}
            input_file_queue.append(temp_dict)


def process_input_file(path_new_file):
    logging.info(u"@ process_input_file")

    try:
        client = storage.Client()
        bucket = client.get_bucket(bucket_name)

        # bucket_name_new_file = path_new_file.split('/')[2]
        temp_file_name_list = path_new_file.split('/')[3:]
        name_new_file_folder = '/'.join(temp_file_name_list)
        name_new_file = temp_file_name_list[-1]

        # bucket_new_file = client.get_bucket(bucket_name_new_file)
        # bucket_new_file = client.get_bucket(bucket_name)
        # blob_new_file = bucket_new_file.get_blob(name_new_file)
        blob_new_file = bucket.get_blob(name_new_file_folder)

        # get GCS file size
        input_file_size = blob_new_file.size
        logging.info(u"Size: {} bytes".format(input_file_size))

        queue_last_file_size = input_file_queue[-1].get('size')
        logging.info(u"Size of last file in queue: {} bytes".format(queue_last_file_size))

        # if the last file on queue has larger or equal size with max
        if queue_last_file_size >= max_file_size:
            logging.info(u"@ if the last file on queue has larger or equal size with max")

            # if the incoming file is less or equal than the max, we just put it on the queue
            if input_file_size <= max_file_size:
                logging.info(u"@ if the incoming file is less or equal than the max, we just put it on the queue")

                destination_blob_name = output_folder_name + '/' + name_new_file
                blob_copy = bucket.copy_blob(blob_new_file, bucket, destination_blob_name)
                blob_new_file.delete()
                new_file_new_path = 'gs://' + bucket_name + '/' + destination_blob_name

                temp_dict = {'file_path': new_file_new_path, 'retries': 0, 'size': blob_copy.size}
                input_file_queue.append(temp_dict)
                logging.info(u"Size: {} bytes".format(input_file_size))

            # else if it is larger than the max size
            else:
                logging.info(u"@ else if it is larger than the max size")
                # new_created_files = split_file(path_new_file, bucket_new_file, name_new_file, blob_new_file)
                new_created_files = split_file(path_new_file, bucket, name_new_file_folder, blob_new_file)

                # add new created files at the queue
                for file_name in new_created_files:
                    temp_blob = bucket.get_blob(file_name)

                    # temp_gcs_file_name = bucket_name_new_file + '/' + file_name
                    temp_gcs_file_name = bucket_name + '/' + file_name
                    temp_gcs_file_path = 'gs://' + temp_gcs_file_name
                    temp_dict = {'file_path': temp_gcs_file_path, 'retries': 0, 'size': temp_blob.size}
                    input_file_queue.append(temp_dict)

        # if the last file on queue has size less than the max size
        else:
            logging.info(u"@ if the last file on queue has size less than the max size")

            # merge files
            # blobs_to_merge = []
            #
            # last_queue_file_path = input_file_queue[-1].get('file_path')
            # logging.info(u"last_queue_file_path file: {}".format(last_queue_file_path))
            # t_bucket_name = last_queue_file_path.split('/')[2]
            # logging.info(u"t_bucket_name file: {}".format(t_bucket_name))
            # t_temp_file_name_list = last_queue_file_path.split('/')[3:]
            # logging.info(u"t_temp_file_name_list file: {}".format(t_temp_file_name_list))
            # t_file_name = '/'.join(t_temp_file_name_list)
            # logging.info(u"t_file_name: {}".format(t_file_name))
            #
            # t_bucket = client.get_bucket(t_bucket_name)
            # logging.info(u"t_bucket: {}".format(t_bucket))
            # t_blob = t_bucket.get_blob(t_file_name)
            # logging.info(u"t_blob: {}".format(t_blob))
            #
            # blobs_to_merge.append(t_blob)
            # blobs_to_merge.append(blob_new_file)
            # logging.info(u"blobs_to_merge: {}".format(blobs_to_merge))
            #
            # # file name of the new file
            # temp_file_name = t_file_name[:-4]
            # merged_file_name = temp_file_name + '_m.csv'
            # logging.info(u"merged_file_name: {}".format(merged_file_name))
            #
            # # create new blob for merging
            # merged_blob = t_bucket.blob(merged_file_name)
            # logging.info(u"merged_blob: {}".format(merged_blob))
            # merged_blob.content_type = 'application/octet-stream'
            # logging.info(u"merged_blob.content_type: {}".format(merged_blob.content_type))
            #
            # # merge blobs
            # merged_blob.compose(blobs_to_merge)
            #
            # merged_destination_uri = 'gs://{}/{}'.format(t_bucket_name, merged_file_name)
            #
            # logging.info(u"merged file: {}".format(merged_destination_uri))

            merged_destination_uri, merged_file_name, merged_blob = merge_files(bucket=bucket,
                                                                                blob_new_file=blob_new_file)

            del input_file_queue[-1]

            if merged_blob.size <= max_file_size:
                temp_dict = {'file_path': merged_destination_uri, 'retries': 0, 'size': merged_blob.size}
                input_file_queue.append(temp_dict)

            else:
                new_created_files = split_file(file_path=merged_destination_uri,
                                               bucket=bucket,
                                               file_name=merged_file_name,
                                               blob=merged_blob)

                # add new created files at the queue
                for file_name in new_created_files:
                    temp_blob = bucket.get_blob(file_name)

                    # temp_gcs_file_name = t_bucket_name + '/' + file_name
                    temp_gcs_file_name = bucket_name + '/' + file_name
                    temp_gcs_file_path = 'gs://' + temp_gcs_file_name
                    temp_dict = {'file_path': temp_gcs_file_path, 'retries': 0, 'size': temp_blob.size}
                    input_file_queue.append(temp_dict)

        logging.info('--> Updated queue: {}.'.format(input_file_queue))

    except Exception as e:
        logging.info('Exception: {}.'.format(e))
        pass


def split_file(file_path, bucket, file_name, blob):
    logging.info(u"@ split_file")

    new_created_files = []

    # Download file locally (to disk)
    current_directory = os.getcwd()
    logging.info(u"Current local directory: {}".format(current_directory))

    # get the folder(s) of the file_name
    # file_name_folder_list = file_name.split('/')[:-1]
    # file_name_folder = '/'.join(file_name_folder_list)
    # logging.info(u"File name with folder: {}".format(file_name_folder))

    # get only the file name without the folder
    file_name_no_folder = file_name.split('/')[-1]
    logging.info(u"File name without folder: {}".format(file_name_no_folder))
    local_file_dir = current_directory + '/' + file_name_no_folder
    blob.download_to_filename(local_file_dir)
    logging.info("Downloaded file from " + file_path + " to " + local_file_dir)

    # Split file main block
    file_counter = 0
    temp_file_name = file_name_no_folder[:-4]

    # Open recently downloaded file
    with open(local_file_dir, 'r') as infile:

        # create name for new splitted file
        file_counter = file_counter + 1
        temp_local_file_name = temp_file_name + '_p' + str(file_counter) + '.csv'
        temp_local_file_dir = current_directory + '/' + temp_local_file_name

        # open new new splitted file
        f = open(temp_local_file_dir, 'a+')
        for line in infile:

            logging.info(u"line: {}".format(line))
            logging.info(u"running size: {} bytes".format(os.stat(temp_local_file_dir).st_size))

            # if we have reached/passed the max file size
            if os.stat(temp_local_file_dir).st_size >= max_file_size:
                f.close()

                # upload newly created file to GCS
                to_file_name = output_folder_name + '/' + temp_local_file_name
                upload_file(bucket=bucket, to_file_name=to_file_name, from_file_path=temp_local_file_dir)

                new_created_files.append(to_file_name)

                # create name for new splitted file
                file_counter = file_counter + 1
                temp_local_file_name = temp_file_name + '_p' + str(file_counter) + '.csv'
                temp_local_file_dir = current_directory + '/' + temp_local_file_name

                # open new splitted file
                f = open(temp_local_file_dir, 'a+')
                f.write(line)
                f.flush()

            # if we have space to write - by writing one more line, it won't exceed a lot the max size
            else:
                f.write(line)
                f.flush()

        f.close()

        # upload newly created file to GCS
        to_file_name = output_folder_name + '/' + temp_local_file_name
        upload_file(bucket=bucket, to_file_name=to_file_name, from_file_path=temp_local_file_dir)

        new_created_files.append(to_file_name)

    return new_created_files


def upload_file(bucket, to_file_name, from_file_path):
    logging.info(u"@ upload_file")
    # upload to GCS

    blob = bucket.blob(to_file_name)
    blob.upload_from_filename(filename=from_file_path)

    logging.info("uploaded file from " + from_file_path + " to " + to_file_name)


def merge_files(bucket, blob_new_file):
    blobs_to_merge = []

    last_queue_file_path = input_file_queue[-1].get('file_path')
    # t_bucket_name = last_queue_file_path.split('/')[2]
    temp_file_name_list = last_queue_file_path.split('/')[3:]
    file_name = '/'.join(temp_file_name_list)

    # t_bucket = client.get_bucket(t_bucket_name)
    blob = bucket.get_blob(file_name)

    blobs_to_merge.append(blob)
    blobs_to_merge.append(blob_new_file)
    logging.info(u"Blobs to merge: {}".format(blobs_to_merge))

    # file name of the new file
    temp_file_name = file_name[:-4]
    merged_file_name = temp_file_name + '_m.csv'
    logging.info(u"Merged file name: {}".format(merged_file_name))

    # create new blob for merging
    merged_blob = bucket.blob(merged_file_name)
    merged_blob.content_type = 'application/octet-stream'

    # merge blobs
    merged_blob.compose(blobs_to_merge)

    merged_destination_uri = 'gs://{}/{}'.format(bucket_name, merged_file_name)

    logging.info(u"Merged file: {}".format(merged_destination_uri))

    # Delete blobs that merged
    blob.delete()
    blob_new_file.delete()

    return merged_destination_uri, merged_file_name, merged_blob


########################################################################################################################
def create_job_from_template():

    global counter_global
    for i in range(10):
        time.sleep(5)
        logging.info(u"i = {}".format(i))
        counter_global = counter_global + i

    logging.info(u"counter_global = {}".format(counter_global))


def delete_blob(path):
    """Deletes a blob from the bucket."""
    try:
        client = storage.Client()

        bucket_name = path.split('/')[2]
        file_name_list = path.split('/')[3:]
        file_name = '/'.join(file_name_list)

        bucket = client.get_bucket(bucket_name)

        blob = bucket.get_blob(file_name)

        blob.delete()

        print("Blob {} deleted.".format(file_name))

    except Exception as e:
        logging.info('Exception: {}.'.format(e))
        pass


def write_file(path):
    try:

        client = storage.Client()

        uploading_string = 'Kobe Bryant'
        gcs_file_id = 'write_test.txt'
        gcs_bucket_id = path.split('/')[2]

        bucket = client.get_bucket(gcs_bucket_id)  # API request

        blob = bucket.blob(gcs_file_id)

        blob.upload_from_string(uploading_string)

    except Exception as e:
        logging.info('Exception: {}.'.format(e))
        pass


def add(path):
    try:
        client = storage.Client()

        bucket_name = path.split('/')[2]
        file_name_list = path.split('/')[3:]
        file_name = '/'.join(file_name_list)

        bucket = client.get_bucket(bucket_name)

        blob = bucket.get_blob(file_name)

        blob_string = blob.download_as_string()

        print(blob_string)

        input_file_queue.append(path)

        logging.info('Queue: {}.'.format(input_file_queue))

    except Exception as e:
        logging.info('Exception: {}.'.format(e))
        pass


def remove(path):
    try:
        client = storage.Client()

        bucket_name = path.split('/')[2]
        file_name_list = path.split('/')[3:]
        file_name = '/'.join(file_name_list)

        bucket = client.get_bucket(bucket_name)

        blob = bucket.get_blob(file_name)

        blob_string = blob.download_as_string()

        print(blob_string)

        if path in input_file_queue:
            input_file_queue.remove(path)
        else:
            logging.info('Element does not exist in the list{}.')

        logging.info('Queue: {}.'.format(input_file_queue))

    except Exception as e:
        logging.info('Exception: {}.'.format(e))
        pass
########################################################################################################################


if __name__ == "__main__":

    init_queue()
    logging.info('Init queue: {}.'.format(input_file_queue))

    last_run = datetime.datetime.now().timestamp()
    allow_to_run = False

    subscription_path = subscriber.subscription_path(
        os.environ.get("PROJECT_ID"), os.environ.get("SUBSCRIPTION_NAME"))

    logging.info(subprocess.check_output(
        ['/root/google-cloud-sdk/bin/gcloud', 'config', 'set', 'project', os.environ.get("PROJECT_ID")]))


    def callback(message):
        """
        Function that listens to the PubSub message from topic "gams-plp-runner"
        :param message: PubSub message
        :return: No return
        """

        try:
            logging.info('Received message: {}'.format(message))

            message_data = message.data.decode()
            logging.info(u'message: {}'.format(message_data))

            if message_data == 'ssot':
                logging.info(u'actions for ssot')
                path = message.attributes.get('PATH')
                process_input_file(path)

            # elif message_data == 'print':
            #     logging.info(u'actions for ssot')
            #     path = message.attributes.get('PATH')
            #     print_file(path)

            elif message_data == 'id_graph':
                logging.info(u'actions for id-graph')

            elif message_data == 'delete_file':
                logging.info(u'actions for delete')
                path = message.attributes.get('PATH')
                delete_blob(path)

            elif message_data == 'write':
                logging.info(u'actions for write')
                path = message.attributes.get('PATH')
                write_file(path)

            elif message_data == 'add':
                logging.info(u'actions for add')
                path = message.attributes.get('PATH')
                add(path)

            elif message_data == 'remove':
                logging.info(u'actions for remove')
                path = message.attributes.get('PATH')
                remove(path)

            else:
                logging.info(u'Wrong message! ! !')

            create_job_from_template()

        finally:
            # TODO: change that
            message.ack()

        # message.ack()

    flow_control = pubsub.types.FlowControl(max_messages=1)
    future = subscriber.subscribe(subscription_path, callback=callback, flow_control=flow_control)
    logging.info('Listening for messages on {}'.format(subscription_path))
    # Blocks the thread while messages are coming in through the stream. Any
    # exceptions that crop up on the thread will be set on the future.
    try:
        # When timeout is unspecified, the result method waits indefinitely.
        # future.result(timeout=timeout)
        future.result()
    except Exception as e:
        print(
            'Listening for messages on {} threw an Exception: {}.'.format(
                subscription_path, e))
