import logging
import datetime
import os

from google.cloud import bigquery
from google.cloud.exceptions import NotFound

from .search_book_queries import *
from .origin_market_queries import *


logger = logging.getLogger(__name__)


def dataset_exists(client, dataset_id):
    logger.info('@ dataset_exists function')

    # check if dataset exists
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def create_dataset(client, account_id):
    logger.info('@ create_dataset function')
    logger.info(u'Creating dataset for account {}...'.format(account_id))

    current_ts = str(datetime.datetime.now()).split('.')[0]
    # print cur_tms
    dataset_id = "acct_" + str(account_id) + "__ts_" + current_ts.split(' ')[0].replace("-", "") \
                 + "t" + current_ts.split(' ')[1].replace(":", "")
    # dataset_id = 'XXX'
    # print dataset_id

    # Check if dataset already exists
    exists = dataset_exists(client, dataset_id)

    if exists:
        logger.info(u'Dataset {} exists!'.format(dataset_id))
        # delete_tables_from_dataset(client, dataset_id)
    else:
        # Creating a DatasetReference using a chosen dataset ID
        dataset_ref = client.dataset(dataset_id)

        # Construct a full Dataset object to send to the API
        dataset = bigquery.Dataset(dataset_ref)

        # Specify the geographic location where the dataset should reside
        dataset.location = 'US'

        # Send the dataset to the API for creation
        # Raises google.api_core.exceptions.Conflict if the Dataset already exists within the project
        dataset = client.create_dataset(dataset)
        # print dataset # del
        logger.info(u'Dataset {} created!'.format(dataset_id))

        # set expiration day for the dataset
        one_day_ms = 24 * 60 * 60 * 1000  # in milliseconds
        one_month_ms = one_day_ms * 30
        # dataset.default_table_expiration_ms = one_month_ms
        dataset.default_table_expiration_ms = one_day_ms * 2    # TODO: replace with above
        dataset = client.update_dataset(
            dataset, ['default_table_expiration_ms'])  # API request

    return dataset_id, current_ts


def search_book_query_driver(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    logger.info('@ search_book_query_driver function')
    logger.info(u'Running search book query for account {}...'.format(account_id))

    mm_markets_of_interest(client, account_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_markets_sql_text(client, account_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_market_hotel_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_market_city_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_us_properties(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_properties_base_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_property_distance_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_property_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_property_word(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_property_match_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_property_match(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_properties(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_markets(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_book(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_search(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_dirty(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ihg_alos_map(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ihg_ekv_hotel_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ihg_ekv_hotel_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel_mid(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ihg_ekv_hotel_insert(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_market_airport_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_destination_airport(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_distinct_destination_airport(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_flight_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_flight_book(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_flight_search(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_flight_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_ekv_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_base_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_base_t2(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_base_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_metric_calculation_base_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_market_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_metro_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_metro_market_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_metro_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_base_metrocode_map(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_metric_calculation_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_metric_calculation_point(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_metric_calculation_cumulative(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_target(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_dataset_base_feature(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_hotel_summary_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_hotel_summary(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_hc_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_search_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_market_stay_stat_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_search_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_search_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_stay_stat_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_search_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_m_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_cm_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_hc_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_book_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_book_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_book_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_hotel_book_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_m_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel_cm_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_flight_summary_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_flight_summary_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_flight_summary(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_search_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_search_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_flight_m_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_search_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_search_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_flight_cm_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_book_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_book_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_book_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_flight_book_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_flight_m_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_flight_cm_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_holidays(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_t2(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_t3_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_t5_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_booking_dataset_full(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)


def origin_market_query_driver(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    logger.info('@ origin_market_query_driver function')
    logger.info(u'Running origin market query for account {}...'.format(account_id))

    mm_metric_aggregate_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_aggregate(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_analytics_a(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_analytics_b(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_analytics_c(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_analytics_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_metric_analytics(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    mm_origin_market_report(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables)


def run_bq_queries(account_id):
    logger.info('@ run_bq_queries function')
    logger.info(u'Running queries for account {}...'.format(account_id))

    # account_id = 382    # Ocean City
    # account_id = 26     # Savannah
    # account_id = 245    # Tampa

    # project we run DAG and where create new dataset/tables
    # adara-market-monitor-qa
    project_id = os.environ['PROJECT_ID']

    # project from where we get data for the queries
    # adara-data-master-qa
    project_id_exist_tables = os.environ['PROJECT_ID_EXIST_TABLES']

    # dataset from where we get data for the queries
    # harry_test
    dataset_id_exist_tables = os.environ['DATASET_ID_EXIST_TABLES']

    client = bigquery.Client(project=project_id)

    # preparing new dataset
    dataset_id, current_ts = create_dataset(client, account_id)

    logger.info(dataset_id)

    # running queries
    search_book_query_driver(client, account_id, project_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    origin_market_query_driver(client, account_id, project_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables)

    return dataset_id, current_ts
