import logging

from google.cloud import bigquery


logger = logging.getLogger(__name__)


def create_table_query(client, query_string, table_ref):
    logger.info('@ create_table_query function')
    job_config = bigquery.QueryJobConfig()
    job_config.destination = table_ref

    # submit API request to start the query
    query_job = client.query(
        query_string, location='US', job_config=job_config)

    query_job.result()

    logger.info(u'Query results loaded to table {}'.format(table_ref.path))


def mm_metric_aggregate_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_aggregate_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'

    query_string = """
    with t1 as (
        select h.market,
            l.metrocode,
            date_trunc(h.checkin_date, month) as stay_month,
            h.activity_type,
            count(*) as events,
            sum(h.avg_daily_rate_usd * h.trip_duration) as adr_numerator,
            sum( if (h.avg_daily_rate_usd is not null and h.trip_duration is not null, h.trip_duration, null) )
                as adr_denominator,
            sum(h.trip_duration) as alos_numerator,
            sum( if (h.trip_duration is not null, 1, 0) ) as alos_denominator,
            sum(h.number_of_travelers) as anot_numerator,
            sum( if (h.number_of_travelers is not null, 1, 0) ) as anot_denominator
        from `{project_id}.{dataset_id}.{t1}` as h
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.location` as l
            on (h.location_id = l.id)
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.metrocodes` as m
            on (l.metrocode = m.id)
        group by 1,2,3,4
    )
    select *,
        adr_numerator as ar_numerator,
        events as ar_denominator
    from t1
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_aggregate(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_aggregate'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_aggregate_base'
    t2 = 'mm_metric_base_metrocode_map'

    query_string = """
    select ma.market,
        mm.metrocode_group,
        ma.stay_month,
        ma.activity_type,
        sum(ma.events) as events,
        sum(ma.adr_numerator) as adr_numerator,
        sum(ma.adr_denominator) as adr_denominator,
        sum(ma.alos_numerator) as alos_numerator,
        sum(ma.alos_denominator) as alos_denominator,
        sum(ma.anot_numerator) as anot_numerator,
        sum(ma.anot_denominator) as anot_denominator,
        sum(ma.ar_numerator) as ar_numerator,
        sum(ma.ar_denominator) as ar_denominator
    from `{project_id}.{dataset_id}.{t1}` as ma
    join `{project_id}.{dataset_id}.{t2}` as mm
        using (metrocode)
    where ma.stay_month >= date('2016-01-01')
    group by 1,2,3,4
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_aggregate'

    query_string = """
    select ma.market,
        ma.metrocode_group,
        extract(month from ma.stay_month) as stay_month,
        sum(ma.events) as bookings,
        sum(ma.adr_numerator) as adr_numerator,
        sum(ma.adr_denominator) as adr_denominator,
        sum(ma.alos_numerator) as alos_numerator,
        sum(ma.alos_denominator) as alos_denominator,
        sum(ma.anot_numerator) as anot_numerator,
        sum(ma.anot_denominator) as anot_denominator,
        sum(ma.ar_numerator) as ar_numerator,
        sum(ma.ar_denominator) as ar_denominator
    from `{project_id}.{dataset_id}.{t1}` as ma
    where ma.activity_type = 'book'
        and ma.stay_month < date_trunc(current_date, month)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_analytics_a(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_analytics_a'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_aggregate'
    t2 = 'mm_metric_base'

    query_string = """
    with t1 as (
        select ma.market,
            ma.metrocode_group,
            extract(month from ma.stay_month) as stay_month,
            sum ( case
                when ma.activity_type = 'search' then ma.events
                else null
                end) as search_sum,
            sum ( case
                when ma.activity_type = 'book' then ma.events
                else null
                end) as book_sum
        from `{project_id}.{dataset_id}.{t1}` as ma
        group by 1,2,3),
    stob as (
        select market,
            metrocode_group,
            stay_month,
            (1.0 * search_sum / book_sum) as stob
        from t1)
    select market,
        metrocode_group,
        stay_month,
        mb.bookings,
        (1.0 * mb.adr_numerator / mb.adr_denominator) as adr,
        (1.0 * mb.alos_numerator / mb.alos_denominator) as alos,
        (1.0 * mb.anot_numerator / mb.anot_denominator) as anot,
        (1.0 * mb.ar_numerator / mb.ar_denominator) as ar,
        stob.stob
    from `{project_id}.{dataset_id}.{t2}` as mb
    join stob
        using (market, metrocode_group, stay_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_analytics_b(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_analytics_b'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_analytics_a'

    query_string = """
    with m as(
        select ma.market,
            ma.stay_month,
            avg(ma.bookings) as avg_bookings,
            stddev(ma.bookings) as stddev_bookings,
            avg(ma.adr) as avg_adr,
            stddev(ma.adr) as stddev_adr,
            avg(ma.alos) as avg_alos,
            stddev(ma.alos) as stddev_alos,
            avg(ma.anot) as avg_anot,
            stddev(ma.anot) as stddev_anot,
            avg(ma.ar) as avg_ar,
            stddev(ma.ar) as stddev_ar,
            avg(ma.stob) as avg_stob,
            stddev(ma.stob) as stddev_stob
        from `{project_id}.{dataset_id}.{t1}` as ma
        group by 1,2)
    select ma.market,
        ma.metrocode_group,
        ma.stay_month,
        ma.bookings,
        (1.0 * (ma.bookings - m.avg_bookings) / (m.stddev_bookings)) as bookings_index,
        ma.adr,
        (1.0 * (ma.adr - m.avg_adr) / (m.stddev_adr)) as adr_index,
        ma.alos,
        (1.0 * (ma.alos - m.avg_alos) / (m.stddev_alos)) as alos_index,
        ma.anot,
        (1.0 * (ma.anot - m.avg_anot) / (m.stddev_anot)) as anot_index,
        ma.ar,
        (1.0 * (ma.ar - m.avg_ar) / (m.stddev_ar)) as ar_index,
        ma.stob,
        (1.0 * (ma.stob - m.avg_stob) / (m.stddev_stob)) as stob_index
    from `{project_id}.{dataset_id}.{t1}` as ma
    join m
        using (market, stay_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_analytics_c(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_analytics_c'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_analytics_b'

    query_string = """
    with t as (
        select ma.*,
            ( (ma.bookings_index * 0.5) + (ma.adr_index * 0.2) + (ma.alos_index * 0.1) +
                (ma.anot_index * 0.1) - (ma.stob_index * 0.1) ) as index
        from `{project_id}.{dataset_id}.{t1}` as ma
    )
    select *,
        row_number() over(
            partition by market, stay_month
            order by index desc
        ) as index_rank
    from t
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_analytics_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_analytics_d'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_analytics_c'

    query_string = """
    with i as (
        select ma.market,
            ma.stay_month,
            min(ma.index) as min_index,
            max(ma.index) as max_index
        from `{project_id}.{dataset_id}.{t1}` as ma
        group by 1,2)
    select ma.market,
        ma.metrocode_group,
        ma.stay_month,
        ma.bookings,
        ma.bookings_index,
        ma.adr,
        ma.adr_index,
        ma.alos,
        ma.alos_index,
        ma.anot,
        ma.anot_index,
        ma.ar,
        ma.ar_index,
        ma.stob,
        ma.stob_index,
        ma.index,
        ma.index_rank,
        (1.0 * (ma.index - i.min_index) / (i.max_index - i.min_index)) as unit_interval_index
    from `{project_id}.{dataset_id}.{t1}` as ma
    join i
        using (market, stay_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_analytics(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_analytics'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_analytics_d'
    t2 = 'mm_metric_aggregate'
    t3 = 'mm_metric_base'

    query_string = """
    with a as(
        select ma.market,
            ma.stay_month,
            sum(ma.unit_interval_index) as unit_interval_index
        from `{project_id}.{dataset_id}.{t1}` as ma
        group by 1,2),
    b as (
        select ma.market,
            ma.metrocode_group,
            extract(month from ma.stay_month) as stay_month,
            sum (case
                when ma.activity_type = 'search' then ma.events
                else null
                end) as stob_numerator,
            sum (case
                when ma.activity_type = 'book' then ma.events
                else null
                end) as stob_denominator
        from `{project_id}.{dataset_id}.{t2}` as ma
       group by 1,2,3)
    select ma.market,
        ma.metrocode_group,
        ma.stay_month,
        mb.bookings,
        ma.bookings_index,
        mb.adr_numerator,
        mb.adr_denominator,
        ma.adr_index,
        mb.alos_numerator,
        mb.alos_denominator,
        ma.alos_index,
        mb.anot_numerator,
        mb.anot_denominator,
        ma.anot_index,
        mb.ar_numerator,
        mb.ar_denominator,
        ma.ar_index,
        b.stob_numerator,
        b.stob_denominator,
        ma.stob_index,
        ma.index,
        ma.index_rank,
        ma.unit_interval_index,
        (1.0 * ma.unit_interval_index / a.unit_interval_index) as index_share
    from `{project_id}.{dataset_id}.{t1}` as ma
    join a
        using (market, stay_month)
    join `{project_id}.{dataset_id}.{t3}` as mb using (market, metrocode_group, stay_month)
       join b using (market, metrocode_group, stay_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_origin_market_report(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_origin_market_report'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_base_metrocode_map'
    t2 = 'mm_metric_analytics'
    t3 = 'mm_metric_metro_coordinates'

    query_string = """
    with mc as (
        select mc.metrocode_group,
            min(mc.distance_miles) as min_distance_miles,
            max(mc.distance_miles) as max_distance_miles
        from `{project_id}.{dataset_id}.{t1}` as mc
        group by 1)
    select ma.market as market,
        ma.metrocode_group as metrocode_group_id,
        if ( m.id is not null, m.name,
            concat("Group [", cast((round(mc.min_distance_miles, 0)) as string), ", ",
            cast((round(mc.max_distance_miles, 0)) as string), "]") ) as metrocode_group_name,
        ma.stay_month as stay_month_id,
        case 
            when ma.stay_month = 1 then 'January'
            when ma.stay_month = 2 then 'February'
            when ma.stay_month = 3 then 'March'
            when ma.stay_month = 4 then 'April'
            when ma.stay_month = 5 then 'May'
            when ma.stay_month = 6 then 'June'
            when ma.stay_month = 7 then 'July'
            when ma.stay_month = 8 then 'August'
            when ma.stay_month = 9 then 'September'
            when ma.stay_month = 10 then 'October'
            when ma.stay_month = 11 then 'November'
            when ma.stay_month = 12 then 'December'
        end
            as stay_month_name,
        ma.bookings as bookings,
        ma.bookings_index as bookings_index,
        (1.0 * ma.adr_numerator / ma.adr_denominator) as adr,
        ma.adr_index as adr_index,
        (1.0 * ma.alos_numerator / ma.alos_denominator) as alos,
        ma.alos_index as alos_index,
        (1.0 * ma.anot_numerator / ma.anot_denominator) as anot,
        ma.anot_index as anot_index,
        (1.0 * ma.ar_numerator / ma.ar_denominator) as ar,
        ma.ar_index as ar_index,
        (1.0 * ma.stob_numerator  / ma.stob_denominator) as stob,
        ma.stob_index as stob_index,
        ma.index as index,
        ma.index_rank as index_rank,
        ma.index_share as index_share,
        if (m.id is not null, mmc.AVG_LATITUDE, null) as lat,
        if(m.id is not null, mmc.AVG_LONGITUDE, null) as lon
    from `{project_id}.{dataset_id}.{t2}` as ma
    left join mc
        on (ma.metrocode_group = mc.metrocode_group)
    left join `{project_id_exist_tables}.{dataset_id_exist_tables}.metrocodes` as m
        on (ma.metrocode_group = m.id)
    left join `{project_id}.{dataset_id}.{t3}` as mmc
        on (ma.metrocode_group = mmc.metrocode)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)










