import logging

from google.cloud import bigquery


logger = logging.getLogger(__name__)


def create_table_query(client, query_string, table_ref):
    logger.info('@ create_table_query function')
    job_config = bigquery.QueryJobConfig()
    job_config.destination = table_ref

    # submit API request to start the query
    query_job = client.query(
        query_string, location='US', job_config=job_config)

    query_job.result()

    logger.info(u'Query results loaded to table {}'.format(table_ref.path))


def query_with_return(client, query_string):
    logger.info('@ query_with_return function')

    job_config = bigquery.QueryJobConfig()

    # submit API request to start the query
    query_job = client.query(
        query_string, location='US', job_config=job_config)

    results = query_job.result()
    results_list = list(results)

    # for row in results:
    #     logger.info(row.id)
    #     logger.info("{}".format(row.market_id))

    logger.info(results_list)

    return results_list


def create_table_schema(client, schema, table_ref):
    logger.info('@ create_table_schema function')

    table = bigquery.Table(table_ref, schema=schema)
    client.create_table(table)
    logger.info('Table {} has been created'.format(table_ref.path))


def mm_markets_of_interest(client, account_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_markets_of_interest'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    with primary_market as (
        select id as primary_market_id
        from `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_d`
        where account_id = {account_id}
        and status_id = 1
    ),
    competitive_markets as (
        select competitive_market_id
        from `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_hierarchy`
        join primary_market
        using (primary_market_id)
        where status_id = 1
    )
    select primary_market_id as market_id from primary_market
    union all
    select competitive_market_id as market_id from competitive_markets ;
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        account_id=str(account_id)
    )
    create_table_query(client, query_string, table_ref)


def mm_markets_sql_text(client, account_id, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_markets_sql_text'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    with T1 as (
        select id as primary_market_id
        from `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_d`
        where account_id = {account_id}
        and status_id = 1
    ),
    T2 as (
        select competitive_market_id id
        from T1
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_hierarchy`
            using (primary_market_id)
        where status_id = 1
        union all
        select primary_market_id id from T1)
    select id market_id,
        sql_text
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_d` as m
    join T2
        using (id)
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        account_id=str(account_id)
    )
    create_table_query(client, query_string, table_ref)


def mm_market_hotel_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    # Get a list of all the market_id for this account
    t1 = 'mm_markets_sql_text'

    query_string = """
    select market_id, sql_text
    from `{project_id_2}.{dataset_id}.{t1}`
    """.format(
        project_id_2=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    market_ids_sql_texts = query_with_return(client, query_string)

    # Create new table
    table_name = 'mm_market_hotel_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)

    counter = 0
    query_string = 'with '
    for row in market_ids_sql_texts:
        temp_string = """T{market_id} as (
            select {market_id} as market_id, str_number
            from `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_hotel_str_d`
            where {sql_text}
        )
        """.format(
            market_id=row.market_id,
            project_id_exist_tables=project_id_exist_tables,
            dataset_id_exist_tables=dataset_id_exist_tables,
            sql_text=row.sql_text
        )

        if counter < (len(market_ids_sql_texts) - 1):
            temp_string = temp_string + ','

        query_string = query_string + temp_string

        counter += 1

    counter = 0
    for row in market_ids_sql_texts:
        temp_string = """
        select * from T{market_id}
        """.format(
            market_id=row.market_id
        )

        if counter < (len(market_ids_sql_texts) - 1):
            temp_string = temp_string + 'union all'

        query_string = query_string + temp_string

        counter += 1

    logging.info(query_string)
    logging.info(len(query_string))

    if len(query_string) > 6:
        # if we have data to insert to mm_market_hotel_mapping table
        create_table_query(client, query_string, table_ref)
    else:
        # if we do not have data, we just create an empty table
        schema = [
            bigquery.SchemaField('market_id', 'INTEGER', mode='REQUIRED'),
            bigquery.SchemaField('str_number', 'INTEGER', mode='REQUIRED'),
        ]
        create_table_schema(client, schema, table_ref)


def mm_market_city_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_market_city_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_hotel_mapping'
    t2 = 'mm_markets_of_interest'

    query_string = """
    select distinct m.market_id,
        d.name as market,
        lower(h.city) as city,
        lower(h.state) as state_code,
        case
            when lower(h.mailing_country) = 'united states' then 'us'
            else 'n/a'
        end as country_code
        from `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_hotel_str_d` as h
        join `{project_id}.{dataset_id}.{t1}` as m
            using (str_number)
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_d` as d
            on m.market_id = d.id
        join `{project_id}.{dataset_id}.{t2}` as mi
            on m.market_id = mi.market_id
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_us_properties(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_us_properties'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    select p.id,
        p.advertiser_id,
        lower(trim(p.external_id)) as external_id,
        p.brand_id,
        p.external_brand_id,
        p.company_id,
        p.external_company_id,
        lower(trim(p.name)) as name,
        p.lat,
        p.lon,
        lower(trim(p.address_1)) as address_1,
        lower(trim(p.address_2)) as address_2,
        lower(trim(p.city)) as city,
        case
            when length(lower(trim(p.state))) > 2 and usl.code is not null then lower(trim(usl.code))
            when lower(trim(p.state)) = 'new york state' then 'ny'
            when lower(trim(p.state)) = 'washington d.c.' then 'dc'
            when lower(trim(p.state)) = 'd.c.' then 'dc'
            when lower(trim(p.state)) = 'island of hawaii' then 'hi'
            else lower(trim(p.state))
            end state,
        'us' as country,
        p.zip,
        p.airport_code
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.properties` as p
    left join `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_us_state_lookup` as usl
        on (lower(trim(p.state)) = lower(trim(usl.name)))
    where lower(trim(p.country)) in ('us','united states','usa')
        and (
        (p.airport_code is not null and p.external_id != p.airport_code)
        or (p.airport_code is null)
        )
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables
    )
    create_table_query(client, query_string, table_ref)


def mm_properties_base_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_properties_base_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_us_properties'
    t2 = 'mm_market_city_mapping'

    query_string = """
    select distinct
        a.dp_id,
        p.name as hotel_name,
        lower(trim(p.external_id)) as hotel_code,
        p.lat as latitude,
        p.lon as longitude,
        mm.city,
        mm.state_code,
        mm.country_code,
        mm.market
        from `{project_id}.{dataset_id}.{t1}` as p
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.advertisers` as a
            on (p.advertiser_id = a.id)
        join `{project_id}.{dataset_id}.{t2}` as mm
            on if(mm.city is not null, (p.city = mm.city), null)
            and if(mm.state_code is not null, (p.state = mm.state_code), null)
            and if(mm.country_code is not null, (p.country = mm.country_code), null)
            where a.category_id = 1
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_property_distance_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_property_distance_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties_base_staging'

    query_string = """
    with p_id as (
        select p.hotel_name,
            row_number() over (order by p.hotel_name) as id
        from `{project_id}.{dataset_id}.{t1}` as p
        group by 1
    ),
    t2 as (
        select distinct
        p.hotel_code,
        p.dp_id as p_dp_id,
        p_id.id as p_id,
        trim(REGEXP_REPLACE(lower(p.hotel_name), r"[^a-zA-Z0-9_]+", ' ')) as p_hotel_name,
        p.latitude as p_latitude,
        p.longitude as p_longitude,
        hd.parent_company,
        case
            -- is this list exhaustive? when should we update it?
            when hd.parent_company = 'Wyndham Worldwide' then 970
            when hd.parent_company = 'Choice Hotels International' then 1040
            when hd.parent_company = 'Marriott International' then 1057
            when hd.parent_company = 'Intercontinental Hotels Group' then 1168
            when hd.parent_company = 'Hilton Worldwide' then 820
            when hd.parent_company = 'Accor Company' then 1221
            when hd.parent_company = 'Best Western Company' then 1837
            when hd.parent_company = 'Best Western Hotels & Resorts' then 1837
            when hd.parent_company = 'Hyatt' then 1195
            when hd.parent_company = 'Shangri-La Hotels' then 2043
            when hd.parent_company = 'Starwood Hotels & Resorts' then 1062
            when hd.parent_company = 'LQ Management LLC' then 1090
            when hd.parent_company = 'Extended Stay Hotels' then 1306
        end hd_dp_id,
        hd.str_number as hd_id,
        trim(REGEXP_REPLACE(lower(hd.hotel_name), r"[^a-zA-Z0-9_]+", ' ')) as hd_hotel_name,
        hd.latitude as hd_latitude,
        hd.longitude as hd_longitude,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((p.latitude - hd.latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((hd.latitude) * ACOS(-1) /180)) *
                    (cos((p.latitude) * ACOS(-1) /180)) *
                    pow( (sin(((p.longitude - hd.longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
        from `{project_id}.{dataset_id}.{t1}` as p
        join p_id
            on (p.hotel_name = p_id.hotel_name)
        join `{project_id_exist_tables}.{dataset_id_exist_tables}.data_providers` as dp
            on (p.dp_id = dp.id),
        `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_hotel_str_d` as hd
    ),
    t3 as (
        select *
        from t2
        where distance_miles < 1.0
    )
    select t3.*,
        row_number() over(
            partition by hotel_code
            order by distance_miles desc
        ) as distance_rank
    from t3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_property_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_property_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance_base'

    query_string = """
    select row_number() over (order by hotel_code, hd_hotel_name) as id,
        pd.*
    from `{project_id}.{dataset_id}.{t1}` as pd
    where p_dp_id = hd_dp_id
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_property_word(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_property_word'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance'

    query_string = """
    with t1 as (
        select *, SPLIT(pd.p_hotel_name, " ") as v
        from `{project_id}.{dataset_id}.{t1}` as pd
    ),
    p as (
        select distinct id, p_id, hd_id, trim(value) as word
        from t1 cross join unnest(v) as value
    ),
    t2 as (
        select *, SPLIT(pd.hd_hotel_name, " ") as v
        from `{project_id}.{dataset_id}.{t1}` as pd
    ),
    hd as (
        select distinct id, p_id, hd_id, trim(value) as word
        from t2 cross join unnest(v) as value
    ),
    pw1 as (
        select id,
            p.p_id,
            hd.hd_id,
            p.word as p_word,
            hd.word as hd_word,
            p.word = hd.word as match
        from p
        left join hd
            using (id, p_id, hd_id)
    ),
    pw2 as (
        select pw1.*,
            row_number() over (
                partition by pw1.id, pw1.p_id, pw1.p_word
                order by match desc) as rank
        from pw1
    )
    select pw2.id,
        pw2.p_id,
        pw2.hd_id,
        pw2.p_word,
        pw2.hd_word
    from pw2
    where pw2.rank = 1
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_property_match_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_property_match_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_word'
    t2 = 'mm_property_distance'

    query_string = """
    with a as (
        select pw.id,
            pw.p_id,
            pw.hd_id,
            sum(if(pw.p_word = pw.hd_word,1,0)) as match,
            count(*) as records
        from `{project_id}.{dataset_id}.{t1}` as pw
        group by 1,2,3
    ),
    b as (
        select *,
            (1.0 * match / records) as match_rate
        from a
    )
    select b.id,
        pd.hotel_code,
        pd.p_dp_id,
        b.p_id,
        pd.p_hotel_name,
        pd.hd_dp_id,
        b.hd_id,
        pd.hd_hotel_name,
        b.match,
        b.records,
        b.match_rate,
        pd.distance_miles,
        row_number() over (
            partition by b.p_id
            order by b.match_rate desc, pd.distance_miles) as match_rank_p
    from b
    join `{project_id}.{dataset_id}.{t2}` as pd using (id)
    where b.match_rate != 0
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_property_match(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_property_match'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match_base'

    query_string = """
    with pm as (
        select pwa.*,
            row_number() over (
                partition by pwa.hd_id
                order by pwa.match_rate desc) as match_rank_hd
        from `{project_id}.{dataset_id}.{t1}` as pwa
        where pwa.match_rank_p = 1
    )
    select  pm.id,
        pm.hotel_code,
        pm.p_dp_id,
        pm.p_id,
        pm.p_hotel_name,
        pm.hd_dp_id,
        pm.hd_id,
        pm.hd_hotel_name,
        pm.match,
        pm.records,
        pm.match_rate,
        pm.distance_miles
    from pm
    where pm.match_rank_hd = 1
        and pm.match > 1
        and pm.records > 1
        and pm.p_dp_id = pm.hd_dp_id
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_properties(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_properties'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match'
    t2 = 'mm_properties_base_staging'

    query_string = """
    select pm.hotel_code,
        p.dp_id,
        p.hotel_name,
        p.latitude,
        p.longitude,
        p.city,
        p.state_code,
        p.country_code,
        p.market,
        -- add hotel attributes from STR data
        parse_date('%E4Y%m', cast (hd.open_date as string)) as open_date,
        hd.rooms as number_of_rooms,
        hd.chain_scale,
        hd.floors,
        hd.location,
        case
            when hd.indoor_corridors = 'Y' then 1
            when hd.indoor_corridors = 'N' then 0
            else null
        end
            as has_indoor_corridors,
        case
            when hd.restaurant = 'Y' then 1
            when hd.restaurant = 'N' then 0
            else null
        end
            as has_restaurant,
        case
            when hd.convention = 'Y' then 1
            when hd.convention = 'N' then 0
            else null
        end
            as has_convention,
        case
            when hd.conference = 'Y' then 1
            when hd.conference = 'N' then 0
            else null
        end
            as has_conference,
        case
            when hd.spa = 'Y' then 1
            when hd.spa = 'N' then 0
            else null
        end
            as has_spa,
        if(hd.largest_meeting_space = hd.total_meeting_space and hd.total_meeting_space != 0, 1, 0)
            as has_single_meeting_space,
        hd.largest_meeting_space,
        hd.total_meeting_space,
        case
            when hd.resort = 'Y' then 1
            when hd.resort = 'N' then 0
            else null
        end
            as is_resort,
        case
            when hd.ski = 'Y' then 1
            when hd.ski = 'N' then 0
            else null
        end
            as is_ski_resort,
        case
            when hd.golf = 'Y' then 1
            when hd.golf = 'N' then 0
            else null
        end
            as is_golf_resort,
        case
            when hd.all_suites = 'Y' then 1
            when hd.all_suites = 'N' then 0
            else null
        end
            as is_all_suites,
        case
            when hd.casino = 'Y' then 1
            when hd.casino = 'N' then 0
            else null
        end
            as is_casino,
        hd.price,
        hd.single_low_rate,
        hd.single_high_rate,
        hd.double_low_rate,
        hd.double_high_rate,
        hd.suite_low_rate,
        hd.suite_high_rate
    from `{project_id}.{dataset_id}.{t1}` as pm
    join `{project_id}.{dataset_id}.{t2}` as p
        on (pm.hotel_code = p.hotel_code)
            and (pm.p_dp_id = p.dp_id)
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_hotel_str_d` as hd
        on (pm.hd_id = hd.str_number)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_markets(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_markets'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_city_mapping'

    query_string = """
    select mm.market,
        count(*) as hotels,
        sum(hd.rooms) as number_of_rooms
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_hotel_str_d` as hd
    join `{project_id}.{dataset_id}.{t1}` as mm
    on (lower(trim(hd.city)) = mm.city)
        and (lower(trim(hd.state)) = mm.state_code)
    where hd.country = 'United States'
    group by 1
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties'
    t2 = 'mm_market_city_mapping'

    query_string = """
    select distinct
        h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        if(h.activity_type in ('click','search'),'search','book') as activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        ifnull(p.market, mm.market) as market,
        ifnull(h.hotel_country, p.country_code) as hotel_country,
        ifnull(h.hotel_state, p.state_code) as hotel_state,
        ifnull(h.hotel_city, p.city) as hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.ekv_hotel` as h
    left join `{project_id}.{dataset_id}.{t1}` as p
        using (dp_id, hotel_code)
    left join `{project_id}.{dataset_id}.{t2}` as mm
        on (h.hotel_city = mm.city)
        and (h.hotel_state = mm.state_code)
        and (h.hotel_country = mm.country_code)
    where h.activity_type in ('book','search','click')
        and h.event_ts < current_timestamp()
        and h.event_ts >= timestamp('2015-01-01 00:00:00')
        and h.checkin_date is not null
        and h.checkout_date >= date(2016,01,01)
        and (
            (p.hotel_code is not null and p.dp_id is not null) or
            (mm.city is not null or
            mm.state_code is not null or
            mm.country_code is not null
            )
        )
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_book(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_book'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base'

    query_string = """
    with h as (
        select h.*,
            row_number() over (
                partition by h.cookie_id,
                    h.dp_id,
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date,
                    h.checkout_date
                order by h.event_ts desc) as ix
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.activity_type = 'book'
    )
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where h.ix = 1
    and h.checkin_date >= date(h.event_ts)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_search(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_search'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base'

    query_string = """
    with h as (
        select h.*,
            timestamp_diff(h.event_ts, lag(h.event_ts, 1) over (
                partition by h.cookie_id,
                    h.dp_id,
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date,
                    h.checkout_date
                order by h.event_ts
                ), second
            ) as lag_seconds
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.activity_type = 'search'
    )
    select  h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where (h.lag_seconds > 300 or h.lag_seconds is null)
        and h.checkin_date >= date(h.event_ts)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_search'
    t2 = 'mm_ekv_hotel_book'

    query_string = """
    select * from `{project_id}.{dataset_id}.{t1}`
    union all
    select * from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_dirty(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_dirty'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_staging'

    query_string = """
    select  h.*,
        if ( (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) > 0 and
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) < 1000,
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd),
            null
            ) as avg_daily_rate_usd
    from `{project_id}.{dataset_id}.{t1}` as h
    left join `{project_id_exist_tables}.{dataset_id_exist_tables}.currencies` as cu
        on (lower(h.currency_type) = lower(cu.currency_code))
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ihg_alos_map(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ihg_alos_map'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'

    query_string = """
    with t1 as (
        select h.market,
            extract(month from h.checkin_date) as checkin_month,
            extract(dayofweek from h.checkin_date) as checkin_dow,
            round((1.0 * sum(h.trip_duration) / count(*)), 1) as alos
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.dp_id = 1168
            and h.activity_type = 'book'
            and h.event_ts < timestamp('2018-04-12 00:00:00')
            and timestamp(h.checkin_date) < timestamp('2018-04-12 00:00:00')
            and h.checkin_date >= date_sub(date('2018-04-12'), interval 2 year)
        group by 1,2,3
    ),
    t2 as (
        select *,
            floor(alos) as alos_whole
        from t1
    )
    select *,
        (alos - alos_whole) as alos_decimal
    from t2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ihg_ekv_hotel_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ihg_ekv_hotel_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'

    query_string = """
    select h.*,
        extract(month from h.checkin_date) as checkin_month,
        extract(dayofweek from h.checkin_date) as checkin_dow,
        row_number() over (
            partition by extract(month from h.checkin_date), extract(dayofweek from h.checkin_date), h.market
                order by null) as ix
    from `{project_id}.{dataset_id}.{t1}` as h
    where h.dp_id = 1168
        and h.activity_type = 'book'
        and h.event_ts >= timestamp('2018-04-12 00:00:00')
        and h.event_ts < timestamp('2018-06-27 00:00:00')
        and h.checkin_date = h.checkout_date
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ihg_ekv_hotel_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ihg_ekv_hotel_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_base'
    t2 = 'mm_ihg_alos_map'

    query_string = """
    with a as (
        select h.checkin_month,
            h.checkin_dow,
            h.market,
            max(h.ix) as max_ix
        from `{project_id}.{dataset_id}.{t1}` as h
        group by 1,2,3
    ),
    b as (
        select checkin_month,
            checkin_dow,
            market,
            round(a.max_ix * am.alos_decimal) as ix_boundary
        from a
        join `{project_id}.{dataset_id}.{t2}` as am
        using (checkin_month, checkin_dow, market)
    ),
    c as (
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        case
            when h.ix <= b.ix_boundary then DATE_ADD(h.checkin_date, interval (cast (am.alos_whole as INT64) + 1) day )
            else DATE_ADD(h.checkin_date, interval (cast (am.alos_whole as INT64)) day )
        end 
            as new_checkout_date,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate,
        h.checkin_month,
        h.checkin_dow
    from `{project_id}.{dataset_id}.{t1}` as h
    join b
        using (checkin_month, checkin_dow, market)
    join `{project_id}.{dataset_id}.{t2}` as am
        using (checkin_month, checkin_dow, market)
    )
    select *,
        date_diff(new_checkout_date, checkin_date, day) as new_trip_duration
    from c
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel_mid(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel_mid'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'
    t2 = 'mm_ihg_ekv_hotel_staging'

    query_string = """
    select *
    from `{project_id}.{dataset_id}.{t1}`
    where event_id not in (
        select event_id
        from `{project_id}.{dataset_id}.{t2}`
    )
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_ihg_ekv_hotel_insert(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ihg_ekv_hotel_insert'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_staging'

    query_string = """
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.new_checkout_date as checkout_date,
        h.new_trip_duration as trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `{project_id}.{dataset_id}.{t1}` as h
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_mid'
    t2 = 'mm_ihg_ekv_hotel_insert'

    query_string = """
    select event_id, cookie_id, location_id, dp_id, vertical,
        activity_type, event_ts, checkin_date, checkout_date, trip_duration, market,
        hotel_country, hotel_state, hotel_city, hotel_code, hotel_name, hotel_brand,
        number_of_rooms, number_of_travelers, currency_type, avg_daily_rate, avg_daily_rate_usd
    from `{project_id}.{dataset_id}.{t1}`
    union all
    select event_id, cookie_id, location_id, dp_id, vertical,
        activity_type, event_ts, checkin_date, checkout_date, trip_duration, market,
        hotel_country, hotel_state, hotel_city, hotel_code, hotel_name, hotel_brand,
        number_of_rooms, number_of_travelers, currency_type, avg_daily_rate, null
    from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_market_airport_mapping(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_market_airport_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_markets_of_interest'

    query_string = """
    select  m.name as market,
        a.market_id,
        a.airport_code
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_airport_map` as a
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_market_d` as m
        on a.market_id = m.id
    join `{project_id}.{dataset_id}.{t1}` as i
        on a.market_id = i.market_id
    where a.status_id = 1
        and m.status_id = 1
    order by market, airport_code;
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_destination_airport(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_destination_airport'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_airport_mapping'

    query_string = """
    select ac.airport_code,
        ac.airport_name,
        ac.airport_type,
        ac.city_name,
        ac.dma_code,
        ac.region_code,
        ac.country_code,
        mm.market,
        ac.latitude,
        ac.longitude,
        ac.elevation,
        ac.modification_ts
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.ekv_lookup_airport_code` as ac
    join `{project_id}.{dataset_id}.{t1}` as mm
        using (airport_code)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_distinct_destination_airport(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_distinct_destination_airport'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_destination_airport'

    query_string = """
    select distinct dac.airport_code
    from `{project_id}.{dataset_id}.{t1}` as dac
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_flight_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_flight_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_distinct_destination_airport'

    query_string = """
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        if(f.activity_type in ('click','search'),'search','book') as activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.ekv_flight` as f
    join `{project_id}.{dataset_id}.{t1}` as da
        on (f.destination_airport = da.airport_code)
    where f.activity_type in ('click','search','book')
        and f.event_ts < current_timestamp
        and f.event_ts >= timestamp('2014-10-01 00:00:00')
        and f.departure_date is not null
        and f.return_date >= date('2016-01-01')
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_flight_book(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_flight_book'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base'

    query_string = """
    with f as (
        select f.*,
            row_number() over(
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts desc
            ) as ix
        from `{project_id}.{dataset_id}.{t1}` as f
        where f.activity_type = 'book'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where f.ix = 1
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_flight_search(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_flight_search'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base'

    query_string = """
    with f as (
        select f.*,
            timestamp_diff(f.event_ts, lag(f.event_ts, 1) over (
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts
                ), second
            ) as lag_seconds
        from `{project_id}.{dataset_id}.{t1}` as f
        where f.activity_type = 'search'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where (f.lag_seconds is null or f.lag_seconds > 300)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_flight_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_flight_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_book'
    t2 = 'mm_ekv_flight_search'

    query_string = """
    select * from `{project_id}.{dataset_id}.{t1}`
    union all
    select * from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_ekv_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_ekv_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_staging'
    t2 = 'mm_destination_airport'

    query_string = """
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        dac.market,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `{project_id}.{dataset_id}.{t1}` as f
    join `{project_id}.{dataset_id}.{t2}` as dac
        on (f.destination_airport = dac.airport_code)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_base_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_base_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'
    t2 = 'mm_properties'

    query_string = """
    select hotel_code,
        dp_id,
        d.cal_date as stay_date,
        date(event_ts) as statistic_date,
        count(1) as bookings
    from `{project_id}.{dataset_id}.{t1}` as h
    join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id)
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_day_d` as d
        on (d.cal_date between h.checkin_date and date_sub(h.checkout_date, interval 1 day))
    where d.cal_date >= '2016-01-01'
        and event_ts >= timestamp(date_sub(d.cal_date, interval 1 year))
        and activity_type = 'book'
    group by 1,2,3,4
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_base_t2(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_base_t2'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t1'

    query_string = """
    with T1 as (
        select hotel_code,
            dp_id,
            stay_date,
            sum(bookings) as bookings,
            min(statistic_date) as min_book_date,
            max(statistic_date) as max_book_date
        from `{project_id}.{dataset_id}.{t1}`
        group by 1,2,3
    ),
    T2 as(
        select hotel_code,
            dp_id,
            min(stay_date) as min_stay_date,
            max(stay_date) as max_stay_date
        from T1
        group by 1,2
    ),
    T3 as (
        select hotel_code,
            dp_id,
            d.cal_date as stay_date
        from T2
        cross join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_day_d` as d
        where d.cal_date between min_stay_date and max_stay_date
    )
    select hotel_code,
        dp_id,
        stay_date,
        ifnull(bookings,0) as bookings
    from T3
    left outer join T1
        using (hotel_code, dp_id, stay_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_base_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_base_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t2'

    query_string = """
    select hotel_code,
        dp_id,
        stay_date,
        d.cal_date as statistic_date,
        bookings as total_bookings_target
    from `{project_id}.{dataset_id}.{t1}` as t
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_day_d` as d
        on (d.cal_date between date_sub(t.stay_date, interval 1 year) and t.stay_date and d.cal_date <= current_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t3'
    t2 = 'mm_booking_dataset_base_t1'
    t3 = 'mm_properties'

    query_string = """
    with T1 as (
        select hotel_code,
        dp_id,
        stay_date,
        statistic_date,
        ifnull(bookings,0) as bookings_today,
        sum(ifnull(bookings,0)) over (
            partition by hotel_code, dp_id, stay_date
            order by statistic_date) as cumulative_bookings_target,
        total_bookings_target
        from `{project_id}.{dataset_id}.{t1}`
        left outer join `{project_id}.{dataset_id}.{t2}`
            using (hotel_code, dp_id, stay_date, statistic_date)
    )
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        least(T1.cumulative_bookings_target, p.number_of_rooms) as cumulative_bookings_target,
        case
            when stay_date >= current_date then null
            else least(T1.total_bookings_target, p.number_of_rooms)
            end total_bookings_target
    from T1
    join `{project_id}.{dataset_id}.{t3}` as p
        using (hotel_code, dp_id)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_metric_calculation_base_staging(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_metric_calculation_base_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'

    query_string = """
    select h.market,
        l.metrocode,
        date_trunc(h.checkin_date, month) as stay_month,
        date_trunc(date(h.event_ts), month) as statistic_month,
        h.activity_type,
        count(*) as events,
        sum(h.avg_daily_rate_usd * h.trip_duration) as adr_numerator,
        sum(if(h.avg_daily_rate_usd is not null and
            h.trip_duration is not null,
            h.trip_duration,
            null)) as adr_denominator,
        sum(h.trip_duration) as alos_numerator,
        sum(if(h.trip_duration is not null, 1, 0)) as alos_denominator,
        sum(h.number_of_travelers) as anot_numerator,
        sum(if(h.number_of_travelers is not null, 1, 0)) as anot_denominator
    from `{project_id}.{dataset_id}.{t1}` as h
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.location` as l
        on (h.location_id = l.id)
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.metrocodes` as m
        on (l.metrocode = m.id)
    group by 1,2,3,4,5
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_market_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_market_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties'

    query_string = """
    select  m.market,
        (180 * atan2(m.zeta, m.xi) / ACOS(-1)) as avg_longitude,
        m.avg_latitude
    from (
        select p.market,
            avg(sin(ACOS(-1) * p.longitude / 180)) as zeta,
            avg(cos(ACOS(-1) * p.longitude / 180)) as xi,
            avg(p.longitude) as avg_longitude,
            avg(p.latitude) as avg_latitude
        from `{project_id}.{dataset_id}.{t1}` as p
        group by 1
    ) as m
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates'

    query_string = """
    select (180 * atan2(c.zeta, c.xi) / ACOS(-1)) as avg_longitude,
        c.avg_latitude
    from (
        select avg(sin(ACOS(-1) * mc.avg_longitude / 180)) as zeta,
            avg(cos(ACOS(-1) * mc.avg_longitude / 180)) as xi,
            avg(mc.avg_longitude) as avg_longitude,
            avg(mc.avg_latitude) as avg_latitude
        from `{project_id}.{dataset_id}.{t1}` as mc
    ) as c
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_metro_coordinates(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_metro_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    select  id as metrocode,
        longitude as avg_longitude,
        latitude as avg_latitude
    from `{project_id_exist_tables}.{dataset_id_exist_tables}.metrocodes`
    """.format(
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_metro_market_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_metro_market_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates'
    t2 = 'mm_metric_metro_coordinates'

    query_string = """
    select m.market,
        mc.metrocode,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((m.avg_latitude - mc.avg_latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((mc.avg_latitude) * ACOS(-1) /180)) *
                    (cos((m.avg_latitude) * ACOS(-1) /180)) *
                    pow( (sin(((m.avg_longitude - mc.avg_longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
    from `{project_id}.{dataset_id}.{t1}` as m,
        `{project_id}.{dataset_id}.{t2}` as mc
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_metro_distance(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_metro_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_coordinates'
    t2 = 'mm_metric_metro_coordinates'

    query_string = """
    select mc.metrocode,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((m.avg_latitude - mc.avg_latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((mc.avg_latitude) * ACOS(-1) /180)) *
                    (cos((m.avg_latitude) * ACOS(-1) /180)) *
                    pow( (sin(((m.avg_longitude - mc.avg_longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
    from `{project_id}.{dataset_id}.{t1}` as m,
        `{project_id}.{dataset_id}.{t2}` as mc
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_base_metrocode_map(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_base_metrocode_map'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging'
    t2 = 'mm_metric_metro_distance'

    query_string = """
    with a1 as (
        select  mc.metrocode,
            sum(mc.events) as metrocode_bookings
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'book'
        group by 1
        ),
    a2 as (
        select *,
            sum(metrocode_bookings) over() as overall_bookings
        from a1
    ),
    a as (
        select *,
            (1.0 * metrocode_bookings / overall_bookings) as metrocode_booking_share
        from a2
    ),
    b as (
        select a.metrocode,
            case
                when a.metrocode_booking_share >= 0.01 then a.metrocode
                else -1
            end
                as metrocode_group,
            a.metrocode_bookings as bookings,
            a.metrocode_booking_share as booking_share
        from a
    ),
    c as (
        select  b.*,
            md.distance_miles,
            row_number() over (order by md.distance_miles) as distance_rank
        from `{project_id}.{dataset_id}.{t2}` as md
        join b
        using (metrocode)
        where b.metrocode_group = -1
    ),
    p as (
        select *,
            percentile_cont(c.distance_miles, 0.2) OVER() as p_20,
            percentile_cont(c.distance_miles, 0.4) OVER() as p_40,
            percentile_cont(c.distance_miles, 0.6) OVER() as p_60,
            percentile_cont(c.distance_miles, 0.8) OVER() as p_80
        from c
    )
    select b.metrocode,
        case
            when b.metrocode_group = -1 and p.distance_miles <  p.p_20 then -1
            when b.metrocode_group = -1 and p.distance_miles >= p.p_20 and p.distance_miles < p.p_40 then -2
            when b.metrocode_group = -1 and p.distance_miles >= p.p_40 and p.distance_miles < p.p_60 then -3
            when b.metrocode_group = -1 and p.distance_miles >= p.p_60 and p.distance_miles < p.p_80 then -4
            when b.metrocode_group = -1 and p.distance_miles >= p.p_80 then -5
            else b.metrocode
        end
            as metrocode_group,
        p.distance_miles,
        b.bookings,
        b.booking_share
    from b
    left join p
        using (metrocode)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_metric_calculation_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_metric_calculation_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging'
    t2 = 'mm_metric_base_metrocode_map'

    query_string = """
    select  mc.market,
        mm.metrocode_group,
        mc.stay_month,
        mc.statistic_month,
        mc.activity_type,
        sum(mc.events) as events,
        sum(mc.adr_numerator) as adr_numerator,
        sum(mc.adr_denominator) as adr_denominator,
        sum(mc.alos_numerator) as alos_numerator,
        sum(mc.alos_denominator) as alos_denominator,
        sum(mc.anot_numerator) as anot_numerator,
        sum(mc.anot_denominator) as anot_denominator
    from `{project_id}.{dataset_id}.{t1}` as mc
    join `{project_id}.{dataset_id}.{t2}` as mm
    using (metrocode)
    where case
        when mc.stay_month = date('2016-01-01')
            then mc.statistic_month >= date_sub(mc.stay_month, interval 15 month)
            else mc.statistic_month >= date_sub(mc.stay_month, interval 1 year)
        end
        and mc.statistic_month <= mc.stay_month
        and mc.stay_month >= date('2016-01-01')
    group by 1,2,3,4,5
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_metric_calculation_point(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_metric_calculation_point'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base'

    query_string = """
    with s as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,

            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'search'
    ),
    b as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,

            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'book'
    )
    select market,
        metrocode_group,
        stay_month,
        statistic_month,

        s.events as search_events,

        s.adr_numerator as search_adr_numerator,
        s.adr_denominator as search_adr_denominator,
        (1.0 * s.adr_numerator / s.adr_denominator) as search_adr,

        s.alos_numerator as search_alos_numerator,
        s.alos_denominator as search_alos_denominator,
        (1.0 * s.alos_numerator / s.alos_denominator) as search_alos,

        s.anot_numerator as search_anot_numerator,
        s.anot_denominator as search_anot_denominator,
        (1.0 * s.anot_numerator / s.anot_denominator) as search_anot,

        b.events as book_events,
        b.adr_numerator as book_adr_numerator,
        b.adr_denominator as book_adr_denominator,
        (1.0 * b.adr_numerator / b.adr_denominator) as book_adr,

        b.alos_numerator as book_alos_numerator,
        b.alos_denominator as book_alos_denominator,
        (1.0 * b.alos_numerator / b.alos_denominator) as book_alos,

        b.anot_numerator as book_anot_numerator,
        b.anot_denominator as book_anot_denominator,
        (1.0 * b.anot_numerator / b.anot_denominator) as book_anot
    from s
    full outer join b
    using (market, metrocode_group, stay_month, statistic_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_metric_calculation_cumulative(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_metric_calculation_cumulative'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'

    query_string = """
    select a.market,
        a.metrocode_group,
        a.stay_month,
        a.statistic_month,
        sum(b.search_events) as search_events,

        (1.0 * sum(b.search_adr_numerator) / sum(b.search_adr_denominator)) as search_adr,
        (1.0 * sum(b.search_alos_numerator) / sum(b.search_alos_denominator)) as search_alos,
        (1.0 * sum(b.search_anot_numerator) / sum(b.search_anot_denominator)) as search_anot,

        sum(b.book_events) as book_events,

        (1.0 * sum(b.book_adr_numerator) / sum(b.book_adr_denominator)) as book_adr,
        (1.0 * sum(b.book_alos_numerator) / sum(b.book_alos_denominator)) as book_alos,
        (1.0 * sum(b.book_anot_numerator) / sum(b.book_anot_denominator)) as book_anot
    from `{project_id}.{dataset_id}.{t1}` as a
    join `{project_id}.{dataset_id}.{t1}` as b
    on (a.market = b.market)
        and (a.metrocode_group = b.metrocode_group)
        and (a.stay_month = b.stay_month)
        and (b.statistic_month <= a.statistic_month)
    group by 1,2,3,4
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_target(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_target'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'

    query_string = """
    select mcp.market,
        mcp.metrocode_group,
        mcp.stay_month,
        sum(mcp.book_events) as bookings_target,
        round(1.0 * sum(mcp.book_adr_numerator) / sum(mcp.book_adr_denominator)) as adr_target,
        round((1.0 * sum(mcp.book_alos_numerator) / sum(mcp.book_alos_denominator)),2) as alos_target,
        round((1.0 * sum(mcp.book_anot_numerator) / sum(mcp.book_anot_denominator)),2) as anot_target,
        round(1.0 * sum(mcp.search_events) / sum(mcp.book_events)) as stob_target
    from `{project_id}.{dataset_id}.{t1}` as mcp
    where mcp.stay_month < date_trunc(current_date(), month)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base_feature(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base_feature'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'
    t2 = 'mm_metric_dataset_base_metric_calculation_cumulative'

    query_string = """
    select market,
        metrocode_group,
        stay_month,
        statistic_month,
        mcp.search_events as ps_events,
        round(mcp.search_adr) as ps_adr,
        round(mcp.search_alos, 2) as ps_alos,
        round(mcp.search_anot, 2) as ps_anot,

        mcp.book_events as pb_events,
        round(mcp.book_adr) as pb_adr,
        round(mcp.book_alos, 2) as pb_alos,
        round(mcp.book_anot, 2) as pb_anot,

        mcc.search_events as cs_events,
        round(mcc.search_adr) as cs_adr,
        round(mcc.search_alos, 2) as cs_alos,
        round(mcc.search_anot, 2) as cs_anot,

        mcc.book_events as cb_events,
        round(mcc.book_adr) as cb_adr,
        round(mcc.book_alos, 2) as cb_alos,
        round(mcc.book_anot, 2) as cb_anot
    from `{project_id}.{dataset_id}.{t1}` as mcp
    full outer join `{project_id}.{dataset_id}.{t2}` as mcc
    using (market, metrocode_group, stay_month, statistic_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_metric_dataset_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_metric_dataset_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_feature'
    t2 = 'mm_metric_dataset_base_target'

    query_string = """
    select market,
        metrocode_group,
        stay_month,
        dbf.statistic_month,
        cast((date_diff(stay_month, dbf.statistic_month, day) / 30) as INT64) as stay_window,
        dbt.bookings_target,
        dbt.adr_target,
        dbt.alos_target,
        dbt.anot_target,
        dbt.stob_target,

        dbf.ps_events,
        dbf.ps_adr,
        dbf.ps_alos,
        dbf.ps_anot,

        dbf.pb_events,
        dbf.pb_adr,
        dbf.pb_alos,
        dbf.pb_anot,

        dbf.cs_events,
        dbf.cs_adr,
        dbf.cs_alos,
        dbf.cs_anot,

        dbf.cb_events,
        dbf.cb_adr,
        dbf.cb_alos,
        dbf.cb_anot
    from `{project_id}.{dataset_id}.{t1}` as dbf
    full outer join `{project_id}.{dataset_id}.{t2}` as dbt
    using (market, metrocode_group, stay_month)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_hotel_summary_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_hotel_summary_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'

    query_string = """
    select h.hotel_code,
        h.dp_id,
        h.market,
        h.activity_type,
        h.checkin_date,
        h.checkout_date,
        date(h.event_ts) as event_date,
        count(*) as events
    from `{project_id}.{dataset_id}.{t1}` as h
    group by 1,2,3,4,5,6,7
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_hotel_summary(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_hotel_summary'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary_base'

    query_string = """
    select hs.hotel_code,
        hs.dp_id,
        hs.market,
        hs.activity_type,
        d.cal_date as stay_date,
        hs.event_date,
        sum(hs.events) as events
    from `{project_id}.{dataset_id}.{t1}` as hs
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_day_d` as d
    on (d.cal_date between hs.checkin_date and date_sub(hs.checkout_date, interval 1 day))
    group by 1,2,3,4,5,6
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_hc_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_hc_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_hotel_summary'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
    on (b.stay_date = hs.stay_date)
        and (hs.event_date >= date_sub(b.statistic_date, interval 14 day))
        and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'search'
        and b.hotel_code = hs.hotel_code
        and b.dp_id = hs.dp_id
    group by 1,2,3,4,5
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_search_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_search_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'search'
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_market_stay_stat_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_market_stay_stat_d'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'

    query_string = """
    select market,
        stay_date,
        statistic_date
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_search_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_search_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_search_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_search_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_stay_stat_d(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_stay_stat_d'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_search_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_search_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_searches
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
    using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_m_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_m_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_cm_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_cm_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_m_search_base'
    t2 = 'mm_hotel_search_by_market_t6'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        t.total_hotel_searches - m.hotel_market_searches as hotel_competitive_market_searches
    from `{project_id}.{dataset_id}.{t1}` as m
    join `{project_id}.{dataset_id}.{t2}` as t
    using (stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_hc_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_hc_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_hotel_summary'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        on b.stay_date = hs.stay_date
            and b.hotel_code = hs.hotel_code
            and b.dp_id = hs.dp_id
            and (hs.event_date >= date_sub(b.statistic_date, interval 14 day))
            and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'book'
    group by 1,2,3,4,5
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_book_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_book_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'book'
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_book_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_book_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_book_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_book_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_hotel_book_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_hotel_book_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_bookings
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_m_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_m_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
    using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel_cm_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel_cm_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_book_by_market_t6'
    t3 = 'mm_hotel_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_hotel_bookings - ifnull(m.events,0) as hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_hc_search_base'
    t2 = 'mm_booking_dataset_hotel_m_search_base'
    t3 = 'mm_booking_dataset_hotel_cm_search_base'
    t4 = 'mm_booking_dataset_hotel_hc_book_base'
    t5 = 'mm_booking_dataset_hotel_m_book_base'
    t6 = 'mm_booking_dataset_hotel_cm_book_base'

    query_string = """
    select  hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,

        hotel_code_searches,
        hotel_market_searches,
        hotel_competitive_market_searches,

        hotel_code_bookings,
        hotel_market_bookings,
        hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as hcs
    full outer join `{project_id}.{dataset_id}.{t2}` as ms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t3}` as cms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t4}` as hcb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t5}` as mb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t6}` as cmb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4,
        t5=t5,
        t6=t6
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_flight_summary_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_flight_summary_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight'

    query_string = """
    select f.destination_airport,
        f.market,
        f.activity_type,
        f.departure_date,
        f.return_date,
        date(f.event_ts) as event_date,
        count(*) as events
    from `{project_id}.{dataset_id}.{t1}` as f
    group by 1,2,3,4,5,6
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_flight_summary_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_flight_summary_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base'

    query_string = """
    with T1 as (
        select departure_date,
            return_date
        from `{project_id}.{dataset_id}.{t1}`
        group by 1,2)
    select departure_date,
        return_date,
        d.cal_date stay_date
    from T1
    join `{project_id_exist_tables}.{dataset_id_exist_tables}.inf_day_d` as d
        on (d.cal_date between T1.departure_date and T1.return_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_flight_summary(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_flight_summary'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base'
    t2 = 'mm_booking_flight_summary_t1'

    query_string = """
    select fs.destination_airport,
        fs.market,
        fs.activity_type,
        t1.stay_date,
        fs.event_date,
        sum(fs.events) as events
    from `{project_id}.{dataset_id}.{t1}` as fs
        join `{project_id}.{dataset_id}.{t2}` as t1
            using (departure_date, return_date)
    group by 1,2,3,4,5
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_search_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_search_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'search'
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_search_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_search_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_flight_m_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_flight_m_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_search_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_search_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_search_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_search_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_searches
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_flight_cm_search_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_flight_cm_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_search_by_market_t6'
    t3 = 'mm_flight_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_searches - ifnull(m.events,0) as flight_competitive_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_book_by_market_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_book_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'book'
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_book_by_market_t3(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_book_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_book_by_market_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_book_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_flight_book_by_market_t6(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_flight_book_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_bookings
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_flight_m_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_flight_m_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_flight_cm_book_base(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_flight_cm_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_book_by_market_t6'
    t3 = 'mm_flight_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_bookings - ifnull(m.events,0) as flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight_m_search_base'
    t2 = 'mm_booking_dataset_flight_cm_search_base'
    t3 = 'mm_booking_dataset_flight_m_book_base'
    t4 = 'mm_booking_dataset_flight_cm_book_base'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,

        flight_market_searches,
        flight_competitive_market_searches,

        flight_market_bookings,
        flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as ms
    full outer join `{project_id}.{dataset_id}.{t2}` as cms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t3}` as mb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t4}` as cmb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_holidays(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_holidays'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'

    query_string = """
    with d as (
        select distinct b.stay_date
        from `{project_id}.{dataset_id}.{t1}` as b
    )
    select d.stay_date,
        max(b.holiday_date) as last_holiday_date,
        min(a.holiday_date) as next_holiday_date
    from d,
        `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_holiday_dates` as b,
        `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_holiday_dates` as a
    where b.holiday_date < d.stay_date
        and a.holiday_date > d.stay_date
    group by 1
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_t1(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_holidays'
    t3 = 'mm_markets'
    t4 = 'mm_properties'

    query_string = """
    with t as (
        select db.hotel_code,
            db.dp_id,
            db.market,
            db.stay_date,
            date_sub(db.stay_date,
                interval (cast(floor(DATE_DifF(db.stay_date, db.statistic_date, day) / 7.0) * 7 as INT64)) day)
                    as statistic_week,
                    
            max(db.cumulative_bookings_target) as cumulative_bookings_target,
            max(db.total_bookings_target) as total_bookings_target,
        
            max(extract(month from db.stay_date)) as stay_month,
            max(extract(dayofweek from db.stay_date)) as stay_dow,

            min(extract(month from db.statistic_date)) as min_statistic_month,
            max(extract(month from db.statistic_date)) as max_statistic_month,

            min(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as min_days_since_holiday,
            max(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as max_days_since_holiday,
            avg(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as avg_days_since_holiday,
            min(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as min_days_until_holiday,
            min(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as max_days_until_holiday,
            avg(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as avg_days_until_holiday,
            cast(min((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as INT64) as min_weeks_open,
            cast(max((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as INT64) as max_weeks_open,
            avg((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as avg_weeks_open,
            stddev((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as stddev_weeks_open
        from `{project_id}.{dataset_id}.{t1}` as db
        left join `{project_id}.{dataset_id}.{t2}` as hs
            on (db.stay_date = hs.stay_date)
        left join `{project_id_exist_tables}.{dataset_id_exist_tables}.mm_holiday_dates` as hd
            on (db.stay_date = hd.holiday_date)
        left join `{project_id}.{dataset_id}.{t3}` as m
            on (db.market = m.market)
        left join `{project_id}.{dataset_id}.{t4}` as p
            on (db.hotel_code = p.hotel_code) and (db.dp_id = p.dp_id)
        where db.stay_date >= date('2016-01-01')
        group by 1,2,3,4,5
        having count(db.statistic_date) = 7
    )
    select t.*,
        cast((date_diff(t.stay_date, t.statistic_week, day) / 7) as INT64) as stay_window
    from t
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        project_id_exist_tables=project_id_exist_tables,
        dataset_id_exist_tables=dataset_id_exist_tables,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_t2(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_t2'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t1'
    t2 = 'mm_markets'
    t3 = 'mm_properties'

    query_string = """
    select hotel_code,
        dp_id,
        t.market,
        stay_date,
        statistic_week,
        cumulative_bookings_target,
        total_bookings_target,
        stay_window,
        stay_month,
        stay_dow,
        min_statistic_month,
        max_statistic_month,
        min_days_since_holiday,
        max_days_since_holiday,
        avg_days_since_holiday,
        min_days_until_holiday,
        max_days_until_holiday,

        avg_days_until_holiday,
        p.number_of_rooms,
        1.0 * p.number_of_rooms / nullif(m.number_of_rooms,0) as room_share_market,

        min_weeks_open,
        max_weeks_open,
        avg_weeks_open,
        stddev_weeks_open,
        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(chain_scale), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as chain_scale,
        floors,
        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(p.location), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as location,

        has_indoor_corridors,
        has_restaurant,
        has_convention,
        has_conference,
        has_spa,
        has_single_meeting_space,
        largest_meeting_space,
        total_meeting_space,
        is_resort,
        is_ski_resort,
        is_golf_resort,
        is_all_suites,
        is_casino,

        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(p.price), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as price,
        single_low_rate,
        single_high_rate,
        double_low_rate,
        double_high_rate,
        suite_low_rate,
        suite_high_rate
    from `{project_id}.{dataset_id}.{t1}` as t
    join `{project_id}.{dataset_id}.{t2}` as m
        using (market)
    join `{project_id}.{dataset_id}.{t3}` as p
        using (hotel_code, dp_id)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_t3_hotel(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_t3_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        date_sub(stay_date, interval (cast(floor(date_diff(stay_date, statistic_date, day) / 7.0) * 7 as INT64)) day)
            as statistic_week,

        max(ifnull(h.hotel_code_searches, 0)) as max_hotel_code_searches,
        min(ifnull(h.hotel_code_searches, 0)) as min_hotel_code_searches,
        avg(ifnull(h.hotel_code_searches, 0)) as avg_hotel_code_searches,

        max(ifnull(h.hotel_market_searches, 0)) as max_hotel_market_searches,
        min(ifnull(h.hotel_market_searches, 0)) as min_hotel_market_searches,
        avg(ifnull(h.hotel_market_searches, 0)) as avg_hotel_market_searches,

        max(ifnull(h.hotel_competitive_market_searches, 0)) as max_hotel_competitive_market_searches,
        min(ifnull(h.hotel_competitive_market_searches, 0)) as min_hotel_competitive_market_searches,
        avg(ifnull(h.hotel_competitive_market_searches, 0)) as avg_hotel_competitive_market_searches,

        max(ifnull(h.hotel_code_bookings, 0)) as max_hotel_code_bookings,
        min(ifnull(h.hotel_code_bookings, 0)) as min_hotel_code_bookings,
        avg(ifnull(h.hotel_code_bookings, 0)) as avg_hotel_code_bookings,

        max(ifnull(h.hotel_market_bookings, 0)) as max_hotel_market_bookings,
        min(ifnull(h.hotel_market_bookings, 0)) as min_hotel_market_bookings,
        avg(ifnull(h.hotel_market_bookings, 0)) as avg_hotel_market_bookings,

        max(ifnull(h.hotel_competitive_market_bookings, 0)) as max_hotel_competitive_market_bookings,
        min(ifnull(h.hotel_competitive_market_bookings, 0)) as min_hotel_competitive_market_bookings,
        avg(ifnull(h.hotel_competitive_market_bookings, 0)) as avg_hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as h
    where stay_date >= date('2016-01-01')
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_t4(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t2'
    t2 = 'mm_booking_dataset_t3_hotel'

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}`
    left outer join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_t5_flight(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_t5_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        date_sub(stay_date, interval (cast(floor(date_diff(stay_date, statistic_date, day) / 7.0) * 7 as INT64)) day)
            as statistic_week,

        max(ifnull(f.flight_market_searches, 0)) as max_flight_market_searches,
        min(ifnull(f.flight_market_searches, 0)) as min_flight_market_searches,
        avg(ifnull(f.flight_market_searches, 0)) as avg_flight_market_searches,

        max(ifnull(f.flight_competitive_market_searches, 0)) as max_flight_competitive_market_searches,
        min(ifnull(f.flight_competitive_market_searches, 0)) as min_flight_competitive_market_searches,
        avg(ifnull(f.flight_competitive_market_searches, 0)) as avg_flight_competitive_market_searches,

        max(ifnull(f.flight_market_bookings, 0)) as max_flight_market_bookings,
        min(ifnull(f.flight_market_bookings, 0)) as min_flight_market_bookings,
        avg(ifnull(f.flight_market_bookings, 0)) as avg_flight_market_bookings,

        max(ifnull(f.flight_competitive_market_bookings, 0)) as max_flight_competitive_market_bookings,
        min(ifnull(f.flight_competitive_market_bookings, 0)) as min_flight_competitive_market_bookings,
        avg(ifnull(f.flight_competitive_market_bookings, 0)) as avg_flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as f
    where stay_date >= date('2016-01-01')
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t4'
    t2 = 'mm_booking_dataset_t5_flight'

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings,
        max_flight_market_searches, min_flight_market_searches,
        avg_flight_market_searches, max_flight_competitive_market_searches,
        min_flight_competitive_market_searches, avg_flight_competitive_market_searches,
        max_flight_market_bookings, min_flight_market_bookings, avg_flight_market_bookings,
        max_flight_competitive_market_bookings, min_flight_competitive_market_bookings,
        avg_flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}`
    left outer join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)


def mm_booking_dataset_full(client, account_id, project_id_2, dataset_id, project_id_exist_tables, dataset_id_exist_tables):
    table_name = 'mm_booking_dataset_full'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset'
    t2 = 'mm_markets'

    query_string = """
    select a.*,
        b.NUMBER_OF_ROOMS as market_total_rooms
    from `{project_id}.{dataset_id}.{t1}` as a,
        `{project_id}.{dataset_id}.{t2}` as b
    where a.market = b.market
    """.format(
        project_id=project_id_2,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )
    create_table_query(client, query_string, table_ref)




























