import os
import logging
from datetime import timedelta, datetime
import time

from google.cloud import pubsub


max_date_format = '%Y-%m-%d'

project_id = os.environ['PROJECT_ID']

topic_id = os.environ['TOPIC_ID']


logger = logging.getLogger(__name__)


publisher = pubsub.PublisherClient()

search_book_topic = 'projects/{project_id}/topics/{topic}'.format(
    project_id=project_id,
    topic=topic_id
)

origin_market_topic = 'projects/{project_id}/topics/{topic}'.format(
    project_id=project_id,
    topic=topic_id
)


def publish_search_book(gcs_path, acc_id, name, run_ts):
    logger.info("@ publish_search_book")
    logger.info(u'Publishing sb msg for account {}...'.format(acc_id))
    logger.info(gcs_path)
    logger.info(acc_id)
    logger.info(name)
    logger.info(run_ts)

    msg = 'mmsbm-start'
    output_sb_bucket_id = os.environ['OUTPUT_SB_BUCKET_ID']
    # output_path = 'gs://{}/output/{}.csv'.format(output_sb_bucket_id, name)
    output_path = 'gs://{}/lala/{}.csv'.format(project_id, name)
    logger.info(output_path)
    logger.info(str.encode(output_path))
    max_date = datetime.strptime(run_ts, '%Y-%m-%d %H:%M:%S') + timedelta(days=180)
    logger.info(max_date)
    logger.info(max_date.strftime(max_date_format))
    publisher.publish(search_book_topic,
                      msg.encode('utf-8'),
                      INPUT_PATH=gcs_path.encode('utf-8'),
                      NAME=name.encode('utf-8'),
                      OUTPUT_PATH=output_path.encode('utf-8'),
                      ACCOUNT_ID=acc_id.encode('utf-8'),
                      MAX_DATE=max_date.strftime(max_date_format)
                      )

    logger.info(u'Published message: {} to {}'.format(msg, search_book_topic))

    # # test if msg has been received
    # time.sleep(30)
    #
    # logger.info('-----> Hi')
    #
    # subscriber = pubsub.SubscriberClient()
    # subscription_name = 'harry_test'
    # subscription_path = subscriber.subscription_path(
    #     project_id, subscription_name)
    #
    # def callback(message):
    #     logger.info('Received message: {}'.format(message))
    #     message.ack()
    #
    # subscriber.subscribe(subscription_path, callback=callback)
    #
    # # The subscriber is non-blocking. We must keep the main thread from
    # # exiting to allow it to process messages asynchronously in the background.
    # logger.info('Listening for messages on {}'.format(subscription_path))
    # # # while True:
    # # #     time.sleep(60)


def publish_origin_market(gcs_path, acc_id):
    logger.info(u'Publishing om msg for account {}...'.format(acc_id))
    msg = 'origin-market'
    publisher.publish(origin_market_topic,
                      msg.encode('utf-8'),
                      INPUT_PATH=gcs_path.encode('utf-8')
                      )

    logger.info(u'Published message: {} to {}'.format(msg, origin_market_topic))
