import gzip
import io
import logging
import os

from google.cloud import bigquery
import googleapiclient.discovery
from google.cloud import storage

from .pubsub import publish_search_book, publish_origin_market


logger = logging.getLogger(__name__)


def export_sb_to_gcs(job_details):
    logger.info("@ export_sb_to_gcs")
    logger.info(u'Exporting sb data to GCS for account {}...'.format(job_details['dataset_id']))

    # for i in dataset_ids:
    #     logger.info(i)

    client = bigquery.Client()

    project_id = os.environ['PROJECT_ID']

    output_sb_bucket_id = os.environ['OUTPUT_SB_BUCKET_ID']

    sb_table_id = os.environ['SB_TABLE_ID']

    dataset_id = job_details['dataset_id']

    # extract table to chunk files
    logger.info(u'Exporting table to chunk files...')
    destination_uri = 'gs://{}/{}-*.csv.gz'.format(output_sb_bucket_id, dataset_id)

    dataset_ref = client.dataset(dataset_id, project=project_id)

    table_ref = dataset_ref.table(sb_table_id)

    # prepare configurations for extraction
    job_config = bigquery.job.ExtractJobConfig()
    job_config.compression = bigquery.Compression.GZIP
    job_config.field_delimiter = '|'
    job_config.print_header = False

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        # Location must match that of the source table
        location='US',
        job_config=job_config
    )  # API request

    extract_job.result()  # Waits for job to complete
    chunk_number = extract_job.destination_uri_file_counts
    logger.info(chunk_number)

    # get files from bucket with specific format
    logger.info(u'Getting files from bucket with specific format...')
    storage_client = storage.Client()

    bucket = storage_client.get_bucket(output_sb_bucket_id)   # API request

    prefix = dataset_id
    # delimiter = '/'     # TODO: check without delimeter
    blobs = bucket.list_blobs(prefix=prefix)
    logger.info(blobs)
    # sources = list(blobs)
    files_to_merge = [blob.name for blob in blobs]

    # for i in sources:
    #     logger.info(i)

    # create GZIP file with column header names
    logger.info(u'Creating GZIP file with column header names...')
    table = client.get_table(table_ref)  # API Request
    logger.info(table.schema)
    col_names = ["{}".format(schema.name) for schema in table.schema]
    logger.info(col_names)
    csv_header = '|'.join(col_names)
    csv_header = csv_header + '\n'
    logger.info(csv_header)

    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode='w') as fo:
        fo.write(csv_header.encode())

    bytes_obj = out.getvalue()
    logger.info(bytes_obj)

    destination_blob_name = dataset_id + '_' + sb_table_id + '_header.csv.gz'
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_string(
        data=bytes_obj,
        content_type='application/octet-stream'
    )

    # merge chunks and header files
    files_to_merge.insert(0, destination_blob_name)
    logger.info(files_to_merge)

    service = googleapiclient.discovery.build('storage', 'v1')

    compose_req_body = {
        'sourceObjects': [{'name': filename} for filename in files_to_merge],
        'destination': {
            'contentType': 'application/octet-stream',
        }
    }

    merged_file_name = dataset_id + '.csv.gz'
    req = service.objects().compose(
        destinationBucket=output_sb_bucket_id,
        destinationObject=merged_file_name,
        body=compose_req_body
    )

    compose_ret = req.execute()
    logger.info(compose_ret)

    logger.info(u'Exported {}:{}.{} to {}'.format(project_id, dataset_id, sb_table_id, destination_uri))

    # pub/sub msg
    publish_search_book(gcs_path=destination_uri,
                        acc_id=job_details['acc_id'],
                        name=job_details['name'],
                        run_ts=job_details['run_ts'])

    logger.info(u'Deleting chunks and header file...')
    # delete chunks
    prefix = dataset_id + '-'
    blobs = bucket.list_blobs(prefix=prefix)
    for blob in blobs:
        blob.delete()

    # delete header file
    blob = bucket.blob(destination_blob_name)
    blob.delete()


def export_om_to_gcs(job_details):
    logger.info("@ export_om_to_gcs")
    logger.info(u'Exporting om data to GCS for account {}...'.format(job_details['dataset_id']))

    # for i in dataset_ids:
    #     logger.info(i)

    client = bigquery.Client()

    project_id = os.environ['PROJECT_ID']

    output_om_bucket_id = os.environ['OUTPUT_OM_BUCKET_ID']

    om_table_id = os.environ['OM_TABLE_ID']

    dataset_id = job_details['dataset_id']

    # extract table to file
    logger.info(u'Exporting table...')
    destination_uri = 'gs://{}/{}_om.csv'.format(output_om_bucket_id, dataset_id)

    dataset_ref = client.dataset(dataset_id, project=project_id)

    table_ref = dataset_ref.table(om_table_id)

    # prepare configurations for extraction
    job_config = bigquery.job.ExtractJobConfig()
    job_config.field_delimiter = '|'
    job_config.print_header = True

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        # Location must match that of the source table
        location='US',
        job_config=job_config
    )  # API request

    extract_job.result()  # Waits for job to complete

    logger.info(u'Exported {}:{}.{} to {}'.format(project_id, dataset_id, om_table_id, destination_uri))

    # pub/sub msg
    publish_origin_market(gcs_path=destination_uri,
                          acc_id=job_details['acc_id'])
