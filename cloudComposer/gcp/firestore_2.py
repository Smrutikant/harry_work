# from pyjavaproperties import Properties
# import os
import logging
import firebase_admin
from firebase_admin import firestore

# import sys
# explicitly append hard-coded path to sys.path in order to import module dependencies
# from folders 2+ levels deep relative to the "dags" folder
# sys.path.append('/opt/opinmind/data/airflow/dags/airflow/mm')
#
# from gcp import firebase_cred
# from utils.constants import date_format, gke_name_format, search_book_query, origin_market_query, \
#     search_book_select_query, origin_market_select_query


# conf_dir = '/opt/opinmind/conf/'
# properties = Properties()
# properties.load(open(os.path.join(conf_dir, 'common.properties')))
# properties.load(open(os.path.join(conf_dir, 'airflow/airflow.properties')))

logger = logging.getLogger(__name__)

firebase_admin.initialize_app()

db = firestore.client()


class User(object):
    collection_name = u'users'

    def __init__(self, name, doc_id=None):
        self.doc_id = doc_id
        self.name = {
            u'first': name,
            u'last': name
        }

    def to_dict(self):
        return {
            u'name': self.name
        }

    @staticmethod
    def from_dict(source, doc_id=None):
        user = User(source[u'name'], doc_id=doc_id)
        return user

    def __repr__(self):
        return u'User(name={})'.format(self.name)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id


class Account(object):
    collection_name = u'accounts'
    jobs_sub_col_name = u'jobs'

    def __init__(self, name, doc_id=None):
        self.doc_id = doc_id
        self.name = name
        # self.markets = markets

    @staticmethod
    def from_dict(source, doc_id=None):
        account = Account(source[u'name'], doc_id=doc_id)
        return account

    def to_dict(self):
        return {
            u'name': self.name,
            # u'markets': self.markets
        }

    def __repr__(self):
        return u'Account(name={})'.format(self.name)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id

    # def add_market(self, market_ref):
    #     self.markets.append(market_ref)


class Market(object):
    collection_name = u'markets'

    def __init__(self, name, doc_id=None):
        self.doc_id = doc_id
        self.name = name

    @staticmethod
    def from_dict(source, doc_id=None):
        market = Market(source[u'name'], doc_id=doc_id)
        return market

    def to_dict(self):
        return {
            u'name': self.name
        }

    def __repr__(self):
        return u'Market(name={})'.format(self.name)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id


class Job(object):
    collection_name = u'jobs'

    def __init__(self, name, status, run_day_of_week, doc_id=None, acc_id=None):
        self.doc_id = doc_id
        self.acc_id = acc_id
        self.name = name
        self.status = status
        self.run_day_of_week = run_day_of_week
        # self.market = self.fetch_market(market)

    @staticmethod
    def from_dict(source, doc_id=None):
        job = Job(source[u'name'],
                  source[u'status'],
                  source[u'run_day_of_week'],
                  # source[u'market'],
                  doc_id=doc_id)
        return job

    def to_dict(self):
        dest = {
            u'name': self.name,
            u'status': self.status,
            u'run_day_of_week': self.run_day_of_week,
            # u'market': self.market
        }
        return dest

    def __repr__(self):
        return u'Job(name={}, status={}, run_day_of_week={})'.format(
            self.name, self.status, self.run_day_of_week)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id

    def set_account_id(self, account_id):
        self.acc_id = account_id

    def get_account_id(self):
        return self.acc_id

    @staticmethod
    def fetch_market(market):
        if isinstance(market, firestore.firestore.DocumentReference):
            # fetch the market
            market_doc = market.get()
            return Market.from_dict(market_doc.to_dict(), doc_id=market_doc.id)
        elif isinstance(market, Market):
            return market
        else:
            logging.warn('\'market\' is of unsupported type: %s', type(market))

    def get_sb_query_filename(self):
        return u'sb_{}.sql'.format(self.doc_id)

    def get_om_query_filename(self):
        return u'om_{}.sql'.format(self.doc_id)

    def get_output_name(self, run_ts):
        report_date = run_ts.strftime(date_format)
        timestamp = report_date[:-3]
        return gke_name_format.format(acc_id=self.acc_id, ts=timestamp)

    def get_output_filename(self, run_ts, gzipped=False):
        filename = self.get_output_name(run_ts) + u'.csv'
        if gzipped:
            filename += u'.gz'
        return filename

    @staticmethod
    def gen_sb_nz_query():
        return search_book_query.format(nz_db=properties['netezza.db.name'])

    @staticmethod
    def gen_om_nz_query():
        return origin_market_query.format(nz_db=properties['netezza.db.name'])

    @staticmethod
    def gen_sb_nz_select_query():
        return search_book_select_query.format(nz_db=properties['netezza.db.name'])

    @staticmethod
    def gen_om_nz_select_query():
        return origin_market_select_query.format(nz_db=properties['netezza.db.name'])


def fetch_jobs():
    logger.info(u'Fetching Active Market Monitor Jobs...')
    jobs = []

    # fetch all accounts
    acc_docs = db.collection(Account.collection_name).get()

    for acc_doc in acc_docs:
        # fetch active jobs within each account
        jobs_sub_collection = acc_doc.reference.collection(Account.jobs_sub_col_name)
        active_job_docs = jobs_sub_collection.where(u'status', u'==', u'A').get()

        for job_doc in active_job_docs:
            job = Job.from_dict(job_doc.to_dict(), doc_id=job_doc.id)
            # attach reference to parent account to the job object
            job.set_account_id(acc_doc.id)
            jobs.append(job)
            logger.info(u'%s => %s', job_doc.id, job.to_dict())

    return jobs


def kariola_2():
    logging.info('... kai kano to zografo!')