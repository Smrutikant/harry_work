import logging
import datetime
import os

from airflow import utils as airflow_utils
from airflow.models import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.latest_only_operator import LatestOnlyOperator

from utils.query_driver import run_bq_queries

from gcp.firestore import fetch_jobs
from gcp.storage import export_sb_to_gcs, export_om_to_gcs


DAG_NAME = 'Market_Monitor_DAG_2'
# job_1_id = 'gamidi_1'
# job_2_id = 'gamidi_2'
fetch_jobs_task_id = 'fetch_jobs'
filter_jobs_run_date_task_id = 'filter_jobs_run_date'
run_if_has_active_job_task_id = 'run_if_has_active_job'
no_active_task_id = 'notify_no_active'
# gen_query_files_task_id = 'generate_queries'
accounts_run_task_id = 'run_queries_on_BQ'
# export_sb_data_task_id = 'sb_data_on_gcs'
# export_om_data_task_id = 'om_data_on_gcs'
# set_dataset_expiration_id = 'set_expiration_date_on_dataset'


logger = logging.getLogger(DAG_NAME)


# TODO: add more owners
owners = [
    'harry.chasparis@adara.com'
]


args = {
    'email': ','.join(owners),
    'owner': ','.join(owners),
    'start_date': airflow_utils.dates.days_ago(10),
    'provide_context': True,
    'email_on_failure': True
}


mm_dag = DAG(
    dag_id=DAG_NAME,
    description='Market Monitor - Job Executor',
    default_args=args,
    schedule_interval='30 0 * * *',  # every day at 00:30
    catchup=False
)


def filter_jobs_run_date(**kwargs):
    logger.info("@ filter_jobs_run_date")
    logger.info(u'Determining which jobs should run today...')
    logger.info(kwargs)
    ti = kwargs['ti']
    logger.info(ti)
    active_jobs = ti.xcom_pull(task_ids=fetch_jobs_task_id)
    logger.info(active_jobs)
    logger.info(type(active_jobs))
    # for i in active_jobs:
    #     logger.info(i)
    #     logger.info(i.get("run_day_of_week"))
    #     logger.info(type(i))

    execution_date = ti.execution_date
    # day_of_week = execution_date.isoweekday() % 7  # so sunday=0, monday=1, etc...
    day_of_week = 0
    logger.info('execution_date = %s', execution_date)
    logger.info('day of week = %s', day_of_week)

    # create list with the jobs need to be run
    jobs_to_run = list(filter(lambda job: job.get("run_day_of_week") == day_of_week, active_jobs))
    logger.info(jobs_to_run)

    return jobs_to_run


def run_if_has_active_job_callable(**kwargs):
    logger.info("@ run_if_has_active_job_callable")
    logger.info(kwargs)
    ti = kwargs['ti']
    logger.info(ti)
    active_jobs = ti.xcom_pull(task_ids=filter_jobs_run_date_task_id)
    logger.info(active_jobs)
    logger.info(type(active_jobs))
    if len(active_jobs) > 0:
        logger.info(u'Having active job to execute...')
        return accounts_run_task_id
    else:
        logger.info(u'No active job to execute...')
        return no_active_task_id


def accounts_run_callable(**kwargs):
    logger.info("@ run_queries_callable")
    logger.info(u'Producing results for each account...')
    logger.info(kwargs)
    ti = kwargs['ti']
    logger.info(ti)
    jobs = ti.xcom_pull(task_ids=filter_jobs_run_date_task_id)
    logger.info(jobs)
    logger.info(type(jobs))

    # dataset_ids = []
    # account_ids = []

    job_details = {}
    for job in jobs:
        logger.info(job)
        acc_id = job.get("acc_id")
        logger.info(acc_id)

        # account_ids.append(acc_id)

        # dataset_id = run_bq_queries(acc_id)
        dataset_id, current_ts = run_bq_queries(acc_id)
        # dataset_ids.append(dataset_id)

        job_details = {
            'name': job.get("name"),
            'acc_id': job.get("acc_id"),
            'dataset_id': dataset_id,
            'run_ts': current_ts
        }

        export_sb_to_gcs(job_details)

        export_om_to_gcs(job_details)

    return job_details


# def export_sb_data_callable(**kwargs):
#     logger.info("@ export_sb_data_callable")
#     logger.info(kwargs)
#     ti = kwargs['ti']
#     logger.info(ti)
#
#     job_details = ti.xcom_pull(task_ids=run_queries_task_id)
#
#     export_sb_to_gcs(job_details)
#
#
# def export_om_data_callable(**kwargs):
#     logger.info("@ export_om_data_callable")
#     logger.info(kwargs)
#     ti = kwargs['ti']
#     logger.info(ti)
#
#     job_details = ti.xcom_pull(task_ids=run_queries_task_id)
#
#     export_om_to_gcs(job_details)


# def kariola_1():
#     logging.info('Sta arxidia mou se grafo...')


# Skip all the previous days that 
latest_only = LatestOnlyOperator(task_id='latest_only', dag=mm_dag)


# malakia_1 = PythonOperator(
#     task_id=job_1_id,
#     dag=mm_dag,
#     provide_context=False,  # if the callable function does not have arguments, you need this
#     python_callable=kariola_1
# )


# malakia_2 = PythonOperator(
#     task_id=job_2_id,
#     dag=mm_dag,
#     provide_context=False,
#     python_callable=kariola_2
# )


fetch_jobs_op = PythonOperator(
    task_id=fetch_jobs_task_id,
    dag=mm_dag,
    provide_context=False,
    python_callable=fetch_jobs
)

filter_jobs_run_date_op = PythonOperator(
    task_id=filter_jobs_run_date_task_id,
    dag=mm_dag,
    python_callable=filter_jobs_run_date
)

run_if_has_active_job = BranchPythonOperator(
    task_id=run_if_has_active_job_task_id,
    dag=mm_dag,
    python_callable=run_if_has_active_job_callable
)

accounts_run = PythonOperator(
    task_id=accounts_run_task_id,
    dag=mm_dag,
    python_callable=accounts_run_callable
)

# export_sb_data = PythonOperator(
#     task_id=export_sb_data_task_id,
#     dag=mm_dag,
#     python_callable=export_sb_data_callable
# )
#
# export_om_data = PythonOperator(
#     task_id=export_om_data_task_id,
#     dag=mm_dag,
#     python_callable=export_om_data_callable
# )

# TODO: Determine the email sender address for the environment variable
email_no_active = EmailOperator(
    task_id=no_active_task_id,
    to=args['email'],
    subject='Market Monitor - Cloud Composer - Airflow DAG skipped',
    html_content='Market Monitor Cloud Composer Airflow DAG was skipped. No active job to execute.',
    dag=mm_dag
)

email_success = EmailOperator(
    task_id='notify_success',
    to=args['email'],
    subject='Market Monitor - Cloud Composer - Airflow DAG finish',
    html_content='Market Monitor Airflow DAG has finished executing.',
    dag=mm_dag
)


# latest_only >> malakia_1 >> malakia_2 >> fetch_jobs_op
# latest_only >> malakia_1 >> malakia_2 >> fetch_jobs_op >> filter_jobs_run_date_op
# latest_only >> malakia_1 >> malakia_2 >> fetch_jobs_op >> filter_jobs_run_date_op >> run_if_has_active_job
latest_only >> fetch_jobs_op >> filter_jobs_run_date_op >> run_if_has_active_job

# run_if_has_active_job.set_downstream([gen_query_files, email_no_active])
run_if_has_active_job.set_downstream([accounts_run, email_no_active])

# will have also an 'export_om_data'
accounts_run >> email_success

# will have also an 'export_om_data'
# email_success.set_upstream([export_sb_data, export_om_data])
