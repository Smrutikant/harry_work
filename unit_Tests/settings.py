import os

from google.cloud import bigquery


# project we run DAG and where create new dataset/tables
project_id = os.environ['PROJECT_ID']

# dataset where mm tables exists
dataset_id_mm_tables = os.environ['DATASET_ID_MM_TABLES']

# project from where we get data for the queries
project_id_datamaster = os.environ['PROJECT_ID_DATAMASTER']

# dataset from where we get data for the queries
dataset_id_datamaster = os.environ['DATASET_ID_DATAMASTER']

# id of the bucket we will store the exported data
output_sb_bucket_id = os.environ['OUTPUT_SB_BUCKET_ID']

# id of the table which we will export
sb_table_id = os.environ['SB_TABLE_ID']

# id of the bucket we will store the exported data
output_om_bucket_id = os.environ['OUTPUT_OM_BUCKET_ID']

# id of the table which we will export
om_table_id = os.environ['OM_TABLE_ID']

# topic_id for the search book pub/sub msg
sb_topic_id = os.environ['SB_TOPIC_ID']

# topic_id for the origin market pub/sub msg
om_topic_id = os.environ['OM_TOPIC_ID']


def get_client():
    client = bigquery.Client(project=project_id)
    return client
