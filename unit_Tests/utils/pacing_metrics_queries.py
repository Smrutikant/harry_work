import logging

from google.cloud import bigquery

from market_monitor import settings

logger = logging.getLogger(__name__)

# Get global variables
client = settings.get_client()
project_id = settings.project_id
dataset_id_mm_tables = settings.dataset_id_mm_tables
project_id_datamaster = settings.project_id_datamaster
dataset_id_datamaster = settings.dataset_id_datamaster


def mm_pacing_market_stay_window_metrics(dataset_id):
    """
    Group by (market, stay date, stay window), filtering to stay dates
    no more than 180 days beyond last observed date.

    The result will contain total & cumulative bookings for each observed
    (stay date, window) combination.

    Input tables:
     - mm_booking_dataset_full: final table containing the features
       for the model, as created by the BQ search/book queries
    """

    table_name = 'mm_pacing_market_stay_window_metrics'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_full'

    query_string = """
    with max_stay_table as (
        -- retain only stay dates up to 180 days beyond the last
        -- observed date; the 'total_bookings_target' is nulled out
        -- for stay dates >= current date
        select DATE_ADD(max(stay_date), interval 180 day) as max_stay_date
        from `{project_id}.{dataset_id}.{t1}`
        where total_bookings_target is not null
    )
    select market, stay_date, stay_window, statistic_week,
        count(distinct dp_id) as partners,
        count(distinct hotel_code) as hotels,
        sum(total_bookings_target) as target_total,
        sum(cumulative_bookings_target) as target_cumul
    from `{project_id}.{dataset_id}.{t1}`
    where stay_date <= (select max_stay_date from max_stay_table)
    group by market, stay_date, stay_window, statistic_week
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_pacing_market_stay_window_grid(dataset_id):
    """
    Creates table containing full grid of (market, stay dates,
    stay window) for:
    - 52 weeks of stay dates, ranging from ~26 weeks prior to
      last observed data to 26 weeks into the future
    - the markets present in 'pacing_market_stay_window_metrics' table,
      i.e. those assigned to the current account
    - the stay windows ranging from 1-51; can we further limit
      this to only windows 1-26?

    Input tables:
     - mm_pacing_market_stay_window_metrics: create by prior 'step 1' query
       under the dataset associated with UtilsBQ instance
     - inf_day_d: map these to stay date
     - inf_week_d: contains mapping between
       cal_week_id and year_ago_week_id & week_end_date_name
    """

    table_name = 'mm_pacing_market_stay_window_grid'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_market_stay_window_metrics'

    query_string = """
    with markets_past_year as (
        -- unique markets in account
        select  distinct market
        from `{project_id}.{dataset_id}.{t1}`
    ),
    stay_windows_past_year as (
        -- get unique stay windows in past year: 0 - 51
        select stay_window from UNNEST(GENERATE_ARRAY(0, 51)) as stay_window
    ),
    stay_date_range as (
        -- get min/max dates over last 52 weeks of stay dates
        select date_sub(max(stay_date), interval 363 day) as min_stay_date,
            max(stay_date) as max_stay_date
        from `{project_id}.{dataset_id}.{t1}`
    ),
    stay_dates_past_year as (
        -- get full grid of stay dates for last 52 weeks: roughly
        -- half will be in the future, the other half in the past
        select distinct cal_date as stay_date
        from `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d`
        where cal_date >= (select min_stay_date from stay_date_range) 
            and cal_date <= (select max_stay_date from stay_date_range)
    ),
    cross_joined_grid as (
        -- create (market, stay date, window) grid from dimensions above
        select market, stay_date, stay_window
        from stay_dates_past_year s
        cross join stay_windows_past_year e
        cross join markets_past_year m
    ),
    dates as (
        -- establish cal_week_id <> year_ago_week_id mapping
        select day.cal_date,
            day.day_of_week,
            day.cal_week_id,
            week.week_end_date_name,
            week.year_ago_week_id
        from `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` day
        join `{project_id_datamaster}.{dataset_id_datamaster}.inf_week_d` week
            on day.cal_week_id = week.cal_week_id
    )
    select g.*,
        -- fields used in downstream step to to match calendar dates
        -- in current year to those in the 3 prior years
        dates_0_yrs_ago.cal_date as date_0_yrs_ago,
        dates_1_yrs_ago.cal_date AS date_1_yrs_ago,
        dates_2_yrs_ago.cal_date AS date_2_yrs_ago,
        dates_3_yrs_ago.cal_date AS date_3_yrs_ago
    from cross_joined_grid g
    -- match stay_date <> cal_date to get (cal_week_id, day_of_week)
    left join dates dates_0_yrs_ago
        on g.stay_date = dates_0_yrs_ago.cal_date
    -- join (cal_week_id, day_of_week) for 0 years & 1 years ago
    left join dates dates_1_yrs_ago
        on dates_0_yrs_ago.year_ago_week_id = dates_1_yrs_ago.cal_week_id 
        and dates_0_yrs_ago.day_of_week = dates_1_yrs_ago.day_of_week
    -- join (cal_week_id, day_of_week) for 1 years & 2 years ago
    left join dates dates_2_yrs_ago
        on dates_1_yrs_ago.year_ago_week_id = dates_2_yrs_ago.cal_week_id 
        and dates_1_yrs_ago.day_of_week = dates_2_yrs_ago.day_of_week
    -- join (cal_week_id, day_of_week) for 2 years & 3 years ago
    left join dates dates_3_yrs_ago
        on dates_2_yrs_ago.year_ago_week_id = dates_3_yrs_ago.cal_week_id 
        and dates_2_yrs_ago.day_of_week = dates_3_yrs_ago.day_of_week
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_pacing_market_stay_window_grid_observed(dataset_id):
    """
    Join current & prior year totals to expanded grid of (market,
    stay dates, windows) and then do a 'union all' to convert data
    to 'long' format with one set of records per stay year.

    This results in a set of records for each stay_year equal to:
    'current' and 'current - K years' for K = 1, 2, 3, with the
    values of (market, stay date, window) repeated across these
    different sets of records.

    Input tables:
     - pacing_market_stay_window_metrics: created by prior 'step 1' query
       under the dataset associated with UtilsBQ instance
     - pacing_market_stay_window_grid: created by prior 'step 2' query
       under the dataset associated with UtilsBQ instance
    """

    table_name = 'mm_pacing_market_stay_window_grid_observed'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_market_stay_window_grid'
    t2 = 'mm_pacing_market_stay_window_metrics'

    query_string = """
    with bookings_0_years_ago as (
        select g.market,
            'current' as stay_year, 
            g.stay_date, 
            g.stay_window,
            coalesce(m.partners, 0) as partners,
            coalesce(m.hotels, 0) as hotels,
            coalesce(m.target_total, 0) as target_total,
            coalesce(m.target_cumul, 0) as target_cumul
        from `{project_id}.{dataset_id}.{t1}` g 
        left join `{project_id}.{dataset_id}.{t2}` m
            on g.market = m.market
            and g.stay_window = m.stay_window
            and g.stay_date = m.stay_date
    ),
    bookings_1_years_ago as (
        select g.market,
            'current - 1 year' as stay_year, 
            g.stay_date, 
            g.stay_window,
            coalesce(m.partners, 0) as partners,
            coalesce(m.hotels, 0) as hotels,
            coalesce(m.target_total, 0) as target_total,
            coalesce(m.target_cumul, 0) as target_cumul
        from `{project_id}.{dataset_id}.{t1}` g 
        left join `{project_id}.{dataset_id}.{t2}` m
            on g.market = m.market
            and g.stay_window = m.stay_window
            and g.date_1_yrs_ago = m.stay_date
    ),
    bookings_2_years_ago as (
        select g.market,
            'current - 2 years' as stay_year, 
            g.stay_date, 
            g.stay_window,
            coalesce(m.partners, 0) as partners,
            coalesce(m.hotels, 0) as hotels,
            coalesce(m.target_total, 0) as target_total,
            coalesce(m.target_cumul, 0) as target_cumul
        from `{project_id}.{dataset_id}.{t1}` g 
        left join `{project_id}.{dataset_id}.{t2}` m
            on g.market = m.market
            and g.stay_window = m.stay_window
            and g.date_2_yrs_ago = m.stay_date
    ),
    bookings_3_years_ago as (
        select g.market,
            'current - 3 years' as stay_year,
            g.stay_date, 
            g.stay_window,
            coalesce(m.partners, 0) as partners,
            coalesce(m.hotels, 0) as hotels,
            coalesce(m.target_total, 0) as target_total,
            coalesce(m.target_cumul, 0) as target_cumul
        from `{project_id}.{dataset_id}.{t1}` g 
        left join `{project_id}.{dataset_id}.{t2}` m
            on g.market = m.market
            and g.stay_window = m.stay_window
            and g.date_3_yrs_ago = m.stay_date
    )
    select * from bookings_0_years_ago
    union all
    select * from bookings_1_years_ago
    union all
    select * from bookings_2_years_ago
    union all
    select * from bookings_3_years_ago
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_pacing_market_stay_grid_future(dataset_id):
    """
    Get estimate of total bookings for future (date, window) based
    on avg of bookings for same date in prior years. These estimates
    will be used in denominator of cumulative percent of bookings
    calculations for future dates.

    Input tables:
     - pacing_market_stay_window_grid_observed: contains grid of (market,
       stay date, window) for (stay date, window) combinations that have
       been observed.
    """

    table_name = 'mm_pacing_market_stay_grid_future'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_market_stay_window_grid_observed'

    query_string = """
    with max_stay_table as (
        -- last stay date with observed data
        select max(stay_date) as max_stay_date
        from `{project_id}.{dataset_id}.{t1}`
        where target_total > 0 and stay_year = 'current'
    ),
    market_year_stay_avg as (
        -- average across windows for given stay_date: remember that total
        -- bookings are repeated across stay windows for a given stay date
        select market, stay_year, stay_date,
            round(avg(target_total),0) as target_total
        from `{project_id}.{dataset_id}.{t1}` 
        where stay_date <= (select max_stay_date from max_stay_table)
        group by market, stay_year, stay_date
    ),
    market_yoy_ratios as (
        -- calculate average of total bookings over the most recent half-year
        -- of elapsed stay dates in both the current and prior years; this
        -- yields two averages whose ratio will be used to adjust our estimate
        -- of total bookings for the current year's future stay dates
        select  c.market, 
            target_current, target_prior,
            -- adjustment factor to apply to future stay/window estimates
            round(target_current / target_prior, 3) as target_yoy_ratio
        from (
            -- avg total bookings over last 6 months of observed bookings
            -- in the most recent year
            select market, stay_year,
                round(avg(target_total),0) as target_current
            from market_year_stay_avg
            where stay_year = 'current'
            group by market, stay_year
        ) c
        join (
            -- avg total bookings over last 6 months of observed bookings
            -- in the prior two years
            select market, stay_year, 
                round(avg(target_total),0) as target_prior
            from market_year_stay_avg
            where stay_year in ('current - 1 year', 'current - 2 year')
            group by market, stay_year
        ) p
        on c.market = p.market
    ),
    -- base estimates of total bookings for future stay dates on
    -- average of observed bookings in same period in prior years,
    -- with dates matched across years by (cal_week_id, day_of_week)
    market_stay_estimates_unscaled as (
        -- no need to retain stay_window dimension in group-by because
        -- total bookings are repeated across windows for given stay date
        select market, stay_date,
            -- average across stay_year by (market, stay date, window)
            round(avg(target_total), 0) as target_total
        from `{project_id}.{dataset_id}.{t1}`
        -- select bookings data from prior years that have been mapped
        -- to future (stay date, window) by (week ID, day of week)
        where date_sub(stay_date, interval 7 * stay_window day) > 
            (select max_stay_date from max_stay_table)
            and stay_year != 'current'
        group by market, stay_date
    )
    -- adjust total bookings by market-level Y/Y ratios
    select 'current' as stay_year, e.market, e.stay_date,
        r.target_yoy_ratio,
        e.target_total as target_unscaled,
        round(e.target_total * r.target_yoy_ratio, 0) as target_scaled
    from market_stay_estimates_unscaled e 
    join market_yoy_ratios r
        on e.market = r.market
    order by market, stay_date
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_pacing_cumul_pct_weekly_by_year(dataset_id):
    """
    Get 'expected cumulative bookings %' for future stay dates and
    historical cumulative bookings % by (market, stay date, window)
    based on prior years.

    Input tables:
     - mm_pacing_market_stay_grid_future: contains grid of future stay dates
       along with their estimated total bookings for each market
     - mm_pacing_market_stay_window_grid_observed: contains grid of (stay
       date, window) for each market and each of the past several
       years ('current', 'current - 1 year', ...)
     - inf_day_d: map these to stay date
     - inf_week_d: contains mapping between
       cal_week_id and year_ago_week_id & week_end_date_name`
    """

    table_name = 'mm_pacing_cumul_pct_weekly_by_year'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_market_stay_grid_future'
    t2 = 'mm_pacing_market_stay_window_grid_observed'

    query_string = """
    with min_future_stay_table as (
        select min(stay_date) as min_future_stay_date
        from `{project_id}.{dataset_id}.{t1}`
    ),
    market_stay_window_observed as (
        select market, stay_date, stay_window, stay_year,
            target_cumul, target_total,
            -- for identifying last observed window within stay date
            row_number() over (
                partition by market, stay_year, stay_date
                order by stay_window
            ) as window_in_date_index
        from `{project_id}.{dataset_id}.{t2}`
        -- keep only stay dates in the future
        where stay_date >= (select min_future_stay_date from min_future_stay_table)
            -- for future stay dates, keep only the stay windows we've seen
            and DATE_SUB(stay_date, interval 7*stay_window day) < 
                (select min_future_stay_date from min_future_stay_table)
            -- window will range from 1-51, but keep only the most recent 
            -- 26 weeks leading up to a stay date since cumulative percent of
            -- bookings tends to be sub-5% in the weeks before that
            and stay_window <= 26
            -- cumulative bookings should be non-null for the
            -- (stay date, windows) that have elapsed
            and target_cumul is not null
    ),
    market_stay_window_observed_last as (
        -- keep only last observed window within each (market, stay date)
        -- since we want to calculate cumulative % of total bookings as of
        -- latest observed date
        select * from market_stay_window_observed
        where window_in_date_index = 1
    ),
    market_stay_window_observed_adj as (
        select orig.market, orig.stay_date, orig.stay_window, 
            orig.stay_year,
            orig.target_cumul, 
            -- since we are joining on stay_year, only the latest year's
            -- target (i.e. the record where stay_year = 'current') will 
            -- get filled in by either the unscaled or scaled average of 
            -- bookings from prior years
            -- estimate of total bookings based on avg of prior years,
            -- without adjusting by a Y/Y factor: not surfaced in Looker
            -- but may be useful for debugging in the future
            coalesce(adj.target_unscaled, orig.target_total) as target_total_unscaled,
            -- estimate of total bookings based on avg of prior years,
            -- after adjusting by a Y/Y factor: used to calculate the
            -- cumulative % of bookings surfaced in Looker
            coalesce(adj.target_scaled, orig.target_total) as target_total_scaled
    from market_stay_window_observed_last orig
    left join `{project_id}.{dataset_id}.{t1}` adj
        on orig.market = adj.market
        and orig.stay_date = adj.stay_date
        and orig.stay_year = adj.stay_year
    ),
    date_to_week_end_mapping as (
        -- establish date <> week-ending date mapping in orer to aggregate
        -- from stay date to stay week level
        select day.cal_date,
            cast(week.week_end_date_name as date) as stay_week_end_date
        from `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` day
        join `{project_id_datamaster}.{dataset_id_datamaster}.inf_week_d` week
            on day.cal_week_id = week.cal_week_id
        where day.cal_date 
        between (select min(stay_date) from market_stay_window_observed_adj)
            and (select max(stay_date) from market_stay_window_observed_adj)
    )
    -- aggregate across stay_date to stay_week_end_date to smooth out
    -- the cumulative % of bookings on which pacing alerts will be based; 
    -- note that we ignore the 'stay_window' key since we kept only the
    -- last observed stay window per date above
    select market, stay_week_end_date, stay_year,
        min(stay_window) as latest_window,
        sum(target_cumul) as target_cumul, 
        sum(target_total_unscaled) as target_total_unscaled,
        sum(target_total_scaled) as target_total_scaled,
        -- retained for debugging purposes
        round(sum(target_cumul) / sum(target_total_unscaled), 4) as cumul_pct_unscaled,
        -- surfaced in Looker application
        round(sum(target_cumul) / sum(target_total_scaled), 4) as cumul_pct_scaled
    from market_stay_window_observed_adj o
    join date_to_week_end_mapping m
        on o.stay_date = m.cal_date
    group by market, stay_week_end_date, stay_year
    order by market, stay_week_end_date, stay_year
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_pacing_cumul_pct_weekly_comp(dataset_id):
    """
    Compare current vs. prior years' cumulative % of bookings,
    with cumulative % of bookings for stay year = 'current' or
    'prior' logged in different rows.

    Input tables:
     - mm_pacing_cumul_pct_weekly_by_year: contains cumulative percent
       of bookings for each future stay week through the latest
       observed stay window, for the current year and each prior
       year
    """

    table_name = 'mm_pacing_cumul_pct_weekly_comp'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_cumul_pct_weekly_by_year'

    query_string = """
    with cumul_pct_prior_years as (
        select market, stay_week_end_date, stay_year, latest_window,
            max(cumul_pct_unscaled) as cumul_pct_unscaled,
            max(cumul_pct_scaled) as cumul_pct_scaled
        from (
            -- calculate historical cumulative percent of bookings
            -- as median across prior years
            select market, stay_week_end_date, 
                'prior' as stay_year, latest_window,
                percentile_cont(cumul_pct_unscaled, 0.5) over (
                    partition by market, stay_week_end_date) 
                    as cumul_pct_unscaled,
                percentile_cont(cumul_pct_scaled, 0.5) over (
                    partition by market, stay_week_end_date) 
                    as cumul_pct_scaled
            from `{project_id}.{dataset_id}.{t1}`
            where stay_year != 'current'
        ) t
        group by market, stay_week_end_date, stay_year, latest_window
    ),
    cumul_pct_current_year as (
        select market, stay_week_end_date, stay_year, latest_window,
            cumul_pct_unscaled, cumul_pct_scaled
        from `{project_id}.{dataset_id}.{t1}`
        where stay_year = 'current'
    )
    select *
    from (
        select * from cumul_pct_current_year
        union all
        select * from cumul_pct_prior_years
    ) t 
    order by market, stay_week_end_date, stay_year
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_pacing_alert_inputs(dataset_id):
    """
    Flatten prior and current years' cumulative % bookings, calculate
    their percentage difference, and define several examples of alert
    criteria.

    Input tables:
     - mm_pacing_cumul_pct_weekly_comp: contains cumulative percent
       of bookings for each future stay week through the latest
       observed stay window, for stay_year = 'current' and 'prior'
    """

    table_name = 'mm_pacing_alert_inputs'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_pacing_cumul_pct_weekly_comp'

    query_string = """
    with cumul_pct_weekly_comp_flat as (
        select p.*, 
        cumul_pct_prior,
        case
            when cumul_pct_prior > 0 then ROUND(cumul_pct_current/cumul_pct_prior - 1.0, 4)
            else null
        end as pct_diff
        from (
            select market, stay_week_end_date, latest_window, 
                cumul_pct_scaled as cumul_pct_current
            from `{project_id}.{dataset_id}.{t1}`
            where stay_year = 'current'
        ) p
        join (
            select market, stay_week_end_date, latest_window, 
                cumul_pct_scaled as cumul_pct_prior
            from `{project_id}.{dataset_id}.{t1}`
            where stay_year = 'prior'
        ) c
        using (market, stay_week_end_date)
    )
    select *,
        -- below are examples of various alerting criteria
        -- based on arbitrary thresholds
        case
            when pct_diff < -0.333 then true
            else false
        end as alert_v0,
        -- only stay dates within 12 weeks of current date are
        -- eligible for pacing alerts
        case
            when latest_window <= 12 and pct_diff < -0.333 then true
            else false
        end as alert_v1,
        -- with historical cumulative % < 10%, we're likely months
        -- away from the stay date, resulting in lots of noise
        case
            when 0.10 <= cumul_pct_prior and 
                 cumul_pct_prior < 0.20 and
                 pct_diff < -0.50 
                 then true
            when 0.20 <= cumul_pct_prior and
                 pct_diff < -0.333
                 then true
            else false
        end as alert_v2
    from cumul_pct_weekly_comp_flat
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref
