from datetime import datetime

from google.cloud import bigquery

from .search_book_queries import *
from .origin_market_queries import *
from .pacing_metrics_queries import *

from market_monitor import settings

logger = logging.getLogger(__name__)

# Get global variables
client = settings.get_client()


def dataset_exists(dataset_id):
    """
    Function to check if a dataset already exists

    :param dataset_id: ID of the dataset we want to check
    :return: True if dataset exists, False if not
    """

    # get a list of the dataset_ids
    client_ds_ids = [
        ds.dataset_id for ds in client.list_datasets()
    ]

    return dataset_id in client_ds_ids


def create_dataset(account_id):
    """
    Function to create a dataset

    :param account_id: ID of the account for which we will create the dataset
    :return: The dataset_id of the newly created dataset and the timestamp when it has been created
    """

    logger.info(u'Creating dataset for account {}...'.format(account_id))

    current_ts = datetime.utcnow()

    dataset_id = "acct_" + str(account_id) + "__ts_" + current_ts.strftime('%Y%m%dt%H%M%S')

    # Check if dataset already exists
    exists = dataset_exists(dataset_id)

    if exists:
        logger.info(u'Dataset {} exists!'.format(dataset_id))
    else:
        # Creating a DatasetReference using a chosen dataset ID
        dataset_ref = client.dataset(dataset_id)

        # Construct a full Dataset object to send to the API
        dataset = bigquery.Dataset(dataset_ref)

        # Specify the geographic location where the dataset should reside
        dataset.location = 'US'

        # Send the dataset to the API for creation
        # Raises google.api_core.exceptions.Conflict if the Dataset already exists within the project
        dataset = client.create_dataset(dataset)

        logger.info(u'Dataset {} created!'.format(dataset_id))

        # set expiration day for the dataset
        one_day_ms = 24 * 60 * 60 * 1000  # in milliseconds
        one_month_ms = one_day_ms * 30
        dataset.default_table_expiration_ms = one_month_ms
        dataset = client.update_dataset(
            dataset, ['default_table_expiration_ms'])  # API request

    return dataset_id, current_ts


def create_table_query(query_string, table_ref):
    """
    Function to call bigquery library and execute query

    :param query_string: string that includes the query to be executed
    :param table_ref: reference to the table where query result will be stored
    :return: No return. After the end of this function the table represented by table_ref will have the query result
    """

    job_config = bigquery.QueryJobConfig()
    job_config.destination = table_ref
    job_config.write_disposition = 'WRITE_TRUNCATE'

    # submit API request to start the query
    query_job = client.query(
        query_string, location='US', job_config=job_config)

    query_job.result()

    logger.info(u'Query results loaded to table {}'.format(table_ref.path))


def query_with_return(query_string):
    """
    Function which executes a query and returns a list of the query results

    :param query_string: string that includes the query to be executed
    :return: list with the query results
    """

    job_config = bigquery.QueryJobConfig()

    # submit API request to start the query
    query_job = client.query(
        query_string, location='US', job_config=job_config)

    results = query_job.result()
    results_list = list(results)

    return results_list


def create_table_schema(schema, table_ref):
    """
    Function which creates a schema for an empty table

    :param schema: list containing schema
    :param table_ref: reference to the table we want to create
    :return: No return. After the end of this function an empty table with the given schema will have been created
    """

    table = bigquery.Table(table_ref, schema=schema)

    client.create_table(table)

    logger.info('Table {} has been created'.format(table_ref.path))


def search_book_query_driver(account_id, dataset_id):
    """
    Function who calls the functions related with the queries needed for the search book data and pacing metric data

    :param account_id: ID of the account
    :param dataset_id: Dataset ID where we create new tables
    :return: No return value.
            After the end of this function we will have a dataset with all the tables created
    """

    logger.info(u'Running search book query for account {}...'.format(account_id))

    query_string, table_ref = mm_markets_of_interest(account_id, dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_markets_sql_text(account_id, dataset_id)
    create_table_query(query_string, table_ref)

    query_string = mm_only_markets_sql_text(dataset_id)
    market_ids_sql_texts = query_with_return(query_string)
    query_string, table_ref = mm_market_hotel_mapping(dataset_id, market_ids_sql_texts)
    if len(query_string) > 6:
        # if we have data to insert to mm_market_hotel_mapping table
        create_table_query(query_string, table_ref)
    else:
        # if we do not have data, we just create an empty table
        schema = [
            bigquery.SchemaField('market_id', 'INTEGER', mode='REQUIRED'),
            bigquery.SchemaField('str_number', 'INTEGER', mode='REQUIRED'),
        ]
        create_table_schema(schema, table_ref)

    query_string, table_ref = mm_market_city_mapping(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_us_properties(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_properties_base_staging(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_property_distance_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_property_distance(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_property_word(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_property_match_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_property_match(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_properties(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_markets(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_book(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_search(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_staging(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_dirty(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ihg_alos_map(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ihg_ekv_hotel_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ihg_ekv_hotel_staging(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel_mid(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ihg_ekv_hotel_insert(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_hotel(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_market_airport_mapping(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_destination_airport(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_distinct_destination_airport(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_flight_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_flight_book(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_flight_search(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_flight_staging(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_ekv_flight(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_base_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_base_t2(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_base_t3(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_metric_calculation_base_staging(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_market_coordinates(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_coordinates(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_metro_coordinates(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_metro_market_distance(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_metro_distance(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_base_metrocode_map(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_metric_calculation_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_metric_calculation_point(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_metric_calculation_cumulative(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_target(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_dataset_base_feature(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_hotel_summary_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_hotel_summary(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_hc_search_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_search_by_market_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_market_stay_stat_d(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_search_by_market_t3(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_search_by_market_t4(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_stay_stat_d(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_search_by_market_t6(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_m_search_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_cm_search_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_hc_book_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_book_by_market_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_book_by_market_t3(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_book_by_market_t4(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_book_by_market_t6(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_m_book_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel_cm_book_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_hotel(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_flight_summary_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_flight_summary_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_flight_summary(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_search_by_market_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_search_by_market_t3(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_flight_m_search_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_search_by_market_t4(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_search_by_market_t6(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_flight_cm_search_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_book_by_market_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_book_by_market_t3(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_book_by_market_t4(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_flight_book_by_market_t6(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_flight_m_book_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_flight_cm_book_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_flight(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_holidays(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_t1(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_t2(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_t3_hotel(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_t4(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_t5_flight(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_booking_dataset_full(dataset_id)
    create_table_query(query_string, table_ref)

    logger.info(u'Running pacing metric query for account {}...'.format(account_id))

    query_string, table_ref = mm_pacing_market_stay_window_metrics(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_market_stay_window_grid(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_market_stay_window_grid_observed(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_market_stay_grid_future(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_cumul_pct_weekly_by_year(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_cumul_pct_weekly_comp(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_pacing_alert_inputs(dataset_id)
    create_table_query(query_string, table_ref)

    logger.info(u'Running multiplier query for account {}...'.format(account_id))

    query_string, table_ref = mm_data_partner_hotel_stay_info(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_data_partner_hotel_stay_info_total_bookings(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_hotel_month_pct_bookings(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_occupancy_rate_stats(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_occupancy_rate_multipliers(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_occupancy_rate_scaled_bookings(dataset_id)
    create_table_query(query_string, table_ref)


def origin_market_query_driver(account_id, dataset_id):
    """

    Function who calls the functions related with the queries needed for the origin market data

    :param account_id: ID of the account
    :param dataset_id: Dataset ID where we create new tables
    :return: No return value.
            After the end of this function we will have a dataset with all the tables created
    """

    logger.info(u'Running origin market query for account {}...'.format(account_id))

    query_string, table_ref = mm_metric_aggregate_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_aggregate(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_base(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_analytics_a(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_analytics_b(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_analytics_c(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_analytics_d(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_metric_analytics(dataset_id)
    create_table_query(query_string, table_ref)

    query_string, table_ref = mm_origin_market_report(dataset_id, account_id)
    create_table_query(query_string, table_ref)


def run_bq_queries(account_id):
    """
    Function which initiates all the required actions to run the queries on BigQuery

    :param account_id: ID of the account
    :return: The dataset_id of the newly created dataset and the timestamp when it has been created
    """

    logger.info(u'Running queries for account {}...'.format(account_id))

    # preparing new dataset
    dataset_id, current_ts = create_dataset(account_id)

    # running queries
    search_book_query_driver(account_id, dataset_id)

    origin_market_query_driver(account_id, dataset_id)

    return dataset_id, current_ts
