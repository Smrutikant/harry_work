import logging

from google.cloud import bigquery

from market_monitor import settings

logger = logging.getLogger(__name__)

# Get global variables
client = settings.get_client()
project_id = settings.project_id
dataset_id_mm_tables = settings.dataset_id_mm_tables
project_id_datamaster = settings.project_id_datamaster
dataset_id_datamaster = settings.dataset_id_datamaster

"""
All the below functions are only using as arguments one or both of the following variables
- account_id: id of the account we run
- dataset_id: dataset id created for this tun
"""


def mm_markets_of_interest(account_id, dataset_id):
    """
    Get market_ids of target & competitive markets as stored in market hierarchy table

    Input tables:
    - mm_market_d
    - mm_market_hierarchy
    """

    table_name = 'mm_markets_of_interest'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    with primary_market as (
        select id as primary_market_id
        from `{project_id}.{dataset_id_mm_tables}.mm_market_d`
        where account_id = {account_id}
        and status_id = 1
    ),
    competitive_markets as (
        select competitive_market_id
        from `{project_id}.{dataset_id_mm_tables}.mm_market_hierarchy`
        join primary_market
        using (primary_market_id)
        where status_id = 1
    )
    select primary_market_id as market_id from primary_market
    union all
    select competitive_market_id as market_id from competitive_markets ;
    """.format(
        project_id=project_id,
        dataset_id_mm_tables=dataset_id_mm_tables,
        account_id=str(account_id)
    )

    return query_string, table_ref


def mm_markets_sql_text(account_id, dataset_id):
    """
    Get sql_texts of target & competitive markets as stored in mm_market_d table

    Input tables:
    - mm_market_d
    - mm_market_hierarchy
    """

    table_name = 'mm_markets_sql_text'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    with T1 as (
        select id as primary_market_id
        from `{project_id}.{dataset_id_mm_tables}.mm_market_d`
        where account_id = {account_id}
        and status_id = 1
    ),
    T2 as (
        select competitive_market_id id
        from T1
        join `{project_id}.{dataset_id_mm_tables}.mm_market_hierarchy`
            using (primary_market_id)
        where status_id = 1
        union all
        select primary_market_id id from T1)
    select id market_id,
        sql_text
    from `{project_id}.{dataset_id_mm_tables}.mm_market_d` as m
    join T2
        using (id)
    """.format(
        project_id=project_id,
        dataset_id_mm_tables=dataset_id_mm_tables,
        account_id=str(account_id)
    )

    return query_string, table_ref


def mm_only_markets_sql_text(dataset_id):
    """

    :param dataset_id:
    :return:
    """
    # Get a list of all the market_id for this account
    t1 = 'mm_markets_sql_text'

    query_string = """
    select market_id, sql_text
    from `{project_id}.{dataset_id}.{t1}`
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string


def mm_market_hotel_mapping(dataset_id, market_ids_sql_texts):
    """
    Create market-str_number (hotel) mapping table
    Query is dynamic created based on the data that we have on each run

    Input tables:
    - mm_markets_sql_text
    """

    # Create new table
    table_name = 'mm_market_hotel_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)

    counter = 0
    query_string = 'with '
    for row in market_ids_sql_texts:
        temp_string = """T{market_id} as (
            select {market_id} as market_id, str_number
            from `{project_id_datamaster}.{dataset_id_datamaster}.inf_hotel_str_d`
            where {sql_text}
        )
        """.format(
            market_id=row.market_id,
            project_id_datamaster=project_id_datamaster,
            dataset_id_datamaster=dataset_id_datamaster,
            sql_text=row.sql_text
        )

        if counter < (len(market_ids_sql_texts) - 1):
            temp_string = temp_string + ','

        query_string = query_string + temp_string

        counter += 1

    counter = 0
    for row in market_ids_sql_texts:
        temp_string = """
        select * from T{market_id}
        """.format(
            market_id=row.market_id
        )

        if counter < (len(market_ids_sql_texts) - 1):
            temp_string = temp_string + 'union all'

        query_string = query_string + temp_string

        counter += 1

    return query_string, table_ref


def mm_market_city_mapping(dataset_id):
    """
    Create market-city mapping from `inf_hotel_str_d`

    Input tables:
    - mm_market_hotel_mapping
    - mm_markets_of_interest
    - inf_hotel_str_d
    """

    table_name = 'mm_market_city_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_hotel_mapping'
    t2 = 'mm_markets_of_interest'

    query_string = """
    select distinct m.market_id,
        d.name as market,
        lower(h.city) as city,
        lower(h.state) as state_code,
        case
            when lower(h.mailing_country) = 'united states' then 'us'
            else 'n/a'
        end as country_code
        from `{project_id_datamaster}.{dataset_id_datamaster}.inf_hotel_str_d` as h
        join `{project_id}.{dataset_id}.{t1}` as m
            using (str_number)
        join `{project_id}.{dataset_id_mm_tables}.mm_market_d` as d
            on m.market_id = d.id
        join `{project_id}.{dataset_id}.{t2}` as mi
            on m.market_id = mi.market_id
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        dataset_id_mm_tables=dataset_id_mm_tables,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_us_properties(dataset_id):
    """
    `mm_us_properties` table creation:
    --  * `properties` table: originally used to allow hotels to run different ads by geo
    --  * join properties against custom `mm_us_state_lookup` to bring in state
    --    codes, which are used when the 'state' in `properties` does not represent
    --    an abbreviated US state name
    --  * further attempt to normalize state codes in the CASE clause
    --  * keep only property records where airport_code is not null and not equal
    --    to external_id, or where airport_code is null
    --  * note that 'external_id' is renamed below to 'hotel_code'

    -- output: ID (property) / advertiser ID / external ID / company ID / ...?

    Input tables:
    - properties
    - mm_us_state_lookup
    """

    table_name = 'mm_us_properties'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    select p.id,
        p.advertiser_id,
        lower(trim(p.external_id)) as external_id,
        p.brand_id,
        p.external_brand_id,
        p.company_id,
        p.external_company_id,
        lower(trim(p.name)) as name,
        p.lat,
        p.lon,
        lower(trim(p.address_1)) as address_1,
        lower(trim(p.address_2)) as address_2,
        lower(trim(p.city)) as city,
        case
            when length(lower(trim(p.state))) > 2 and usl.code is not null then lower(trim(usl.code))
            when lower(trim(p.state)) = 'new york state' then 'ny'
            when lower(trim(p.state)) = 'washington d.c.' then 'dc'
            when lower(trim(p.state)) = 'd.c.' then 'dc'
            when lower(trim(p.state)) = 'island of hawaii' then 'hi'
            else lower(trim(p.state))
            end state,
        'us' as country,
        p.zip,
        p.airport_code
    from `{project_id_datamaster}.{dataset_id_datamaster}.properties` as p
    left join `{project_id_datamaster}.{dataset_id_datamaster}.mm_us_state_lookup` as usl
        on (lower(trim(p.state)) = lower(trim(usl.name)))
    where lower(trim(p.country)) in ('us','united states','usa')
        and (
        (p.airport_code is not null and p.external_id != p.airport_code)
        or (p.airport_code is null)
        )
    """.format(
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster
    )

    return query_string, table_ref


def mm_properties_base_staging(dataset_id):
    """
    -- join market/city mapping in `mm_market_mapping_hotel` by against
    -- `mm_us_properties` and `advertisers` to bring in hotel info (name,
    -- code, lat/lon) and advertiser info (dp_id) into single table
    --  * join `mm_us_properties` against advertisers, matching on advertiser_id
    --  * join `mm_market_mapping_hotel` and `mm_us_properties` on city, state,
    --    country, which may result in duplicates
    --  * certain hotels may be repeated across more than one record;
    --  * subsequent steps will try to identify and eliminate near-duplicates
    --    hotels based on distances from one another & hotel name similarity

    --  output: dp_id / hotel / coordinates / city / market

    Input tables:
    - mm_us_properties
    - mm_market_city_mapping
    - advertisers
    """

    table_name = 'mm_properties_base_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_us_properties'
    t2 = 'mm_market_city_mapping'

    query_string = """
    select distinct
        a.dp_id,
        p.name as hotel_name,
        lower(trim(p.external_id)) as hotel_code,
        p.lat as latitude,
        p.lon as longitude,
        mm.city,
        mm.state_code,
        mm.country_code,
        mm.market
        from `{project_id}.{dataset_id}.{t1}` as p
        join `{project_id_datamaster}.{dataset_id_datamaster}.advertisers` as a
            on (p.advertiser_id = a.id)
        join `{project_id}.{dataset_id}.{t2}` as mm
            on if(mm.city is not null, (p.city = mm.city), null)
            and if(mm.state_code is not null, (p.state = mm.state_code), null)
            and if(mm.country_code is not null, (p.country = mm.country_code), null)
            where a.category_id = 1
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_property_distance_base(dataset_id):
    """
    -- step to identify near-duplicate hotel entries
    --  * first assign unique IDs to each hotel_name in the table created
    --    above, `mm_properties_base_staging`, which may have duplicates
    --  * filter down to DP IDs in `data_providers`
    --  * cross-join the ~117 records in `mm_properties_base_staging`
    --    against STR hotel data in `inf_hotel_str_d`
    --  * find hotel pairs with distance < 1.0 mile, where distance is
    --    calculated using lat/lon for hotels in `mm_properties_base_staging`
    --    and lat/lon for hotels in STR data
    --  * check which hotels are actually the same by checking for hotels
    --    with very similar coordinates
    --  * near-duplicates can appear because a property can show up multiple
    --    times for a given advertiser or across advertisers
    --  * define a 'distance_rank' by partitioning on 'hotel_code' and
    --    ordering by distance in descending order; note that this is not
    --    used downstream
    --  * note that the vast majority of these hotel pairs will be discarded
    --    downstream because the hotels' DP IDs do not match; these represent
    --    hotels from different chains that are located close to one another

    --  output: dp_id / hotel_code / dp_id from STR table / str_number / ...

    -- Colin's TODOs and references
    --  * look at ADR variability between markets
    --  * explore features that capture variability between markets
    --  * http://daynebatten.com/2015/09/latitude-longitude-distance-sql/

    -- additional TODOs:
    --  * remove 'distance_rank' calculation
    --  * any way to generalize the hotel parent companies listed in the CASE
    --    clause for defining hd_dp_id? how do we keep this list updated?
    --    Could be done as a separate process offline.

    Input tables:
    - mm_properties_base_staging
    - data_providers
    - inf_hotel_str_d
    """

    table_name = 'mm_property_distance_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties_base_staging'

    query_string = """
    with p_id as (
        select p.hotel_name,
            row_number() over (order by p.hotel_name) as id
        from `{project_id}.{dataset_id}.{t1}` as p
        group by 1
    ),
    t2 as (
        select distinct
        p.hotel_code,
        p.dp_id as p_dp_id,
        p_id.id as p_id,
        trim(REGEXP_REPLACE(lower(p.hotel_name), r"[^a-zA-Z0-9_]+", ' ')) as p_hotel_name,
        p.latitude as p_latitude,
        p.longitude as p_longitude,
        hd.parent_company,
        case
            -- is this list exhaustive? when should we update it?
            when hd.parent_company = 'Wyndham Worldwide' then 970
            when hd.parent_company = 'Choice Hotels International' then 1040
            when hd.parent_company = 'Marriott International' then 1057
            when hd.parent_company = 'Intercontinental Hotels Group' then 1168
            when hd.parent_company = 'Hilton Worldwide' then 820
            when hd.parent_company = 'Accor Company' then 1221
            when hd.parent_company = 'Best Western Company' then 1837
            when hd.parent_company = 'Best Western Hotels & Resorts' then 1837
            when hd.parent_company = 'Hyatt' then 1195
            when hd.parent_company = 'Shangri-La Hotels' then 2043
            when hd.parent_company = 'Starwood Hotels & Resorts' then 1062
            when hd.parent_company = 'LQ Management LLC' then 1090
            when hd.parent_company = 'Extended Stay Hotels' then 1306
        end hd_dp_id,
        hd.str_number as hd_id,
        trim(REGEXP_REPLACE(lower(hd.hotel_name), r"[^a-zA-Z0-9_]+", ' ')) as hd_hotel_name,
        hd.latitude as hd_latitude,
        hd.longitude as hd_longitude,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((p.latitude - hd.latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((hd.latitude) * ACOS(-1) /180)) *
                    (cos((p.latitude) * ACOS(-1) /180)) *
                    pow( (sin(((p.longitude - hd.longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
        from `{project_id}.{dataset_id}.{t1}` as p
        join p_id
            on (p.hotel_name = p_id.hotel_name)
        join `{project_id_datamaster}.{dataset_id_datamaster}.data_providers` as dp
            on (p.dp_id = dp.id),
        `{project_id_datamaster}.{dataset_id_datamaster}.inf_hotel_str_d` as hd
    ),
    t3 as (
        select *
        from t2
        where distance_miles < 1.0
    )
    select t3.*,
        row_number() over(
            partition by hotel_code
            order by distance_miles desc
        ) as distance_rank
    from t3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_property_distance(dataset_id):
    """
    -- further processing on `mm_property_distance_base` hotel pairs
    --  * add ID column based on row_number() across all records
    --  * keep only records where DP IDs in `mm_properties_base_staging`
    --    match those defined based on STR parent_company
    --  * each hotel pair now has the key set: (id, p_id, hd_id) where
    --    id = overall row number, p_id = ID for each unique hotel_name,
    --    hd_id = 'str_number' in STR hotel data

    -- output:
    --  * using original market-city mapping: 273 remaining hotel pairs
    --  * using updated market-city mapping: 202 remaining hotel pairs

    Input tables:
    - mm_property_distance_base
    """

    table_name = 'mm_property_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance_base'

    query_string = """
    select row_number() over (order by hotel_code, hd_hotel_name) as id,
        pd.*
    from `{project_id}.{dataset_id}.{t1}` as pd
    where p_dp_id = hd_dp_id
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


"""
-- in the next several queries we try to identify duplicate hotels in 
-- `mm_property_distance` on the basis of how many matching words they 
-- have in their hotel names
"""


def mm_property_word(dataset_id):
    """
    -- break up the words in the names of both hotels in each hotel pair
    -- and retain only the matching words
    --  * each hotel pair now has the key set: (id, p_id, hd_id) where
    --    id = overall row number, p_id = ID for each unique hotel_name,
    --    hd_id = 'str_number' in STR hotel data
    --  * split up 'p_hotel_name' representing the hotel name taken
    --    from `mm_properties_base_staging` into tokens
    --  * split up 'hd_hotel_name' representing the hotel name taken
    --    from STR data into tokens
    --  * join the hotel name tokens based on `mm_properties_base_staging`
    --    with the tokens based on STR hotel data by (id, p_id, hd_id);
    --    in this join, every token ('word') on LHS will get matched with
    --    every word on RHS;
    --  * within each hotel pair identified by (id, p_id, hd_id), flag any
    --    matching word pairs, where word1 comes from the hotel name in
    --    `mm_properties_base_staging` and word2 comes from the hotel name
    --    in the STR data;
    --  * for each (id, p_id, p.word), rank in desc order each hotel name
    --    token in the STR data according to whether it matches with p.word;
    --    keep only records where this rank = 1; so if a given p.word
    --    matches a token in the STR hotel name, it will get retained; if
    --    a given p.word doesn't match any tokens in the STR hotel name, one
    --    of the (p.word, hd_word) records will get selected randomly (?)

    -- output: keys = (id, p_id, h_id), values: p_word, hd_word
    --  * where p_word = token in hotel name from `mm_property_distance` and
    --    hd_word = token in hotel name from STR data
    --  * the cardinality of the output table will be based on the unique
    --    values of (id, p_id, word), where p_id and word come from the
    --    `mm_property_distance` table

    -- question: could we get comparable results using NZ's `le_dst()`
    -- function for calculating Levenshtein edit distance between strings?

    -- data sample: (p_word, hd_word) pairs before filtering on rank = 1
    -- for a given (id, p_id, hd_id) key set

    --  ID | P_ID | HD_ID |    P_WORD    |   HD_WORD    | MATCH | RANK
    -- ----+------+-------+--------------+--------------+-------+------
    --   1 |    9 | 31389 | beach        | beach        | t     |    1
    --   1 |    9 | 31389 | beach        | beachfront   | f     |    2
    --   1 |    9 | 31389 | beach        | myrtle       | f     |    3
    --   1 |    9 | 31389 | beach        | inn          | f     |    4
    --   1 |    9 | 31389 | beach        | days         | f     |    5
    --   1 |    9 | 31389 | days         | days         | t     |    1
    --   1 |    9 | 31389 | days         | beachfront   | f     |    2
    --   1 |    9 | 31389 | days         | myrtle       | f     |    3
    --   1 |    9 | 31389 | days         | beach        | f     |    4
    --   1 |    9 | 31389 | days         | inn          | f     |    5
    --   1 |    9 | 31389 | front        | beach        | f     |    1
    --   1 |    9 | 31389 | front        | inn          | f     |    2
    --   1 |    9 | 31389 | front        | myrtle       | f     |    3
    --   1 |    9 | 31389 | front        | beachfront   | f     |    4
    --   1 |    9 | 31389 | front        | days         | f     |    5
    -- ...

    Input tables:
    - mm_property_distance
    """

    table_name = 'mm_property_word'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_distance'

    query_string = """
    with t1 as (
        select *, SPLIT(pd.p_hotel_name, " ") as v
        from `{project_id}.{dataset_id}.{t1}` as pd
    ),
    p as (
        select distinct id, p_id, hd_id, trim(value) as word
        from t1 cross join unnest(v) as value
    ),
    t2 as (
        select *, SPLIT(pd.hd_hotel_name, " ") as v
        from `{project_id}.{dataset_id}.{t1}` as pd
    ),
    hd as (
        select distinct id, p_id, hd_id, trim(value) as word
        from t2 cross join unnest(v) as value
    ),
    pw1 as (
        select id,
            p.p_id,
            hd.hd_id,
            p.word as p_word,
            hd.word as hd_word,
            p.word = hd.word as match
        from p
        left join hd
            using (id, p_id, hd_id)
    ),
    pw2 as (
        select pw1.*,
            row_number() over (
                partition by pw1.id, pw1.p_id, pw1.p_word
                order by match desc) as rank
        from pw1
    )
    select pw2.id,
        pw2.p_id,
        pw2.hd_id,
        pw2.p_word,
        pw2.hd_word
    from pw2
    where pw2.rank = 1
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_property_match_base(dataset_id):
    """
    -- calculate hotel name token match rate for each (id, p_id, hd_id)
    -- key set, where id = overall row number, p_id = ID for each unique
    -- hotel_name, hd_id = 'str_number' in STR hotel data
    --  * 'match' = # of tokens in hotel name that match a token in the
    --     associated hotel name taken from the STR data
    --  * 'match_rate' = % of words in `mm_property_distance`'s hotel name
    --    that match a word in the STR data's hotel name
    --  * keep only hotel pairs, as identified by (id, p_id, hd_id), with
    --    a match rate > 0, i.e. having at least one matching token in
    --    their names

    -- output: the hotel pairs remaining after filtering out pairs without
    -- any matching words

    Input tables:
    - mm_property_word
    - mm_property_distance
    """

    table_name = 'mm_property_match_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_word'
    t2 = 'mm_property_distance'

    query_string = """
    with a as (
        select pw.id,
            pw.p_id,
            pw.hd_id,
            sum(if(pw.p_word = pw.hd_word,1,0)) as match,
            count(*) as records
        from `{project_id}.{dataset_id}.{t1}` as pw
        group by 1,2,3
    ),
    b as (
        select *,
            (1.0 * match / records) as match_rate
        from a
    )
    select b.id,
        pd.hotel_code,
        pd.p_dp_id,
        b.p_id,
        pd.p_hotel_name,
        pd.hd_dp_id,
        b.hd_id,
        pd.hd_hotel_name,
        b.match,
        b.records,
        b.match_rate,
        pd.distance_miles,
        row_number() over (
            partition by b.p_id
            order by b.match_rate desc, pd.distance_miles) as match_rank_p
    from b
    join `{project_id}.{dataset_id}.{t2}` as pd using (id)
    where b.match_rate != 0
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_property_match(dataset_id):
    """
    -- select the hotel in STR master data having the most similar name based on
    -- hotel name token match rates and distance

    --  * input data consists of hotel pairs remaining in `mm_property_match_base`
    --    (i.e. those with at least one matching hotel name token)
    --  * for each p_id (hotel ID defined in `mm_properties_base_staging`), select
    --    the associated hotel in the STR data having the most similar name based on
    --    the match_rate and distance in miles (match_rank_p)
    --  * then keep only the remaining hotel pairs whose hotel names have multiple
    --    matching tokens, whose DP IDs match, and having the top match rate within
    --    each 'hd_id' (str_number in the STR data)
    --  * question: why do we need to filter on match_rank_hd = 1 here after already
    --    filtering on 'match_rank_p' = 1?

    Input tables:
    - mm_property_match_base
    """

    table_name = 'mm_property_match'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match_base'

    query_string = """
    with pm as (
        select pwa.*,
            row_number() over (
                partition by pwa.hd_id
                order by pwa.match_rate desc) as match_rank_hd
        from `{project_id}.{dataset_id}.{t1}` as pwa
        where pwa.match_rank_p = 1
    )
    select  pm.id,
        pm.hotel_code,
        pm.p_dp_id,
        pm.p_id,
        pm.p_hotel_name,
        pm.hd_dp_id,
        pm.hd_id,
        pm.hd_hotel_name,
        pm.match,
        pm.records,
        pm.match_rate,
        pm.distance_miles
    from pm
    where pm.match_rank_hd = 1
        and pm.match > 1
        and pm.records > 1
        and pm.p_dp_id = pm.hd_dp_id
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_properties(dataset_id):
    """
    -- de-duplicated market / hotel / city / state table
    --  * for hotels remaining in de-duplicated `mm_property_match`, join
    --    in property info from `mm_properties_base_staging` and
    --    `inf_hotel_str_d` (STR data)

    -- input data:
    --  * mm_property_match: hotel_code. Table contains 95 records
    --    and 94 unique hotel codes
    --  * mm_properties_base_staging: dp_id, name, lat/lon,
    --    city, state, country, market. The `mm_property_match`
    --    table is derived from this table, which has duplicates.
    --  * inf_hotel_str_d: STR data
    --  * note that we join `mm_property_match` against `inf_hotel_str_d`
    --    on (hd_id = str_number), where 'hd_id' was added to the
    --    former table via a cross-join against `inf_hotel_str_d`;
    --    there is no way to directly join tables derived from
    --    `mm_us_properties` to `inf_hotel_str_d`

    -- output:
    --  * using original market-city mapping: 95 records
    --  * using updated market-city mapping: 79 records

    Input tables:
    - mm_property_match
    - mm_properties_base_staging
    - inf_hotel_str_d
    """

    table_name = 'mm_properties'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_property_match'
    t2 = 'mm_properties_base_staging'

    query_string = """
    select pm.hotel_code,
        p.dp_id,
        p.hotel_name,
        p.latitude,
        p.longitude,
        p.city,
        p.state_code,
        p.country_code,
        p.market,
        -- add hotel attributes from STR data
        parse_date('%E4Y%m', cast (hd.open_date as string)) as open_date,
        hd.rooms as number_of_rooms,
        hd.chain_scale,
        hd.floors,
        hd.location,
        case
            when hd.indoor_corridors = 'Y' then 1
            when hd.indoor_corridors = 'N' then 0
            else null
        end
            as has_indoor_corridors,
        case
            when hd.restaurant = 'Y' then 1
            when hd.restaurant = 'N' then 0
            else null
        end
            as has_restaurant,
        case
            when hd.convention = 'Y' then 1
            when hd.convention = 'N' then 0
            else null
        end
            as has_convention,
        case
            when hd.conference = 'Y' then 1
            when hd.conference = 'N' then 0
            else null
        end
            as has_conference,
        case
            when hd.spa = 'Y' then 1
            when hd.spa = 'N' then 0
            else null
        end
            as has_spa,
        if(hd.largest_meeting_space = hd.total_meeting_space and hd.total_meeting_space != 0, 1, 0)
            as has_single_meeting_space,
        hd.largest_meeting_space,
        hd.total_meeting_space,
        case
            when hd.resort = 'Y' then 1
            when hd.resort = 'N' then 0
            else null
        end
            as is_resort,
        case
            when hd.ski = 'Y' then 1
            when hd.ski = 'N' then 0
            else null
        end
            as is_ski_resort,
        case
            when hd.golf = 'Y' then 1
            when hd.golf = 'N' then 0
            else null
        end
            as is_golf_resort,
        case
            when hd.all_suites = 'Y' then 1
            when hd.all_suites = 'N' then 0
            else null
        end
            as is_all_suites,
        case
            when hd.casino = 'Y' then 1
            when hd.casino = 'N' then 0
            else null
        end
            as is_casino,
        hd.price,
        hd.single_low_rate,
        hd.single_high_rate,
        hd.double_low_rate,
        hd.double_high_rate,
        hd.suite_low_rate,
        hd.suite_high_rate
    from `{project_id}.{dataset_id}.{t1}` as pm
    join `{project_id}.{dataset_id}.{t2}` as p
        on (pm.hotel_code = p.hotel_code)
            and (pm.p_dp_id = p.dp_id)
    join `{project_id_datamaster}.{dataset_id_datamaster}.inf_hotel_str_d` as hd
        on (pm.hd_id = hd.str_number)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_markets(dataset_id):
    """
    -- get market-level counts of hotels and rooms based on hotels and
    -- room counts found in the STR data
    --  * inputs: `inf_hotel_str_d` and `mm_market_mapping_hotel`
    --  * `mm_market_mapping_hotel` is the market/city level
    --     table created by joining the market/area-level info
    --     in `mm_market_mapping_hotel_boundary` with the
    --     `location` table and aggregating by market/city
    --  * join market/city combinations against the hotels in
    --    `inf_hotel_str_d` by city/state and aggregate by
    --    market to get counts of hotels & rooms by market

    Input tables:
    - mm_market_city_mapping
    - inf_hotel_str_d
    """

    table_name = 'mm_markets'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_city_mapping'

    query_string = """
    select mm.market,
        count(*) as hotels,
        sum(hd.rooms) as number_of_rooms
    from `{project_id_datamaster}.{dataset_id_datamaster}.inf_hotel_str_d` as hd
    join `{project_id}.{dataset_id}.{t1}` as mm
    on (lower(trim(hd.city)) = mm.city)
        and (lower(trim(hd.state)) = mm.state_code)
    where hd.country = 'United States'
    group by 1
    """.format(
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_hotel_base(dataset_id):
    """
    -- get search & booking events for de-duplicated set of hotels
    -- in target & competitive markets over following date range:
    --  * event_ts > 2014-10-01 & checkout >= 2016-01-01
    --  * purpose of extended range: for Y/Y comparisons in 2017 & 2018
    --  * expensive query: run-times for 3 clients: 135 / 100 / 84 secs
    --  * high run-time due to volume of hotel events + length of period

    -- inputs
    --  * mm_properties: de-duplicated market / hotel / city / state
    --    containing hotels in target & competitive markets;
    --    get market, hotel city, state, country from this; only hotels
    --    with non-null hotel_code and dp_id are retained
    --  * mm_market_mapping_hotel: market / city mapping; get market
    --    from this table, used as backup in case market in
    --    `mm_properties` is missing; only records where one of city,
    --    state, or country or non-null
    --  * ekv_hotel

    -- output: +32.M records for Ocean City, +148M for Tampa

    --  expensive query
    --  * high run-time due to volume of hotel events + length of period

    Input tables:
    - mm_properties
    - mm_market_city_mapping
    - ekv_hotel
    """

    table_name = 'mm_ekv_hotel_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties'
    t2 = 'mm_market_city_mapping'

    query_string = """
    select distinct
        h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        if(h.activity_type in ('click','search'),'search','book') as activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        ifnull(p.market, mm.market) as market,
        ifnull(h.hotel_country, p.country_code) as hotel_country,
        ifnull(h.hotel_state, p.state_code) as hotel_state,
        ifnull(h.hotel_city, p.city) as hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `{project_id_datamaster}.{dataset_id_datamaster}.ekv_hotel` as h
    left join `{project_id}.{dataset_id}.{t1}` as p
        using (dp_id, hotel_code)
    left join `{project_id}.{dataset_id}.{t2}` as mm
        on (h.hotel_city = mm.city)
        and (h.hotel_state = mm.state_code)
        and (h.hotel_country = mm.country_code)
    where h.activity_type in ('book','search','click')
        and h.event_ts < current_timestamp()
        and h.event_ts >= timestamp('2015-01-01 00:00:00')
        and h.checkin_date is not null
        and h.checkout_date >= date(2016,01,01)
        and (
            (p.hotel_code is not null and p.dp_id is not null) or
            (mm.city is not null or
            mm.state_code is not null or
            mm.country_code is not null
            )
        )
    """.format(
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_ekv_hotel_book(dataset_id):
    """
    -- EKV hotel events by market: original market/city mapping
    --  * total records for OC = 32M

    --       MARKET      |   SUM
    -- ------------------+----------
    --  coastal_carolina |  6931341
    --  delaware_beaches |  1001455
    --  myrtle_beach     | 10632585
    --  ocean_city       |  6600402
    --  virginia_beach   |  8346370

    -- EKV hotel events by market: updated market/city mapping
    --  * total records for OC = 43M

    --       MARKET      |  EVENTS
    -- ------------------+----------
    --  Coastal Carolina |  2550782
    --  Delaware Beaches |  3003460
    --  Myrtle Beach     | 11751492
    --  Ocean City       |  6655818
    --  Virginia Beach   |  8414051

    -- de-deduplicated booking events for hotels in target &
    -- competitive markets
    --  * filter events in prior table to activity_type = 'book'
    --    and where event time is prior to checkin date
    --  * select only the earliest event per partition key set
    --    (cookie, dp_id, <hotel info>, <stay dates>)

    -- tampa: 5.4M rows,  7% with null hotel_code

    Input tables:
    - mm_market_city_mapping
    - inf_hotel_str_d
    """

    table_name = 'mm_ekv_hotel_book'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base'

    query_string = """
    with h as (
        select h.*,
            row_number() over (
                partition by h.cookie_id,
                    h.dp_id,
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date,
                    h.checkout_date
                order by h.event_ts desc) as ix
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.activity_type = 'book'
    )
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where h.ix = 1
    and h.checkin_date >= date(h.event_ts)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_hotel_search(dataset_id):
    """
    -- de-deduplicated search events for hotels in target &
    -- competitive markets
    --  * filter events in prior table to activity_type = 'search'
    --    and where event time is prior to checkin date
    --  * select 'non-duplicated' search events per partition
    --    key set (cookie, dp_id, <hotel info>, <stay dates>) by
    --    keeping only the initial search or searches w/o preceding
    --    searches in the past 5 minutes for a given key set
    --  * TODO: check distribution of duplicate search events to
    --    check whether 5-minute threshold should be updated

    Input tables:
    - mm_ekv_hotel_base
    """

    table_name = 'mm_ekv_hotel_search'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_base'

    query_string = """
    with h as (
        select h.*,
            timestamp_diff(h.event_ts, lag(h.event_ts, 1) over (
                partition by h.cookie_id,
                    h.dp_id,
                    h.hotel_code,
                    h.hotel_city,
                    h.hotel_country,
                    h.checkin_date,
                    h.checkout_date
                order by h.event_ts
                ), second
            ) as lag_seconds
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.activity_type = 'search'
    )
    select  h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.checkout_date,
        h.trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from h
    where (h.lag_seconds > 300 or h.lag_seconds is null)
        and h.checkin_date >= date(h.event_ts)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_hotel_staging(dataset_id):
    """
    -- combine the de-duplicated search & book events, with
    -- de-duplication logic based on the partition keys in
    -- prior two queries, (cookie, dp_id, <hotel info>, <stay dates>)

    Input tables:
    - mm_ekv_hotel_search
    - mm_ekv_hotel_book
    """

    table_name = 'mm_ekv_hotel_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_search'
    t2 = 'mm_ekv_hotel_book'

    query_string = """
    select * from `{project_id}.{dataset_id}.{t1}`
    union all
    select * from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_ekv_hotel_dirty(dataset_id):
    """
    -- calculate 'avg_daily_rate_usd' using 'avg_daily_rate' in
    -- hotel events and the currency's exchange_rate_from_usd',
    -- leaving 'avg_daily_rate_usd' null if it's outside range
    -- of ($0, $1,000)
    --  * TODO: can cross check with room rates in STR master data
    --  * TODO: Jian - "need to distinguish between points vs. USD"

    Input tables:
    - mm_ekv_hotel_staging
    - currencies
    """

    table_name = 'mm_ekv_hotel_dirty'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_staging'

    query_string = """
    select  h.*,
        if ( (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) > 0 and
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd) < 1000,
            (1.0 * h.avg_daily_rate / cu.exchange_rate_from_usd),
            null
            ) as avg_daily_rate_usd
    from `{project_id}.{dataset_id}.{t1}` as h
    left join `{project_id_datamaster}.{dataset_id_datamaster}.currencies` as cu
        on (lower(h.currency_type) = lower(cu.currency_code))
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


############################################
# *** begin special processing for IHG *** #
############################################

"""
-- Purpose: to handle incorrect logging of stay dates between
-- 2018-04-12 - 2018-06-26 due to onsite tagging issue. The
-- following queries calculate the average stay duration by
-- market / checkin month / checkin DOW and use these averages
-- to impute the stay duration and checkout date for affected
-- bookings.

-- background
--  * https://adarainc.atlassian.net/browse/SE-3197 ("HG: PDE rule
--    to fix booked checkin date = booked out date"): per comment,
--    the issue affected bookings between 04/12 - 06/26.

-- result of these processing steps: bookings in `mm_ekv_hotel`
-- affected by the incorrect tagging issue on IHG's site will get
-- updated with imputed checkout date and trip_duration
"""


def mm_ihg_alos_map(dataset_id):
    """
    -- get avg length of stay by (market, checkin_month, checkin_dow)
    -- for IHG's bookings (dp_id = 1168)
    --  * retaining events with timestamp and checkin dates before
    --    '2018-04-12', with checkin up to 2 years prior
    --  * these represent bookings logged prior to the incorrect
    --    tagging issue on IHG's site
    --  * break up each (market, checking month, checkin DOW) into
    --    two components: floor & remaining fractional component,
    --    which will be used in the checkout date & length of stay
    --    imputation logic below; this is to ensure that the
    --    length of stay for each affected booking is a whole number
    --    while the average of these whole numbers within each
    --    (market, checkin month, checkin DOW) is close to the average
    --    calculated in this query

    Input tables:
    - mm_ekv_hotel_dirty
    """

    table_name = 'mm_ihg_alos_map'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'

    query_string = """
    with t1 as (
        select h.market,
            extract(month from h.checkin_date) as checkin_month,
            extract(dayofweek from h.checkin_date) as checkin_dow,
            round((1.0 * sum(h.trip_duration) / count(*)), 1) as alos
        from `{project_id}.{dataset_id}.{t1}` as h
        where h.dp_id = 1168
            and h.activity_type = 'book'
            and h.event_ts < timestamp('2018-04-12 00:00:00')
            and timestamp(h.checkin_date) < timestamp('2018-04-12 00:00:00')
            and h.checkin_date >= date_sub(date('2018-04-12'), interval 2 year)
        group by 1,2,3
    ),
    t2 as (
        select *,
            floor(alos) as alos_whole
        from t1
    )
    select *,
        (alos - alos_whole) as alos_decimal
    from t2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ihg_ekv_hotel_base(dataset_id):
    """
    -- get de-duplicated booking events for IHG during period
    -- affected by incorrect logging of stay dates: '2018-04-12'
    -- to '2018-06-26' where checkin date = checkout date
    --  * the 'ix' index field defined here represents the count
    --    of affected bookings in each (market, checkin month/DOW)

    Input tables:
    - mm_ekv_hotel_dirty
    """

    table_name = 'mm_ihg_ekv_hotel_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'

    query_string = """
    select h.*,
        extract(month from h.checkin_date) as checkin_month,
        extract(dayofweek from h.checkin_date) as checkin_dow,
        row_number() over (
            partition by extract(month from h.checkin_date), extract(dayofweek from h.checkin_date), h.market
                order by null) as ix
    from `{project_id}.{dataset_id}.{t1}` as h
    where h.dp_id = 1168
        and h.activity_type = 'book'
        and h.event_ts >= timestamp('2018-04-12 00:00:00')
        and h.event_ts < timestamp('2018-06-27 00:00:00')
        and h.checkin_date = h.checkout_date
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ihg_ekv_hotel_staging(dataset_id):
    """
    -- calculate imputed checkout date and hotel length of stay based
    -- on IHG bookings from pre-2018-04-12 unaffected by tag issue

    --  * from `mm_ihg_ekv_hotel_base`: get the number of IHG bookings
    --    affected by incorrect logging of hotel stay dates per
    --    (checkin month, checkin day of week, market)

    --  * from `mm_ihg_alos_map` and the table created above: for each
    --    (checkin month, checkin DOW, market), define an 'ix_boundary'
    --    calculated as `max_ix * am.alos_decimal`, representing the
    --    number of hotel stay days to re-allocate to the affected
    --    IHG bookings; this is done in order to ensure that the
    --    lengths of stay for individual bookings remain whole numbers,
    --    while their average ends up being equal to the average for
    --    that (market, month, DOW) calculated over unaffected bookings

    --  * impute an updated checkout date and trip_duration for each
    --    affected booking as follows:
    --     - new_checkout_date: checkin_date + (am.alos_whole + 1) if
    --       booking index < ix_boundary, else; else set to
    --       (checkin_date + alos_whole)
    --     - new_trip_duration: new_checkout_date - checkin_date

    Input tables:
    - mm_ihg_ekv_hotel_base
    - mm_ihg_alos_map
    """

    table_name = 'mm_ihg_ekv_hotel_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_base'
    t2 = 'mm_ihg_alos_map'

    query_string = """
    with a as (
        select h.checkin_month,
            h.checkin_dow,
            h.market,
            max(h.ix) as max_ix
        from `{project_id}.{dataset_id}.{t1}` as h
        group by 1,2,3
    ),
    b as (
        select checkin_month,
            checkin_dow,
            market,
            round(a.max_ix * am.alos_decimal) as ix_boundary
        from a
        join `{project_id}.{dataset_id}.{t2}` as am
        using (checkin_month, checkin_dow, market)
    ),
    c as (
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        case
            when h.ix <= b.ix_boundary then DATE_ADD(h.checkin_date, interval (cast (am.alos_whole as INT64) + 1) day )
            else DATE_ADD(h.checkin_date, interval (cast (am.alos_whole as INT64)) day )
        end 
            as new_checkout_date,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate,
        h.checkin_month,
        h.checkin_dow
    from `{project_id}.{dataset_id}.{t1}` as h
    join b
        using (checkin_month, checkin_dow, market)
    join `{project_id}.{dataset_id}.{t2}` as am
        using (checkin_month, checkin_dow, market)
    )
    select *,
        date_diff(new_checkout_date, checkin_date, day) as new_trip_duration
    from c
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_ekv_hotel_mid(dataset_id):
    """
    -- update the affected IHG bookings in `mm_ekv_hotel` with the
    -- corrected bookings with imputed checkout date and trip duration

    Input tables:
    - mm_ekv_hotel_dirty
    - mm_ihg_ekv_hotel_staging
    """

    table_name = 'mm_ekv_hotel_mid'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_dirty'
    t2 = 'mm_ihg_ekv_hotel_staging'

    query_string = """
    select *
    from `{project_id}.{dataset_id}.{t1}`
    where event_id not in (
        select event_id
        from `{project_id}.{dataset_id}.{t2}`
    )
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_ihg_ekv_hotel_insert(dataset_id):
    """
    -- insert corrected IHG booking events into `mm_ekv_hotel`

    Input tables:
    - mm_ihg_ekv_hotel_staging
    """

    table_name = 'mm_ihg_ekv_hotel_insert'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ihg_ekv_hotel_staging'

    query_string = """
    select h.event_id,
        h.cookie_id,
        h.location_id,
        h.dp_id,
        h.vertical,
        h.activity_type,
        h.event_ts,
        h.checkin_date,
        h.new_checkout_date as checkout_date,
        h.new_trip_duration as trip_duration,
        h.market,
        h.hotel_country,
        h.hotel_state,
        h.hotel_city,
        h.hotel_code,
        h.hotel_name,
        h.hotel_brand,
        h.number_of_rooms,
        h.number_of_travelers,
        h.currency_type,
        h.avg_daily_rate
    from `{project_id}.{dataset_id}.{t1}` as h
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_hotel(dataset_id):
    """
    -- insert corrected events into `mm_ekv_hotel`

    Input tables:
    - mm_ekv_hotel_mid
    - mm_ihg_ekv_hotel_insert
    """

    table_name = 'mm_ekv_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel_mid'
    t2 = 'mm_ihg_ekv_hotel_insert'

    query_string = """
    select event_id, cookie_id, location_id, dp_id, vertical,
        activity_type, event_ts, checkin_date, checkout_date, trip_duration, market,
        hotel_country, hotel_state, hotel_city, hotel_code, hotel_name, hotel_brand,
        number_of_rooms, number_of_travelers, currency_type, avg_daily_rate, avg_daily_rate_usd
    from `{project_id}.{dataset_id}.{t1}`
    union all
    select event_id, cookie_id, location_id, dp_id, vertical,
        activity_type, event_ts, checkin_date, checkout_date, trip_duration, market,
        hotel_country, hotel_state, hotel_city, hotel_code, hotel_name, hotel_brand,
        number_of_rooms, number_of_travelers, currency_type, avg_daily_rate, null
    from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


##########################################
# *** end special processing for IHG *** #
##########################################


def mm_market_airport_mapping(dataset_id):
    """
    -- Get airport_codes of target & competitive markets as stored in mm_market_d table

    Input tables:
    - mm_markets_of_interest
    - mm_market_airport_map
    - mm_market_d
    """

    table_name = 'mm_market_airport_mapping'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_markets_of_interest'

    query_string = """
    select  m.name as market,
        a.market_id,
        a.airport_code
    from `{project_id}.{dataset_id_mm_tables}.mm_market_airport_map` as a
    join `{project_id}.{dataset_id_mm_tables}.mm_market_d` as m
        on a.market_id = m.id
    join `{project_id}.{dataset_id}.{t1}` as i
        on a.market_id = i.market_id
    where a.status_id = 1
        and m.status_id = 1
    order by market, airport_code;
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        dataset_id_mm_tables=dataset_id_mm_tables,
        t1=t1
    )

    return query_string, table_ref


def mm_destination_airport(dataset_id):
    """
    -- Get airport information

    Input tables:
    - mm_market_airport_mapping
    - ekv_lookup_airport_code
    """

    table_name = 'mm_destination_airport'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_market_airport_mapping'

    query_string = """
    select ac.airport_code,
        ac.airport_name,
        ac.airport_type,
        ac.city_name,
        ac.dma_code,
        ac.region_code,
        ac.country_code,
        mm.market,
        ac.latitude,
        ac.longitude,
        ac.elevation,
        ac.modification_ts
    from `{project_id_datamaster}.{dataset_id_datamaster}.ekv_lookup_airport_code` as ac
    join `{project_id}.{dataset_id}.{t1}` as mm
        using (airport_code)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_distinct_destination_airport(dataset_id):
    """
    -- get distinct airport codes in target & competitive markets in
    -- order to filter ekv_flight searches & bookings down to flights
    -- headed to these airports

    Input tables:
    - mm_destination_airport
    """

    table_name = 'mm_distinct_destination_airport'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_destination_airport'

    query_string = """
    select distinct dac.airport_code
    from `{project_id}.{dataset_id}.{t1}` as dac
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_flight_base(dataset_id):
    """
    -- searches & bookings for flights headed to airport in target &
    -- competitive markets, with event timestamp >= '2014-10-01' and
    -- return date >= '2016-01-01'
    --  * expensive query: run-times for 3 clients: 152 / 150 / 154 secs
    --  * high run-time due to volume of flight events + length of period
    --  * note: currently excludes flights without an return date

    -- 120 seconds disk IO, 54 secs CPU
    -- output: 35.6M records for Ocean City (search to book: 30:1)
    --         548M for Tampa (search to book: 35:1)

    Input tables:
    - mm_distinct_destination_airport
    - ekv_flight
    """

    table_name = 'mm_ekv_flight_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_distinct_destination_airport'

    query_string = """
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        if(f.activity_type in ('click','search'),'search','book') as activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `{project_id_datamaster}.{dataset_id_datamaster}.ekv_flight` as f
    join `{project_id}.{dataset_id}.{t1}` as da
        on (f.destination_airport = da.airport_code)
    where f.activity_type in ('click','search','book')
        and f.event_ts < current_timestamp
        and f.event_ts >= timestamp('2014-10-01 00:00:00')
        and f.departure_date is not null
        and f.return_date >= date('2016-01-01')
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_flight_book(dataset_id):
    """
    -- de-duplicate bookings based on fields listed as
    -- partition keys in row_number(), limiting to flights
    -- with departure_date on/after booking event timestamp

    Input tables:
    - mm_ekv_flight_base
    """

    table_name = 'mm_ekv_flight_book'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base'

    query_string = """
    with f as (
        select f.*,
            row_number() over(
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts desc
            ) as ix
        from `{project_id}.{dataset_id}.{t1}` as f
        where f.activity_type = 'book'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where f.ix = 1
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_flight_search(dataset_id):
    """
    -- de-duplicate search events based on whether an identical
    -- search was run within the preceding 5 minutes, where
    -- 'identical' is defined based on partition keys below

    -- 380M for Tampa

    Input tables:
    - mm_ekv_flight_base
    """

    table_name = 'mm_ekv_flight_search'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_base'

    query_string = """
    with f as (
        select f.*,
            timestamp_diff(f.event_ts, lag(f.event_ts, 1) over (
                partition by f.cookie_id,
                    f.dp_id,
                    f.departure_date,
                    f.return_date,
                    f.origin_airport,
                    f.destination_airport
                order by f.event_ts
                ), second
            ) as lag_seconds
        from `{project_id}.{dataset_id}.{t1}` as f
        where f.activity_type = 'search'
        and f.departure_date >= date(f.event_ts)
    )
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from f
    where (f.lag_seconds is null or f.lag_seconds > 300)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_ekv_flight_staging(dataset_id):
    """
    -- combine de-duplicated flight bookings + search

    -- outputs:
    --  * Ocean City: 23.25M records (down from original 35.6M)
    --  * Tampa: 393M records (down from original 548M)

    Input tables:
    - mm_ekv_flight_book
    - mm_ekv_flight_search
    """

    table_name = 'mm_ekv_flight_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_book'
    t2 = 'mm_ekv_flight_search'

    query_string = """
    select * from `{project_id}.{dataset_id}.{t1}`
    union all
    select * from `{project_id}.{dataset_id}.{t2}`
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_ekv_flight(dataset_id):
    """
    -- join de-duplicated flight bookings + searches against
    -- `mm_destination_airport` containing airport/market
    -- combinations for target & competitive markets
    --  * `mm_destination_airport` should contain only
    --    unique (market, airport) combinations as a result
    --    of upstream 'select distinct'
    --  * if an airport is associated with multiple markets
    --    in `mm_destination_airport`, then this join will
    ---   result in a greater number of records

    -- output for Ocean City: no changes in record counts
    -- from using updated market-airport mapping

    Input tables:
    - mm_ekv_flight_staging
    - mm_destination_airport
    """

    table_name = 'mm_ekv_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight_staging'
    t2 = 'mm_destination_airport'

    query_string = """
    select f.event_id,
        f.cookie_id,
        f.dp_id,
        f.vertical,
        f.activity_type,
        f.event_ts,
        f.departure_date,
        f.return_date,
        f.origin_airport,
        dac.market,
        f.destination_airport,
        f.air_carrier,
        f.cabin_class,
        f.cabin_class_group,
        f.currency_type,
        f.number_of_travelers,
        f.trip_duration,
        f.booked_date,
        f.airfare,
        f.location_id
    from `{project_id}.{dataset_id}.{t1}` as f
    join `{project_id}.{dataset_id}.{t2}` as dac
        on (f.destination_airport = dac.airport_code)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


##############################################
# *** Cumulative & Total Bookings Target *** #
##############################################


def mm_booking_dataset_base_t1(dataset_id):
    """
    Input tables:
    - mm_ekv_hotel
    - mm_properties
    - inf_day_d
    """

    table_name = 'mm_booking_dataset_base_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'
    t2 = 'mm_properties'

    query_string = """
    select hotel_code,
        dp_id,
        d.cal_date as stay_date,
        date(event_ts) as statistic_date,
        count(1) as bookings
    from `{project_id}.{dataset_id}.{t1}` as h
    join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id)
    join `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` as d
        on (d.cal_date between h.checkin_date and date_sub(h.checkout_date, interval 1 day))
    where d.cal_date >= '2016-01-01'
        and event_ts >= timestamp(date_sub(d.cal_date, interval 1 year))
        and activity_type = 'book'
    group by 1,2,3,4
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_base_t2(dataset_id):
    """
    Input tables:
    - mm_booking_dataset_base_t1
    - inf_day_d
    """

    table_name = 'mm_booking_dataset_base_t2'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t1'

    query_string = """
    with T1 as (
        select hotel_code,
            dp_id,
            stay_date,
            sum(bookings) as bookings,
            min(statistic_date) as min_book_date,
            max(statistic_date) as max_book_date
        from `{project_id}.{dataset_id}.{t1}`
        group by 1,2,3
    ),
    T2 as(
        select hotel_code,
            dp_id,
            min(stay_date) as min_stay_date,
            max(stay_date) as max_stay_date
        from T1
        group by 1,2
    ),
    T3 as (
        select hotel_code,
            dp_id,
            d.cal_date as stay_date
        from T2
        cross join `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` as d
        where d.cal_date between min_stay_date and max_stay_date
    )
    select hotel_code,
        dp_id,
        stay_date,
        ifnull(bookings,0) as bookings
    from T3
    left outer join T1
        using (hotel_code, dp_id, stay_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_base_t3(dataset_id):
    """
    --Resource: Hcpu 0.009 Cpu 1.965/2.560 Dread 0.008/0.030 Fpga 0.001 Dwrite 0.526/0.545

    Input tables:
    - mm_booking_dataset_base_t2
    - inf_day_d
    """

    table_name = 'mm_booking_dataset_base_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t2'

    query_string = """
    select hotel_code,
        dp_id,
        stay_date,
        d.cal_date as statistic_date,
        bookings as total_bookings_target
    from `{project_id}.{dataset_id}.{t1}` as t
    join `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` as d
        on (d.cal_date between date_sub(t.stay_date, interval 1 year) and t.stay_date and d.cal_date <= current_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_base(dataset_id):
    """
    -- Resource: Hcpu 0.020 Cpu 5.807/7.320 Dread 0.143/0.149 Fpga 0.326/0.341 Dwrite 0.502/0.520
    -- 112478526 rows affected (Tampa)
    -- TODO: should remove the case ... logic for total_booking_target

    Input tables:
    - mm_booking_dataset_base_t3
    - mm_booking_dataset_base_t1
    """

    table_name = 'mm_booking_dataset_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base_t3'
    t2 = 'mm_booking_dataset_base_t1'
    t3 = 'mm_properties'

    query_string = """
    with T1 as (
        select hotel_code,
        dp_id,
        stay_date,
        statistic_date,
        ifnull(bookings,0) as bookings_today,
        sum(ifnull(bookings,0)) over (
            partition by hotel_code, dp_id, stay_date
            order by statistic_date) as cumulative_bookings_target,
        total_bookings_target
        from `{project_id}.{dataset_id}.{t1}`
        left outer join `{project_id}.{dataset_id}.{t2}`
            using (hotel_code, dp_id, stay_date, statistic_date)
    )
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        least(T1.cumulative_bookings_target, p.number_of_rooms) as cumulative_bookings_target,
        case
            when stay_date >= current_date then null
            else least(T1.total_bookings_target, p.number_of_rooms)
            end total_bookings_target
    from T1
    join `{project_id}.{dataset_id}.{t3}` as p
        using (hotel_code, dp_id)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


####################################################
# *** Bookings, ADR, ALOS, ANOT & S/B Duration *** #
####################################################


"""
-- https://carto.com/blog/center-of-points/
--  * for each market, get center of hotel coordinates (lat/lon)
--    but when calculating avg of multiple points' longitudes, take
--    into account how longitudes are represent below/above the 
--    180-degree line
--  * latitudes are averaged as-is; not sure if there are edge cases
--    for which this wouldn't work; 
--  * see see https://gis.stackexchange.com/questions/7555 for 
--    approach to averaging latitudes as well
"""


def mm_metric_dataset_base_metric_calculation_base_staging(dataset_id):
    """
    -- aggregate ekv_hotel to the following level:
    -- (market, metrocode, stay_month, statistic_month, activity_type

    --  * this resulting table tells us the hotel search & booking behavior
    --    of travelers going to $market from their origin $metrocode

    --  * the 'market' field contains one of the target or competitor
    --    markets (e.g. 'ocean_city', 'coastal_carolina', etc.)

    --  * but the 'metrocode' field contains the metrocode of travelers'
    --    locations while searching for/booking hotels, _not_ the location
    --    of the hotels; hence there are 210 unique metrocodes represented
    --    in this resulting table. The metrocode represents the metrocodes
    --    mapped to location IDs found in 'mm_ekv_hotel', which represent
    --    the locations of the travelers as they search/book onsite

    --  * calculate numerator and denominator of: ADR, avg length
    --    of stay, avg numberof travelers

    --  * contains only the markets and metrocodes represented in
    --    mm_ekv_hotel, i.e. those in MM's target & competitor markets

    Input tables:
    - mm_ekv_hotel
    - location
    - metrocodes
    """

    table_name = 'mm_metric_dataset_base_metric_calculation_base_staging'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'

    query_string = """
    select h.market,
        l.metrocode,
        date_trunc(h.checkin_date, month) as stay_month,
        date_trunc(date(h.event_ts), month) as statistic_month,
        h.activity_type,
        count(*) as events,
        sum(h.avg_daily_rate_usd * h.trip_duration) as adr_numerator,
        sum(if(h.avg_daily_rate_usd is not null and
            h.trip_duration is not null,
            h.trip_duration,
            null)) as adr_denominator,
        sum(h.trip_duration) as alos_numerator,
        sum(if(h.trip_duration is not null, 1, 0)) as alos_denominator,
        sum(h.number_of_travelers) as anot_numerator,
        sum(if(h.number_of_travelers is not null, 1, 0)) as anot_denominator
    from `{project_id}.{dataset_id}.{t1}` as h
    join `{project_id_datamaster}.{dataset_id_datamaster}.location` as l
        on (h.location_id = l.id)
    join `{project_id_datamaster}.{dataset_id_datamaster}.metrocodes` as m
        on (l.metrocode = m.id)
    group by 1,2,3,4,5
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_market_coordinates(dataset_id):
    """
    -- get avg_longitude and avg_latitude of hotels in each of the
    -- target and competitive markets, based on approach in blog
    -- post linked above
    --  * first get raw naive avg longitude & latitude by market
    --  * then apply transformations to convert raw longitude into
    --    more accurate avg longitude
    --  * avg latitude is used as-is: not sure if there are edge
    --    cases where this could present problems;
    --  * see https://gis.stackexchange.com/questions/7555 for
    --    approach to averaging latitudes

    Input tables:
    - mm_properties
    """

    table_name = 'mm_metric_market_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_properties'

    query_string = """
    select  m.market,
        (180 * atan2(m.zeta, m.xi) / ACOS(-1)) as avg_longitude,
        m.avg_latitude
    from (
        select p.market,
            avg(sin(ACOS(-1) * p.longitude / 180)) as zeta,
            avg(cos(ACOS(-1) * p.longitude / 180)) as xi,
            avg(p.longitude) as avg_longitude,
            avg(p.latitude) as avg_latitude
        from `{project_id}.{dataset_id}.{t1}` as p
        group by 1
    ) as m
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_coordinates(dataset_id):
    """
    -- calculate cross-market avg latitude and longitude using same
    -- approach in previous query; results in a single central (lat/lon)
    --  * represents central lat/lon across the target & comp markets
    --  * how is this cross-market average lat/lon used?
    --  * does this work for edge cases where the markets are far-removed
    --    from each other?

    Input tables:
    - mm_metric_market_coordinates
    """

    table_name = 'mm_metric_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates'

    query_string = """
    select (180 * atan2(c.zeta, c.xi) / ACOS(-1)) as avg_longitude,
        c.avg_latitude
    from (
        select avg(sin(ACOS(-1) * mc.avg_longitude / 180)) as zeta,
            avg(cos(ACOS(-1) * mc.avg_longitude / 180)) as xi,
            avg(mc.avg_longitude) as avg_longitude,
            avg(mc.avg_latitude) as avg_latitude
        from `{project_id}.{dataset_id}.{t1}` as mc
    ) as c
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_metro_coordinates(dataset_id):
    """
    -- get lat/lon of each metrocode directly from `metrocodes` table

    Input tables:
    - metrocodes
    """

    table_name = 'mm_metric_metro_coordinates'
    table_ref = client.dataset(dataset_id).table(table_name)

    query_string = """
    select  id as metrocode,
        longitude as avg_longitude,
        latitude as avg_latitude
    from `{project_id_datamaster}.{dataset_id_datamaster}.metrocodes`
    """.format(
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster
    )

    return query_string, table_ref


def mm_metric_metro_market_distance(dataset_id):
    """
    -- cross-join MM markets x metrocodes and calculate distance between
    -- each market's central lat/lon and each metrocode's central lat/lon

    -- output: 1050 records = 210 x 5 due to market x metrocode cross-join;
    -- the number of markets will depend on the set of target & comp markets
    -- defined upfront during data setup phase

    Input tables:
    - mm_metric_market_coordinates
    - mm_metric_metro_coordinates
    """

    table_name = 'mm_metric_metro_market_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_market_coordinates'
    t2 = 'mm_metric_metro_coordinates'

    query_string = """
    select m.market,
        mc.metrocode,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((m.avg_latitude - mc.avg_latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((mc.avg_latitude) * ACOS(-1) /180)) *
                    (cos((m.avg_latitude) * ACOS(-1) /180)) *
                    pow( (sin(((m.avg_longitude - mc.avg_longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
    from `{project_id}.{dataset_id}.{t1}` as m,
        `{project_id}.{dataset_id}.{t2}` as mc
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_metric_metro_distance(dataset_id):
    """
    -- cross join the metrocodes' central lat/lon against the overall
    -- cross-market central lat/lon to calculate the distance between the
    -- each MM market and each of the 210 US metrocodes

    -- inputs
    --  * mm_metric_coordinates: cross-market avg latitude and longitude
    --    based on the per-market central lat/lon calculated across each
    --    market's hotels (just 1 record)
    --  * mm_metric_metro_coordinates: average lat/lon across cities in
    --    each of the 210 US metrocodes in `opinmind_prod..metrocodes`

    -- output: ~210 records, one per metrocode, each containing distance
    -- between metrocode center and center of all target & comp MM markets

    Input tables:
    - mm_metric_coordinates
    - mm_metric_metro_coordinates
    """

    table_name = 'mm_metric_metro_distance'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_coordinates'
    t2 = 'mm_metric_metro_coordinates'

    query_string = """
    select mc.metrocode,
        (2 * 3961 *
            asin(
                sqrt(
                    pow( (sin(((m.avg_latitude - mc.avg_latitude) / 2) * ACOS(-1) /180 )), 2) +
                    (cos((mc.avg_latitude) * ACOS(-1) /180)) *
                    (cos((m.avg_latitude) * ACOS(-1) /180)) *
                    pow( (sin(((m.avg_longitude - mc.avg_longitude) / 2) * ACOS(-1) /180 )), 2)
                )
            )
        ) as distance_miles
    from `{project_id}.{dataset_id}.{t1}` as m,
        `{project_id}.{dataset_id}.{t2}` as mc
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_metric_base_metrocode_map(dataset_id):
    """
    -- assign each origin metrocode to 'metrocode_group' based on its share
    -- of overall bookings to the target & competitor markets;
    --  * leave higher-share origin metrocodes in their own group, i.e.
    --    set metrocode_group = metrocode
    --  * but cluster lower-share origin metrocodes into one of five
    --    groups {{-1, -2, -3, -4, -5}} based on their distance to the center
    --    of the target & competitor markets; group = -1 contains origin
    --    metrocodes closer to the central lat/lon of the target/comp markets,
    --    while group -5 contains origin metrocodes farthest away;
    --  * note: the 'center' of the target & competitor markets makes sense
    --    only if they are relatively close to one another geographically;
    --    should be the case most of the time, but could be edge cases;
    --  * the metrocode_group defined here is also used downstream in origin
    --    market reporting queries

    -- in downstream aggregations, we'll aggregate by metrocode_group instead
    -- of metrocode in order to group together origin metrocodes responsible
    -- for only a small share of bookings in the target/comp markets; otherwise
    -- we'll end up with (market, metrocode, stay month, stat month) combinations
    -- with very few bookings/searches

    -- inputs:
    --  * mm_metric_dataset_base_metric_calculation_base_staging: aggregated
    --    to (market, metrocode, stay_month, statistic_month, activity_type;
    --    tells us hotel search & booking behavior of travelers going to
    --    $market from their origin $metrocode
    --  * mm_metric_metro_distance: contains the 210 US metrocdes, along with
    --    the distance between center of metrocode and center of the target &
    --    comp MM markets

    -- output: 210 records, one per metrocode, only ~12% of which are higher-share
    -- (assigned to their own metrocode_group) in the case of Ocean City, the rest
    -- of which are clustered into one of the low booking share groups labeled as
    -- {{-1, -2, -3, -4, -5}}

    Input tables:
    - mm_metric_dataset_base_metric_calculation_base_staging
    - mm_metric_metro_distance
    """

    table_name = 'mm_metric_base_metrocode_map'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging'
    t2 = 'mm_metric_metro_distance'

    query_string = """
    with a1 as (
        select  mc.metrocode,
            sum(mc.events) as metrocode_bookings
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'book'
        group by 1
        ),
    a2 as (
        select *,
            sum(metrocode_bookings) over() as overall_bookings
        from a1
    ),
    a as (
        select *,
            (1.0 * metrocode_bookings / overall_bookings) as metrocode_booking_share
        from a2
    ),
    b as (
        select a.metrocode,
            case
                when a.metrocode_booking_share >= 0.01 then a.metrocode
                else -1
            end
                as metrocode_group,
            a.metrocode_bookings as bookings,
            a.metrocode_booking_share as booking_share
        from a
    ),
    c as (
        select  b.*,
            md.distance_miles,
            row_number() over (order by md.distance_miles) as distance_rank
        from `{project_id}.{dataset_id}.{t2}` as md
        join b
        using (metrocode)
        where b.metrocode_group = -1
    ),
    p as (
        select *,
            percentile_cont(c.distance_miles, 0.2) OVER() as p_20,
            percentile_cont(c.distance_miles, 0.4) OVER() as p_40,
            percentile_cont(c.distance_miles, 0.6) OVER() as p_60,
            percentile_cont(c.distance_miles, 0.8) OVER() as p_80
        from c
    )
    select b.metrocode,
        case
            when b.metrocode_group = -1 and p.distance_miles <  p.p_20 then -1
            when b.metrocode_group = -1 and p.distance_miles >= p.p_20 and p.distance_miles < p.p_40 then -2
            when b.metrocode_group = -1 and p.distance_miles >= p.p_40 and p.distance_miles < p.p_60 then -3
            when b.metrocode_group = -1 and p.distance_miles >= p.p_60 and p.distance_miles < p.p_80 then -4
            when b.metrocode_group = -1 and p.distance_miles >= p.p_80 then -5
            else b.metrocode
        end
            as metrocode_group,
        p.distance_miles,
        b.bookings,
        b.booking_share
    from b
    left join p
        using (metrocode)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_metric_dataset_base_metric_calculation_base(dataset_id):
    """
    -- calculate inputs to ADR, avg length of stay, avg number of travelers
    -- aggregated to the level of: {{market (dest.), metrocode group (origin),
    -- stay_month, statistic_month}}

    -- aggregate by metrocode_group instead of metrocode in order to group
    -- together origin metrocodes responsible for only a small share of
    -- bookings in the target/comp markets; otherwise we'll end up with
    -- (market, metrocode, stay month, stat month) combinations with very
    -- few bookings/searches, resulting in noisy estimates of ADR,
    -- avg travelers, avg length of stay

    -- inputs:
    --  * mm_metric_dataset_base_metric_calculation_base_staging: 282,491
    --    records aggregated by (market, metrocode group, stay_month,
    --    statistic_month, activity_type). Tells us the following:
    --    1) in what month did they search/book a hotel in target/comp markets?
    --    2) in what month would they be staying in the target/comp markets?
    --    3) where would they be coming from (origin metrocode group)?
    --    4) in which market (target or competitive) would they be staying?

    --  * mm_metric_base_metrocode_map: 210 records, one per metrocode, with
    --    each origin metrocode assigned to a metrocode_group based on:
    --    1) its share of overall bookings to the target & competitor markets,
    --    2) its distance to the center of the target/comp markets

    --  * only records where statistic_month is within 15 months or 12 months
    --    the stay_month, and where stay_month >= '2016-01-01', are included

    Input tables:
    - mm_metric_dataset_base_metric_calculation_base_staging
    - mm_metric_base_metrocode_map
    """

    table_name = 'mm_metric_dataset_base_metric_calculation_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base_staging'
    t2 = 'mm_metric_base_metrocode_map'

    query_string = """
    select  mc.market,
        mm.metrocode_group,
        mc.stay_month,
        mc.statistic_month,
        mc.activity_type,
        sum(mc.events) as events,
        sum(mc.adr_numerator) as adr_numerator,
        sum(mc.adr_denominator) as adr_denominator,
        sum(mc.alos_numerator) as alos_numerator,
        sum(mc.alos_denominator) as alos_denominator,
        sum(mc.anot_numerator) as anot_numerator,
        sum(mc.anot_denominator) as anot_denominator
    from `{project_id}.{dataset_id}.{t1}` as mc
    join `{project_id}.{dataset_id}.{t2}` as mm
    using (metrocode)
    where case
        when mc.stay_month = date('2016-01-01')
            then mc.statistic_month >= date_sub(mc.stay_month, interval 15 month)
            else mc.statistic_month >= date_sub(mc.stay_month, interval 1 year)
        end
        and mc.statistic_month <= mc.stay_month
        and mc.stay_month >= date('2016-01-01')
    group by 1,2,3,4,5
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_metric_dataset_base_metric_calculation_point(dataset_id):
    """
    -- flatten (market, metrocode_group, stay month, stat month) records in prior table
    -- to create separate sets of fields containing search and booking metrics (i.e.
    -- removing 'activity_type' as an aggregation key)

    Input tables:
    - mm_metric_dataset_base_metric_calculation_base
    """

    table_name = 'mm_metric_dataset_base_metric_calculation_point'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_base'

    query_string = """
    with s as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,

            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'search'
    ),
    b as (
        select mc.market,
            mc.metrocode_group,
            mc.stay_month,
            mc.statistic_month,

            mc.events,
            mc.adr_numerator,
            mc.adr_denominator,
            mc.alos_numerator,
            mc.alos_denominator,
            mc.anot_numerator,
            mc.anot_denominator
        from `{project_id}.{dataset_id}.{t1}` as mc
        where mc.activity_type = 'book'
    )
    select market,
        metrocode_group,
        stay_month,
        statistic_month,

        s.events as search_events,

        s.adr_numerator as search_adr_numerator,
        s.adr_denominator as search_adr_denominator,
        (1.0 * s.adr_numerator / s.adr_denominator) as search_adr,

        s.alos_numerator as search_alos_numerator,
        s.alos_denominator as search_alos_denominator,
        (1.0 * s.alos_numerator / s.alos_denominator) as search_alos,

        s.anot_numerator as search_anot_numerator,
        s.anot_denominator as search_anot_denominator,
        (1.0 * s.anot_numerator / s.anot_denominator) as search_anot,

        b.events as book_events,
        b.adr_numerator as book_adr_numerator,
        b.adr_denominator as book_adr_denominator,
        (1.0 * b.adr_numerator / b.adr_denominator) as book_adr,

        b.alos_numerator as book_alos_numerator,
        b.alos_denominator as book_alos_denominator,
        (1.0 * b.alos_numerator / b.alos_denominator) as book_alos,

        b.anot_numerator as book_anot_numerator,
        b.anot_denominator as book_anot_denominator,
        (1.0 * b.anot_numerator / b.anot_denominator) as book_anot
    from s
    full outer join b
    using (market, metrocode_group, stay_month, statistic_month)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_dataset_base_metric_calculation_cumulative(dataset_id):
    """
    -- calculate cumulative counts and averages for the same subset of features
    -- as in the past several queries: events, and {{ADR, ALOS, ANOT}} for searches
    -- & bookings

    -- to calculate cumulative metrics: self-join the table created above,
    -- `mm_metric_dataset_base_metric_calculation_point` with fuzzy join on
    -- statistic_month (b.statistic_month <= a.statistic_month) to associate each
    -- record in the LHS table with all records for the same (market, metrocode_group,
    -- stay_month, statistic_month) from the RHS table for the same and earlier
    -- statistic months

    -- then aggregate back up to the same (market, metrocode_group, stay_month,
    -- statistic_month) level, resulting in an output table with same row count
    -- as the input table

    -- output: ~50K records for Ocean City, with run-time < 1-2 seconds
    --  * the self-join doesn't present an issue for a modest-sized table like this
    --  * for the 3 existing clients, this query took only 1-2 secs
    --  * can compare run-time to window function approach later to handle larger
    --    target/competitive markets

    Input tables:
    - mm_metric_dataset_base_metric_calculation_point
    """

    table_name = 'mm_metric_dataset_base_metric_calculation_cumulative'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'

    query_string = """
    select a.market,
        a.metrocode_group,
        a.stay_month,
        a.statistic_month,
        sum(b.search_events) as search_events,

        (1.0 * sum(b.search_adr_numerator) / sum(b.search_adr_denominator)) as search_adr,
        (1.0 * sum(b.search_alos_numerator) / sum(b.search_alos_denominator)) as search_alos,
        (1.0 * sum(b.search_anot_numerator) / sum(b.search_anot_denominator)) as search_anot,

        sum(b.book_events) as book_events,

        (1.0 * sum(b.book_adr_numerator) / sum(b.book_adr_denominator)) as book_adr,
        (1.0 * sum(b.book_alos_numerator) / sum(b.book_alos_denominator)) as book_alos,
        (1.0 * sum(b.book_anot_numerator) / sum(b.book_anot_denominator)) as book_anot
    from `{project_id}.{dataset_id}.{t1}` as a
    join `{project_id}.{dataset_id}.{t1}` as b
    on (a.market = b.market)
        and (a.metrocode_group = b.metrocode_group)
        and (a.stay_month = b.stay_month)
        and (b.statistic_month <= a.statistic_month)
    group by 1,2,3,4
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_dataset_base_target(dataset_id):
    """
    -- aggregate bookings, ADR, ALOS, ANOT across statistic_month to level of
    -- (market, metrocode_group, stay_month)

    -- inputs:
    --  * mm_metric_dataset_base_metric_calculation_point: search & booking
    --    metrics at (market, metrocode_group, stay month, stat month) level

    -- output: ~4K records, among which are 5 markets (target & comp) and 27
    - - metrocode groups (22 of which contain a single metrocode, the other 5
    -- of consist of multiple low-share metrocodes aggregated into groups)

    Input tables:
    - mm_metric_dataset_base_metric_calculation_point
    """

    table_name = 'mm_metric_dataset_base_target'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'

    query_string = """
    select mcp.market,
        mcp.metrocode_group,
        mcp.stay_month,
        sum(mcp.book_events) as bookings_target,
        round(1.0 * sum(mcp.book_adr_numerator) / sum(mcp.book_adr_denominator)) as adr_target,
        round((1.0 * sum(mcp.book_alos_numerator) / sum(mcp.book_alos_denominator)),2) as alos_target,
        round((1.0 * sum(mcp.book_anot_numerator) / sum(mcp.book_anot_denominator)),2) as anot_target,
        round(1.0 * sum(mcp.search_events) / sum(mcp.book_events)) as stob_target
    from `{project_id}.{dataset_id}.{t1}` as mcp
    where mcp.stay_month < date_trunc(current_date(), month)
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_metric_dataset_base_feature(dataset_id):
    """
    -- combine 'point search', 'point bookings', 'cumulative search', and
    -- 'cumulative bookings' features, all at the level of (market,
    -- metrocode_group, stay_month, stat_month)
    --  * metrics: events, ADR, avg length of stay, avg number of travelers

    -- inputs:
    --  * mm_metric_dataset_base_metric_calculation_point: search & booking
    --    metrics at (market, metrocode_group, stay month, stat month) level
    --    ("ps_*" and "pb_*" in output table);
    --  * mm_metric_dataset_base_metric_calculation_cumulative: cumulative
    --    search and booking metrics ("cs_*" and "cb_"*) in output table

    Input tables:
    - mm_metric_dataset_base_metric_calculation_point
    - mm_metric_dataset_base_metric_calculation_cumulative
    """

    table_name = 'mm_metric_dataset_base_feature'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_metric_calculation_point'
    t2 = 'mm_metric_dataset_base_metric_calculation_cumulative'

    query_string = """
    select market,
        metrocode_group,
        stay_month,
        statistic_month,
        mcp.search_events as ps_events,
        round(mcp.search_adr) as ps_adr,
        round(mcp.search_alos, 2) as ps_alos,
        round(mcp.search_anot, 2) as ps_anot,

        mcp.book_events as pb_events,
        round(mcp.book_adr) as pb_adr,
        round(mcp.book_alos, 2) as pb_alos,
        round(mcp.book_anot, 2) as pb_anot,

        mcc.search_events as cs_events,
        round(mcc.search_adr) as cs_adr,
        round(mcc.search_alos, 2) as cs_alos,
        round(mcc.search_anot, 2) as cs_anot,

        mcc.book_events as cb_events,
        round(mcc.book_adr) as cb_adr,
        round(mcc.book_alos, 2) as cb_alos,
        round(mcc.book_anot, 2) as cb_anot
    from `{project_id}.{dataset_id}.{t1}` as mcp
    full outer join `{project_id}.{dataset_id}.{t2}` as mcc
    using (market, metrocode_group, stay_month, statistic_month)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_metric_dataset_base(dataset_id):
    """
    -- Combine features at the (market, metrocode_group, stay_month) level
    -- with point & cumulative features at the (market, metrocode_group,
    -- stay_month, stat month) level;
    --  * resulting table is used to create `cg_mm_metric_dataset` table,
    --    which is a separate table from the input table passed to the
    --    model training

    -- inputs:
    --  * mm_metric_dataset_base_feature: contains the 'point search',
    --   'point bookings', 'cumulative search', and 'cumulative bookings'
    --   features, all at the level of (market, metrocode_group, stay_month,
    --   stat_month); contains 53,122 records for Ocean City
    --  * mm_metric_dataset_base_target: bookings, ADR, ALOS, ANOT aggregated
    --    across statistic_month to (market, metrocode_group, stay_month)
    --    level; contains 4,304 records, among which are 5 markets (target &
    --    comp) and 27 metrocode groups (22 of which contain a single
    --    metrocode, the other 5 consisting of multiple low-share metrocodes
    --    aggregated into groups)

    -- output: at level of (market, metrocode_group, stay_month, statistic_month)

    Input tables:
    - mm_metric_dataset_base_feature
    - mm_metric_dataset_base_target
    """

    table_name = 'mm_metric_dataset_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_metric_dataset_base_feature'
    t2 = 'mm_metric_dataset_base_target'

    query_string = """
    select market,
        metrocode_group,
        stay_month,
        dbf.statistic_month,
        cast((date_diff(stay_month, dbf.statistic_month, day) / 30) as INT64) as stay_window,
        dbt.bookings_target,
        dbt.adr_target,
        dbt.alos_target,
        dbt.anot_target,
        dbt.stob_target,

        dbf.ps_events,
        dbf.ps_adr,
        dbf.ps_alos,
        dbf.ps_anot,

        dbf.pb_events,
        dbf.pb_adr,
        dbf.pb_alos,
        dbf.pb_anot,

        dbf.cs_events,
        dbf.cs_adr,
        dbf.cs_alos,
        dbf.cs_anot,

        dbf.cb_events,
        dbf.cb_adr,
        dbf.cb_alos,
        dbf.cb_anot
    from `{project_id}.{dataset_id}.{t1}` as dbf
    full outer join `{project_id}.{dataset_id}.{t2}` as dbt
    using (market, metrocode_group, stay_month)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


################################################
# *** inputs to hotel-event-based features *** #
################################################


def mm_booking_hotel_summary_base(dataset_id):
    """
    -- get counts of all hotel events aggregated by the following
    -- keys: (hotel_code, dp_id, market, activity_type, stay date
    -- range, event_date)

    -- inputs: mm_ekv_hotel: 21.1M records for Ocean City

    -- output: 9.2M records for Ocean City

    Input tables:
    - mm_ekv_hotel
    """

    table_name = 'mm_booking_hotel_summary_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_hotel'

    query_string = """
    select h.hotel_code,
        h.dp_id,
        h.market,
        h.activity_type,
        h.checkin_date,
        h.checkout_date,
        date(h.event_ts) as event_date,
        count(*) as events
    from `{project_id}.{dataset_id}.{t1}` as h
    group by 1,2,3,4,5,6,7
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_hotel_summary(dataset_id):
    """
    -- expand each search/booking event in prior table by cross-joining
    -- against `inf_day_d` and keeping the calendar dates falling between
    -- the checkin & checkout dates for each hotel search event, i.e.
    -- `(d.cal_date between hs.checkin_date and hs.checkout_date - 1)`;
    -- then aggregate result to the same keys as before but replacing
    -- (checkin_date, checkout_date) with (stay_date)

    -- inputs:
    --  * cg_mm_booking_hotel_summary_base: aggregated by keys
    --    (hotel_code, dp_id, market, activity_type, stay date
    --    range, event_date); contains 9.2M records for Ocean City
    --  * inf_day_d: contains calendar dates

    -- output: 12.2M records for Ocean City at the level of
    -- (hotel_code, dp_id, market, activity_type, stay_date, event_date)

    Input tables:
    - mm_booking_hotel_summary_base
    - inf_day_d
    """

    table_name = 'mm_booking_hotel_summary'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary_base'

    query_string = """
    select hs.hotel_code,
        hs.dp_id,
        hs.market,
        hs.activity_type,
        d.cal_date as stay_date,
        hs.event_date,
        sum(hs.events) as events
    from `{project_id}.{dataset_id}.{t1}` as hs
    join `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` as d
    on (d.cal_date between hs.checkin_date and date_sub(hs.checkout_date, interval 1 day))
    group by 1,2,3,4,5,6
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


#############################################################
# *** prior-14 day searches over various hotel segments *** #
#############################################################


def mm_booking_dataset_hotel_hc_search_base(dataset_id):
    """
    -- for a given (hotel_code, dp_id, market, stay_date, statistic_date),
    -- count the prior-14-day searches for stays in that hotel through a
    -- given dp_id where the checkin/checkout date range overlaps with
    -- the stay date

    -- inputs:
    --  * mm_booking_dataset_base: contains bookings target and cumulative
    --    bookings target by (hotel_code, dp_id, market, stay_date, book_date);
    --    contains 39.7M records for OC
    --  * mm_booking_hotel_summary: contains counts of hotel events at the
    --    (hotel_code, dp_id, market, activity_type, stay date, event_date)
    --    level; contains 12,162,735 records for OC, 10,658,614 of which have
    --    activity_type = 'search'
    --  * note: the query joins on (hotel_code, dp_id, stay_date), does a
    --    fuzzy match on stay_date <> statistic_date, but no matching at all
    --    market, which is found in both tables; this is because joining on
    --    hotel_code implicitly joins on market as well;

    -- outputs: 12,995,912 records for OC

    -- moderately expensive query:
    --  * run-times across 3 existing clients = 3 / 3 / 3 seconds
    --  * run-times observed for Ocean City from additional tests:
    --    0m17.983s, 0m15.771s, 0m15.924s
    --  * run-times of alternate query with aggregation of the RHS table
    --    `mm_booking_hotel_summary` in a prior step: no run-time reduction
    --    because there was only a slight reduction in cardinality of LHS
    --    table from 13M to 10.7M records

    Input tables:
    - mm_booking_dataset_base
    - mm_booking_hotel_summary
    """

    table_name = 'mm_booking_dataset_hotel_hc_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_hotel_summary'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
    on (b.stay_date = hs.stay_date)
        and (hs.event_date >= date_sub(b.statistic_date, interval 14 day))
        and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'search'
        and b.hotel_code = hs.hotel_code
        and b.dp_id = hs.dp_id
    group by 1,2,3,4,5
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to prior query, but counting the prior-14-day searches
-- for the hotel's entire market (not just for that hotel_code); 

-- for a given (hotel_code, dp_id, market, stay_date, statistic_date), 
-- count the prior-14-day searches for stays in the hotel's market 
-- where the checkin/checkout date range overlaps with stay date; 

-- inputs:
--  * mm_booking_dataset_base: contains bookings target and cumulative 
--    bookings target by (hotel_code, dp_id, market, stay_date, stat_date);
--    contains 39.7M records for OC
--  * mm_booking_hotel_summary: contains counts of hotel events at the
--    (hotel_code, dp_id, market, activity_type, stay date, event_date) 
--    level; contains 12.2M records for OC, 10.7M which are searches

-- output: 38,234,140 records at level of (hotel_code, dp_id, market, 
-- stay_date, stat_date) for OC

-- expensive query: run-times of original query across 3 clients = 
-- 59 / 30 / 26 seconds
--  * alternative #1: first aggregate and then join. This alternative
--    was implemented below and reduced run-time by > 50% based on 
--    comparisons across multiple runs, e.g. from 45 secs to 20 secs.
--  * alternative #2: try using window functions with window defined as:
--    `range between interval '14 days' preceding and interval '1 day'
--    preceding`. Unfortunately was not able to get query results to
--    match that of original.

-- updated to aggregate search counts from `mm_booking_hotel_summary` 
-- prior to joining against `mm_booking_dataset_base`
"""


def mm_hotel_search_by_market_t1(dataset_id):
    """
    Resource: Hcpu 0.910 Cpu 0.278/0.360 Dread 0.126/0.130 Fpga 0.154/0.159 Dwrite 0.049/0.060 Fabric 0.030

    -- 3656974 rows affected (Tampa)

    Input tables:
    - mm_booking_hotel_summary
    """

    table_name = 'mm_hotel_search_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'search'
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_market_stay_stat_d(dataset_id):
    """
    -- 3432912 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_base
    """

    table_name = 'mm_booking_dataset_market_stay_stat_d'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'

    query_string = """
    select market,
        stay_date,
        statistic_date
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_hotel_search_by_market_t3(dataset_id):
    """
    -- Resource: Hcpu 0.618 Cpu 0.348/0.500 Dread 0.017/0.020 Fpga 0.017/0.020 Dwrite 0.042/0.045
    -- 3394838 rows affected (Tampa)

    Input tables:
    - mm_hotel_search_by_market_t1
    - mm_booking_dataset_market_stay_stat_d
    """

    table_name = 'mm_hotel_search_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_hotel_search_by_market_t4(dataset_id):
    """
    -- demand (searches) for all markets

    -- Resource: Hcpu 0.919 Cpu 0.088/0.120 Dread 0.029/0.076 Fpga 0.008/0.010 Dwrite 0.103/0.125 Fabric 0.005/0.007
    -- 641024 rows affected (Tampa)

    Input tables:
    - mm_hotel_search_by_market_t1
    """

    table_name = 'mm_hotel_search_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_stay_stat_d(dataset_id):
    """
    -- Resource: Hcpu 0.705 Cpu 0.028/0.040 Dread 0.025/0.028 Fpga 0.006/0.007 Dwrite 0.086/0.090 Fabric 0.002
    -- 429114 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_market_stay_stat_d
    """

    table_name = 'mm_booking_dataset_stay_stat_d'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_hotel_search_by_market_t6(dataset_id):
    """
    -- Resource: Hcpu 0.589 Cpu 0.071/0.340 Dread 0.023/0.072 Fpga 0.003 Dwrite 0.096/0.115
    -- 429114 rows affected (Tampa)

    Input tables:
    - mm_hotel_search_by_market_t4
    - mm_booking_dataset_stay_stat_d
    """

    table_name = 'mm_hotel_search_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_search_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_searches
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
    using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_hotel_m_search_base(dataset_id):
    """
    -- before: 200 secs CPU -> 5 secs CPU
    -- Resource: Hcpu 0.256 Cpu 3.566/4.420 Dread 0.167/0.170 Fpga 0.292/0.297 Dwrite 0.686/0.695 Fabric 0.536/0.607
    -- 116452386 rows affected (Tampa)
    -- original: 116452386 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_hotel_search_by_market_t3
    """

    table_name = 'mm_booking_dataset_hotel_m_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to the 2 prior queries, but counting the prior-14-day 
-- searches for each hotel's competitive markets for a given stay_date
--  * note that both of the input tables contain data for both the 
--    target and comp markets, so in the query results, we'll see
--    each target & comp market listed in the 'market' field and the
--    other markets aggregated in 'hotel_competitive_market_searches'
"""


def mm_booking_dataset_hotel_cm_search_base(dataset_id):
    """
    -- inputs:
    --  * mm_booking_dataset_base: contains bookings target and cumulative
    --    bookings target by (hotel_code, dp_id, market, stay_date, stat_date);
    --    contains 39.7M records for OC
    --  * mm_booking_hotel_summary: 12.2M records for Ocean City at level of
    --    (hotel_code, dp_id, market, activity_type, stay_date, event_date)

    -- outputs: 39,292,271 records at (hotel_code, dp_id, market, stay_date,
    -- statistic_date) level

    -- expensive query: run-times of original query across 3 clients =
    -- 278 / 161 / 150 secs
    --  * updated query below by aggregating RHS table first then joining
    --  * original query run-times for OC from further testing:
    --    3m56.565s, 3m39.505s, 4m23.592s
    --  * updated query run-times for Ocean City from further testing:
    --    0m29.923s, 1m8.180s, 0m29.265s


    -- before: 3441 secs CPU -> 4 secs CPU
    -- we should use total market demands: total_hotel_searches instead of the correlated hotel_competitive_market_searches
    -- Resource: Hcpu 0.009 Cpu 2.785/3.160 Dread 0.187/0.334 Fpga 0.326/0.334 Dwrite 0.739/0.785
    -- 116452386 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_hotel_m_search_base
    - mm_hotel_search_by_market_t6
    """

    table_name = 'mm_booking_dataset_hotel_cm_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_m_search_base'
    t2 = 'mm_hotel_search_by_market_t6'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,
        t.total_hotel_searches - m.hotel_market_searches as hotel_competitive_market_searches
    from `{project_id}.{dataset_id}.{t1}` as m
    join `{project_id}.{dataset_id}.{t2}` as t
    using (stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


#############################################################
# *** prior-14 day bookings over various hotel segments *** #
#############################################################


def mm_booking_dataset_hotel_hc_book_base(dataset_id):
    """
    -- for a given (hotel_code, dp_id, market, stay_date, statistic_date),
    -- count the prior-14-day bookings for stays in that hotel through a
    -- given dp_id where the checkin/checkout date range overlaps with
    -- the stay date

    -- inputs:
    --  * mm_booking_dataset_base: contains bookings target and cumulative
    --    bookings target by (hotel_code, dp_id, market, stay_date, book_date);
    --    contains 39.7M records for OC
    --  * mm_booking_hotel_summary: contains counts of hotel events at the
    --    (hotel_code, dp_id, market, activity_type, stay date, event_date)
    --    level; contains 12,162,735 records for OC, 10,658,614 of which have
    --    activity_type = 'search'
    --  * note: the query joins on (hotel_code, dp_id, stay_date), does a
    --    fuzzy match on stay_date <> statistic_date, but no matching at all
    --    market, which is found in both tables; this is because joining on
    --    hotel_code implicitly joins on market as well;

    -- output: 5.2M records at (hotel_code, dp_id, market, stay_date,
    -- statistic_date) level for Ocean City

    -- cheap query: run-times on 3 existing clients = 3 / 2 / 1 secs
    --  * run-times from additional testing: 0m15.014s, 0m13.656s, 0m22.239s

    Input tables:
    - mm_booking_dataset_base
    - mm_booking_hotel_summary
    """

    table_name = 'mm_booking_dataset_hotel_hc_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_hotel_summary'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        sum(hs.events) as hotel_code_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        on b.stay_date = hs.stay_date
            and b.hotel_code = hs.hotel_code
            and b.dp_id = hs.dp_id
            and (hs.event_date >= date_sub(b.statistic_date, interval 14 day))
            and (hs.event_date < b.statistic_date)
    where hs.activity_type = 'book'
    group by 1,2,3,4,5
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to prior query, but counting the prior-14-day bookings
-- for the hotel's entire market (not just for that hotel_code)
"""


def mm_hotel_book_by_market_t1(dataset_id):
    """
    -- for a given (hotel_code, dp_id, market, stay_date, statistic_date),
    -- count the prior-14-day bookings for stays in the hotel's market
    -- where the checkin/checkout date range overlaps with stay date;

    -- inputs:
    --  * mm_booking_dataset_base: contains bookings target and cumulative
    --    bookings target by (hotel_code, dp_id, market, stay_date, stat_date);
    --    contains 39.7M records for OC
    --  * mm_booking_hotel_summary: contains counts of hotel events at the
    --    (hotel_code, dp_id, market, activity_type, stay date, event_date)
    --    level; contains 12.2M records for OC, 10.7M which are searches

    -- output: 24.6M records at level of (hotel_code, dp_id, market, stay_date,
    -- stat_date)

    -- run-time for 3 existing clients: 11 / 6 / 5 seconds
    --  * updated to aggregate booking counts from `mm_booking_hotel_summary`
    --    prior to joining against `mm_booking_dataset_base`
    --  * original query's run-times from further testing on OC:
    --    1m59.603s, 0m53.232s, 0m18.696s, 0m18.405s, 0m18.457s
    --  * updated query's run-times from testing on OC:
    --    0m15.343s, 0m15.911s, 0m15.434s, 0m15.687s

    -- 1614676 rows affected (Tampa)

    Input tables:
    - mm_booking_hotel_summary
    """

    table_name = 'mm_hotel_book_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_hotel_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'book'
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_hotel_book_by_market_t3(dataset_id):
    """
    -- 2882067 rows affected (Tampa)

    Input tables:
    - mm_hotel_book_by_market_t1
    - mm_booking_dataset_market_stay_stat_d
    """

    table_name = 'mm_hotel_book_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_hotel_book_by_market_t4(dataset_id):
    """
    -- 411762 rows affected (Tampa)

    Input tables:
    - mm_hotel_book_by_market_t1
    """

    table_name = 'mm_hotel_book_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_hotel_book_by_market_t6(dataset_id):
    """
    -- 427996 rows affected (Tampa)

    Input tables:
    - mm_hotel_book_by_market_t1
    """

    table_name = 'mm_hotel_book_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_book_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_hotel_bookings
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_hotel_m_book_base(dataset_id):
    """
    -- before: 39 secs CPU -> 5 secs CPU
    -- Resource: Hcpu 0.016 Cpu 3.366/4.140 Dread 0.166/0.169 Fpga 0.291/0.296 Dwrite 0.619/0.630 Fabric 0.507/0.577
    -- 105833975 rows affected

    -- original: 105833975 rows (Tampa)

    Input tables:
    - mm_hotel_book_by_market_t1
    """

    table_name = 'mm_booking_dataset_hotel_m_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as hotel_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
    using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to the 2 prior queries, but counting the prior-14-day 
-- bookings for each hotel's competitive markets for a given stay_date
"""


def mm_booking_dataset_hotel_cm_book_base(dataset_id):
    """
    -- inputs:
    --  * mm_booking_dataset_base: contains bookings target and cumulative
    --    bookings target by (hotel_code, dp_id, market, stay_date, stat_date);
    --    contains 39.7M records for OC
    --  * mm_booking_hotel_summary: 12.2M records for Ocean City at level of
    --    (hotel_code, dp_id, market, activity_type, stay_date, event_date)

    -- outputs: 32.6M records at (hotel_code, dp_id, market, stay_date,
    -- statistic_date) level for Ocean City

    -- expensive query: run-times for 3 clients = 22 / 29 / 22 secs
    --  * updated to aggregate booking counts from `mm_booking_hotel_summary`
    --    prior to joining against `mm_booking_dataset_base`
    --  * original query's run-times based on further testing on Ocean City:
    --    1m7.749s, 0m40.686s, 0m40.806s, 0m41.753s
    --  * updated query's run-times based on further testing on OC:
    --    0m44.470s, 0m19.972s, 0m22.065s, 0m19.677s


    -- before: 347 secs CPU -> 4 secs CPU
    -- we should use total market demands: total_hotel_bookings instead of the correlated hotel_competitive_market_searches
    -- Resource: Hcpu 0.468 Cpu 3.567/4.280 Dread 0.167/0.170 Fpga 0.291/0.296 Dwrite 0.730/0.740 Fabric 0.543/0.557


    -- 116658464 rows affected (Tampa)

    -- original: 116437825 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_hotel_book_by_market_t6
    - mm_hotel_book_by_market_t3
    """

    table_name = 'mm_booking_dataset_hotel_cm_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_hotel_book_by_market_t6'
    t3 = 'mm_hotel_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_hotel_bookings - ifnull(m.events,0) as hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


def mm_booking_dataset_hotel(dataset_id):
    """
    -- combine hotel-event-based features into single table
    --  * all of the constituent tables have the same aggregation
    --    keys: (hotel_code, dp_id, market, stay_date, stat_date)
    --  * to summarize, the features defined in the preceding 6
    --    queries are combined into a single table: search-based
    --    features by hotel / hotel's market / hotel's comp markets;
    --    bookings-based features by hotel / hotel's market / hotel's
    --    comp markets

    -- in-expensive query:
    --  * run-times for 3 existing clients = 14 / 6 / 6 secs

    -- output: 39.M records for Ocean City

    Input tables:
    - mm_booking_dataset_hotel_hc_search_base
    - mm_booking_dataset_hotel_m_search_base
    - mm_booking_dataset_hotel_cm_search_base
    - mm_booking_dataset_hotel_hc_book_base
    - mm_booking_dataset_hotel_m_book_base
    - mm_booking_dataset_hotel_cm_book_base
    """

    table_name = 'mm_booking_dataset_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel_hc_search_base'
    t2 = 'mm_booking_dataset_hotel_m_search_base'
    t3 = 'mm_booking_dataset_hotel_cm_search_base'
    t4 = 'mm_booking_dataset_hotel_hc_book_base'
    t5 = 'mm_booking_dataset_hotel_m_book_base'
    t6 = 'mm_booking_dataset_hotel_cm_book_base'

    query_string = """
    select  hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,

        hotel_code_searches,
        hotel_market_searches,
        hotel_competitive_market_searches,

        hotel_code_bookings,
        hotel_market_bookings,
        hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as hcs
    full outer join `{project_id}.{dataset_id}.{t2}` as ms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t3}` as cms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t4}` as hcb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t5}` as mb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t6}` as cmb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4,
        t5=t5,
        t6=t6
    )

    return query_string, table_ref


#################################################
# *** inputs to flight-event-based features *** #
#################################################


def mm_booking_flight_summary_base(dataset_id):
    """
    -- count flight search/booking events by (dest airport,
    -- market, activity_type, depart/return dates, event date)
    --  * how much of a difference do flight features make?
    --  * what % of training records have flight info?

    -- input: 23.8M events based on updated query with
    -- de-duplication of airport codes / markets beforehand

    -- output: 4.M records for Ocean City

    -- in-expensive query: run-times across 3 clients = 1 / 1 / 1 secs

    Input tables:
    - mm_ekv_flight
    """

    table_name = 'mm_booking_flight_summary_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_ekv_flight'

    query_string = """
    select f.destination_airport,
        f.market,
        f.activity_type,
        f.departure_date,
        f.return_date,
        date(f.event_ts) as event_date,
        count(*) as events
    from `{project_id}.{dataset_id}.{t1}` as f
    group by 1,2,3,4,5,6
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_flight_summary_t1(dataset_id):
    """
    -- expand each flight search/booking to grid-of-trip-dates, creating
    -- one record per calendar date between depart/return dates, as done
    -- above with each hotel search/booking;

    -- then aggregate up to the (dest airport, market, activity_type,
    -- stay date, event date) level, which collapses the record count
    -- from 4.8M to 2.9M records because even though each flight record
    -- is expanded to the number of days in the trip, this query then
    -- aggregates across different flights searches/bookings conducted
    -- on a given event date for a given stay date

    -- inputs:
    --  * mm_booking_flight_summary_base: 4,800,874 records
    --  * inf_day_d

    -- output: 2.9M records for Ocean City

    -- in-expensive query: run-times across 3 clients = 4 / 3/ 4 secs

    -- Resource: Hcpu 0.010 Cpu 0.627/0.820 Dread 0.110/0.125 Fpga 0.094/0.107 Dwrite 0.234/0.245
    -- 56737129 rows affected (Tampa)
    -- should exclude stay_date > current_date + '1 year'

    Input tables:
    - mm_booking_flight_summary_base
    - inf_day_d
    """

    table_name = 'mm_booking_flight_summary_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base'

    query_string = """
    with T1 as (
        select departure_date,
            return_date
        from `{project_id}.{dataset_id}.{t1}`
        group by 1,2)
    select departure_date,
        return_date,
        d.cal_date stay_date
    from T1
    join `{project_id_datamaster}.{dataset_id_datamaster}.inf_day_d` as d
        on (d.cal_date between T1.departure_date and T1.return_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_flight_summary(dataset_id):
    """
    -- before: 31 secs CPU -> 18 secs CPU
    -- Resource: Hcpu 0.030 Cpu 14.749/18.280 Dread 0.160/0.175 Fpga 0.245/0.266 Dwrite 0.083/0.085 Fabric 0.804/0.863
    -- 9958191 rows (Tampa)

    Input tables:
    - mm_booking_flight_summary_base
    - mm_booking_flight_summary_t1
    """

    table_name = 'mm_booking_flight_summary'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary_base'
    t2 = 'mm_booking_flight_summary_t1'

    query_string = """
    select fs.destination_airport,
        fs.market,
        fs.activity_type,
        t1.stay_date,
        fs.event_date,
        sum(fs.events) as events
    from `{project_id}.{dataset_id}.{t1}` as fs
        join `{project_id}.{dataset_id}.{t2}` as t1
            using (departure_date, return_date)
    group by 1,2,3,4,5
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


############################################################
# *** prior-14 day flight searches over hotel segments *** #
############################################################


def mm_flight_search_by_market_t1(dataset_id):
    """
    -- for a given (hotel_code, dp_id, market, stay_date, statistic_date),
    -- count the prior-14-day searches for flights to the hotel's market
    -- where the hotel stay date matches a date in the flight's departure
    -- / return date range

    -- inputs:
    --  * mm_booking_dataset_base: hotel bookings aggregated to level of:
    --    (hotel, dp_id, market, stay_date, book_date); 39.7M records for OC
    --  * mm_booking_flight_summary: flight search/booking events expanded
    --    to grid-of-trip-dates and then aggregated; has 2.9M records for OC

    -- output: 38.2M records

    -- slightly expensive query: updated and original versions of query have
    -- very similar run times
    --  * updated to aggregate search counts from `mm_booking_flight_summary`
    --    prior to joining against `mm_booking_dataset_base`, but with little
    --    impact to run-times
    --  * prior run-times for 3 existing clients = 6 / 6 / 6 secs
    --  * original query run-times from more tests on OC:
    --    1m8.176s, 0m19.255s, 0m19.320s, 0m19.012s
    --  * updated query run-times from more tests on OC:
    --    0m19.833s, 0m18.363s, 0m18.599s, 1m0.864s

    -- 5784951 rows affected (Tampa)

    Input tables:
    - mm_booking_flight_summary
    """

    table_name = 'mm_flight_search_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'search'
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_flight_search_by_market_t3(dataset_id):
    """
    -- 2989231 rows affected (Tampa)

    Input tables:
    - mm_flight_search_by_market_t1
    - mm_booking_dataset_market_stay_stat_d
    """

    table_name = 'mm_flight_search_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_flight_m_search_base(dataset_id):
    """
    -- before: 19 secs CPU -> 4 secs CPU
    -- Resource: Hcpu 0.258 Cpu 3.322/3.920 Dread 0.167/0.170 Fpga 0.295/0.302 Dwrite 0.617/0.630 Fabric 0.502/0.565
    -- 104464465 rows affected (Tampa)

    -- original: 104464465 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_flight_search_by_market_t3
    """

    table_name = 'mm_booking_dataset_flight_m_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to the prior queries, but counting the prior-14-day 
-- searches for flights to each hotel's competitive markets for a 
-- given stay_date
"""


def mm_flight_search_by_market_t4(dataset_id):
    """
    -- inputs:
    --  * mm_booking_dataset_base: hotel bookings aggregated to level of:
    --    (hotel, dp_id, market, stay_date, book_date); 39.7M records for OC
    --  * mm_booking_flight_summary: flight search/booking events expanded
    --    to grid-of-trip-dates and then aggregated; has 2.9M records for OC

    -- outputs: 39.6M records at (hotel_code, dp_id, market, stay_date,
    -- statistic_date) level

    -- moderately expensive query: run-times of original query across 3
    -- clients = 28 / 28 / 30 secs
    --  * updated to aggregate RHS table before join: results in moderate
    --    ~33% reduction in run-time
    --  * original query run-times from further testing on OC:
    --    0m54.034s, 0m45.272s, 1m7.211s, 0m45.740s
    --  * updated query run-times from further testing on OC:
    --    0m33.702s, 0m31.250s, 0m30.777s, 0m30.546s

    -- 1601494 rows affected (Tampa)

    Input tables:
    - mm_flight_search_by_market_t1
    """

    table_name = 'mm_flight_search_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_flight_search_by_market_t6(dataset_id):
    """
    -- 429114 rows affected (Tampa)

    Input tables:
    - mm_flight_search_by_market_t4
    - mm_booking_dataset_stay_stat_d
    """

    table_name = 'mm_flight_search_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_search_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_searches
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_flight_cm_search_base(dataset_id):
    """
    --- before: 201 secs CPU -> 5 secs CPU
    -- we should use total market demands: total_flight_searches instead of the correlated flight_competitive_market_searches
    -- Resource: Hcpu 0.745 Cpu 4.047/4.540 Dread 0.168/0.171 Fpga 0.291/0.297 Dwrite 0.748/0.760 Fabric 0.545/0.557

    -- 116965242 rows affected  (check for duplicate rows as hotel -> market -> airport) (Tampa)

    -- original: 116965242 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_flight_search_by_market_t6
    - mm_flight_search_by_market_t3
    """

    table_name = 'mm_booking_dataset_flight_cm_search_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_search_by_market_t6'
    t3 = 'mm_flight_search_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_searches - ifnull(m.events,0) as flight_competitive_market_searches
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


############################################################
# *** prior-14 day flight bookings over hotel segments *** #
############################################################


def mm_flight_book_by_market_t1(dataset_id):
    """
    -- for a given (hotel_code, dp_id, market, stay_date, statistic_date),
    -- count the prior-14-day booked flights headed to the hotel's market
    -- where departure/return date range overlaps with stay date;

    -- inputs:
    --  * mm_booking_dataset_base: hotel bookings aggregated to level of:
    --    (hotel, dp_id, market, stay_date, book_date); 39.7M records for OC
    --  * mm_booking_flight_summary: flight search/booking events expanded
    --    to grid-of-trip-dates and then aggregated; has 2.9M records for OC

    -- output: 23.6M records at level of (hotel_code, dp_id, market,
    -- stay_date, stat_date)

    -- run-time for 3 existing clients: 11 / 6 / 5 seconds
    --  * updated to aggregate search counts from `mm_booking_flight_summary`
    --    prior to joining against `mm_booking_dataset_base`; results in
    --    little change to run-times
    --  * original query run-times from additional testing on OC:
    --    0m15.919s, 0m15.754s, 0m54.409s, 0m15.778s
    --  * updated query run-times from additional testing on OC:
    --    0m20.165s, 0m15.269s, 0m15.685s, 0m15.604s

    -- 2063914 rows affected (Tampa)

    Input tables:
    - mm_booking_flight_summary
    """

    table_name = 'mm_flight_book_by_market_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_flight_summary'

    query_string = """
    select market,
        stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    where activity_type = 'book'
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_flight_book_by_market_t3(dataset_id):
    """
    -- 2243677 rows affected (Tampa)

    Input tables:
    - mm_flight_book_by_market_t1
    - mm_booking_dataset_market_stay_stat_d
    """

    table_name = 'mm_flight_book_by_market_t3'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1'
    t2 = 'mm_booking_dataset_market_stay_stat_d'

    query_string = """
    select market,
        stay_date,
        statistic_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}` as t1
    join `{project_id}.{dataset_id}.{t2}` as t2
        using (market, stay_date)
    where t1.event_date between date_sub(t2.statistic_date, interval 14 day)
        and date_sub(t2.statistic_date, interval 1 day)
    group by 1,2,3
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_flight_book_by_market_t4(dataset_id):
    """
    -- demand (searches) for all markets

    -- 449099 rows affected (Tampa)

    Input tables:
    - mm_flight_book_by_market_t1
    """

    table_name = 'mm_flight_book_by_market_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t1'

    query_string = """
    select stay_date,
        event_date,
        sum(events) as events
    from `{project_id}.{dataset_id}.{t1}`
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_flight_book_by_market_t6(dataset_id):
    """
    -- 416942 rows affected (Tampa)

    Input tables:
    - mm_flight_book_by_market_t4
    - mm_booking_dataset_stay_stat_d
    """

    table_name = 'mm_flight_book_by_market_t6'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_flight_book_by_market_t4'
    t2 = 'mm_booking_dataset_stay_stat_d'

    query_string = """
    select stay_date,
        statistic_date,
        sum(events) as total_flight_bookings
    from `{project_id}.{dataset_id}.{t1}` as t4
    join `{project_id}.{dataset_id}.{t2}` as t5
        using (stay_date)
    where t4.event_date between date_sub(t5.statistic_date, interval 14 day)
        and date_sub(t5.statistic_date, interval 1 day)
    group by 1,2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_flight_m_book_base(dataset_id):
    """
    -- before: 10 secs CPU -> 4 secs CPU
    -- Resource: Hcpu 0.011 Cpu 2.974/3.500 Dread 0.165/0.169 Fpga 0.290/0.296 Dwrite 0.527/0.535 Fabric 0.462/0.516

    -- 90309093 rows affected (Tampa)

    -- original: 90309093 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_flight_book_by_market_t3
    """

    table_name = 'mm_booking_dataset_flight_m_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        events as flight_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as hs
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


"""
-- very similar to the 2 prior queries, but counting the prior-14-day 
-- booked flights to each hotel's competitive markets for given stay_date
"""


def mm_booking_dataset_flight_cm_book_base(dataset_id):
    """
    -- inputs:
    --  * mm_booking_dataset_base: hotel bookings aggregated to level of:
    --    (hotel, dp_id, market, stay_date, book_date); 39.7M records for OC
    --  * mm_booking_flight_summary: flight search/booking events expanded
    --    to grid-of-trip-dates and then aggregated; has 2.9M records for OC

    -- outputs: 32.3M records at (hotel_code, dp_id, market, stay_date,
    -- statistic_date) level

    -- run-times across 3 clients = 11 / 10 / 10 secs
    --  * updated to aggregate RHS table prior to join
    --  * original query run-times from further testing on OC:
    --    0m22.131s, 0m46.793s, 0m22.681s
    --  * updated query run-times from further testing on OC:
    --    0m11.856s, 0m22.537s, 0m22.443s

    -- before: 73 secs CPU -> 4 secs CPU
    -- we should use total market demands: total_hotel_bookings instead of the correlated hotel_competitive_market_searches
    -- Resource: Hcpu 0.016 Cpu 3.487/4.000 Dread 0.168/0.189 Fpga 0.289/0.295 Dwrite 0.718/0.805 Fabric 0.534/0.547
    -- 113647573 rows affected (Tampa)

    -- original: 113084491 rows

    Input tables:
    - mm_booking_dataset_base
    - mm_flight_book_by_market_t6
    - mm_flight_book_by_market_t3
    """

    table_name = 'mm_booking_dataset_flight_cm_book_base'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_flight_book_by_market_t6'
    t3 = 'mm_flight_book_by_market_t3'

    query_string = """
    select b.hotel_code,
        b.dp_id,
        b.market,
        b.stay_date,
        b.statistic_date,
        t.total_flight_bookings - ifnull(m.events,0) as flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as b
    join `{project_id}.{dataset_id}.{t2}` as t
        using (stay_date, statistic_date)
    left outer join `{project_id}.{dataset_id}.{t3}` as m
        using (market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


def mm_booking_dataset_flight(dataset_id):
    """
    -- combine features based on searched & booked flights to hotel
    -- market or competing markets

    -- inputs:
    --  * mm_booking_dataset_flight_m_search_base: counts of prior
    --    14-day searched flights headed to each hotel's market
    --    where depart/return date range overlaps with stay date;
    --    contains 38,190,794 records for OC
    --  * mm_booking_dataset_flight_cm_search_base: counts of prior
    --    14-day searched flights headed to each hotel's competing
    --    markets where depart/return date range overlaps with stay
    --    date; contains 39,645,770 records for OC
    --  * mm_booking_dataset_flight_m_book_base: counts of prior
    --    14-day booked flights headed to each hotel's market
    --    where depart/return date range overlaps with stay date;
    --    contains 23,634,360 records for OC
    --  * mm_booking_dataset_flight_cm_book_base: counts of prior
    --    14-day booked flights headed to each hotel's competing
    --    markets where depart/return date range overlaps with stay
    --    date; contains 32,321,459 records for OC

    -- output: 39.7M records at the (hotel_code, dp_id, market,
    -- stay_date, statistic_date) level

    Input tables:
    - mm_booking_dataset_flight_m_search_base
    - mm_booking_dataset_flight_cm_search_base
    - mm_booking_dataset_flight_m_book_base
    - mm_booking_dataset_flight_cm_book_base
    """

    table_name = 'mm_booking_dataset_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight_m_search_base'
    t2 = 'mm_booking_dataset_flight_cm_search_base'
    t3 = 'mm_booking_dataset_flight_m_book_base'
    t4 = 'mm_booking_dataset_flight_cm_book_base'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        statistic_date,

        flight_market_searches,
        flight_competitive_market_searches,

        flight_market_bookings,
        flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as ms
    full outer join `{project_id}.{dataset_id}.{t2}` as cms
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t3}` as mb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    full outer join `{project_id}.{dataset_id}.{t4}` as cmb
        using (hotel_code, dp_id, market, stay_date, statistic_date)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4
    )

    return query_string, table_ref


def mm_booking_holidays(dataset_id):
    """
    -- create a mapping between each stay_date in the booking table
    -- `mm_booking_dataset_base` and the dates of the most recent
    -- and next upcoming holidays between

    -- output: 1,340 stay_date / (previous holiday, next holiday)
    -- combinations for Ocean City

    Input tables:
    - mm_booking_dataset_base
    - mm_holiday_dates
    """

    table_name = 'mm_booking_holidays'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'

    query_string = """
    with d as (
        select distinct b.stay_date
        from `{project_id}.{dataset_id}.{t1}` as b
    )
    select d.stay_date,
        max(b.holiday_date) as last_holiday_date,
        min(a.holiday_date) as next_holiday_date
    from d,
        `{project_id_datamaster}.{dataset_id_datamaster}.mm_holiday_dates` as b,
        `{project_id_datamaster}.{dataset_id_datamaster}.mm_holiday_dates` as a
    where b.holiday_date < d.stay_date
        and a.holiday_date > d.stay_date
    group by 1
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_t1(dataset_id):
    """
    -- combine training features by joining dimension and booking tables and
    -- aggregate across statistic_dates to statistic_week
    --  * since fact tables are at level of (hotel_code, dp_id, stay_date,
    --    statistic_date), this query is taking min / max / avg across
    --    statistic_dates within each statistic_week

    -- inputs:
    --  * dimension tables: mm_booking_holidays, holiday_dates, mm_markets,
    --    mm_properties
    --  * fact tables: mm_booking_dataset_base, mm_booking_dataset_hotel,
    --    mm_booking_dataset_flight

    -- output: 5,464,587 records at level of (hotel_code, dp_id, market,
    -- stay_date, statistic_week);
    --  * reduction in row count is due to aggregation from date of search /
    --    booking event to week of search / booking event;

    -- moderately expensive query with run-times across 3 clients =
    -- 16 / 16 / 15 secs

    -- general follow-up questions:
    --  * would aggregating then joining reduce run-time of query?
    --  * how much do the min / max / avg of each features across statistic
    --    dates within a week help the model versus just training on the
    --    min / max or avg?
    --  * look at feature importance in ensemble model to see if cumulative
    --    features are necessary since they're expensive
    --  * in a time-series model, would these cumulative features be necessary?
    --  * could we satisfy all requirements by modeling at a market instead of
    --    individual hotel level?

    Input tables:
    - mm_booking_dataset_base
    - mm_booking_holidays
    - mm_markets
    - mm_properties
    """

    table_name = 'mm_booking_dataset_t1'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'
    t2 = 'mm_booking_holidays'
    t3 = 'mm_markets'
    t4 = 'mm_properties'

    query_string = """
    with t as (
        select db.hotel_code,
            db.dp_id,
            db.market,
            db.stay_date,
            date_sub(db.stay_date,
                interval (cast(floor(DATE_DifF(db.stay_date, db.statistic_date, day) / 7.0) * 7 as INT64)) day)
                    as statistic_week,
                    
            max(db.cumulative_bookings_target) as cumulative_bookings_target,
            max(db.total_bookings_target) as total_bookings_target,
        
            max(extract(month from db.stay_date)) as stay_month,
            max(extract(dayofweek from db.stay_date)) as stay_dow,

            min(extract(month from db.statistic_date)) as min_statistic_month,
            max(extract(month from db.statistic_date)) as max_statistic_month,

            min(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as min_days_since_holiday,
            max(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as max_days_since_holiday,
            avg(DATE_DifF(db.stay_date, hs.last_holiday_date, day)) as avg_days_since_holiday,
            min(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as min_days_until_holiday,
            min(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as max_days_until_holiday,
            avg(DATE_DifF(hs.next_holiday_date, db.stay_date, day)) as avg_days_until_holiday,
            cast(min((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as INT64) as min_weeks_open,
            cast(max((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as INT64) as max_weeks_open,
            avg((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as avg_weeks_open,
            stddev((DATE_DifF(db.statistic_date, p.open_date, day)) / 7) as stddev_weeks_open
        from `{project_id}.{dataset_id}.{t1}` as db
        left join `{project_id}.{dataset_id}.{t2}` as hs
            on (db.stay_date = hs.stay_date)
        left join `{project_id_datamaster}.{dataset_id_datamaster}.mm_holiday_dates` as hd
            on (db.stay_date = hd.holiday_date)
        left join `{project_id}.{dataset_id}.{t3}` as m
            on (db.market = m.market)
        left join `{project_id}.{dataset_id}.{t4}` as p
            on (db.hotel_code = p.hotel_code) and (db.dp_id = p.dp_id)
        where db.stay_date >= date('2016-01-01')
        group by 1,2,3,4,5
        having count(db.statistic_date) = 7
    )
    select t.*,
        cast((date_diff(t.stay_date, t.statistic_week, day) / 7) as INT64) as stay_window
    from t
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        project_id_datamaster=project_id_datamaster,
        dataset_id_datamaster=dataset_id_datamaster,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4
    )

    return query_string, table_ref


def mm_booking_dataset_t2(dataset_id):
    """
    -- Resource: Hcpu 0.012 Cpu 2.630/3.500 Dread 0.123/0.129 Fpga 0.101/0.113 Dwrite 0.660/0.685
    -- 16112644 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_t1
    - mm_markets
    - mm_properties
    """

    table_name = 'mm_booking_dataset_t2'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t1'
    t2 = 'mm_markets'
    t3 = 'mm_properties'

    query_string = """
    select hotel_code,
        dp_id,
        t.market,
        stay_date,
        statistic_week,
        cumulative_bookings_target,
        total_bookings_target,
        stay_window,
        stay_month,
        stay_dow,
        min_statistic_month,
        max_statistic_month,
        min_days_since_holiday,
        max_days_since_holiday,
        avg_days_since_holiday,
        min_days_until_holiday,
        max_days_until_holiday,

        avg_days_until_holiday,
        p.number_of_rooms,
        1.0 * p.number_of_rooms / nullif(m.number_of_rooms,0) as room_share_market,

        min_weeks_open,
        max_weeks_open,
        avg_weeks_open,
        stddev_weeks_open,
        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(chain_scale), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as chain_scale,
        floors,
        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(p.location), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as location,

        has_indoor_corridors,
        has_restaurant,
        has_convention,
        has_conference,
        has_spa,
        has_single_meeting_space,
        largest_meeting_space,
        total_meeting_space,
        is_resort,
        is_ski_resort,
        is_golf_resort,
        is_all_suites,
        is_casino,

        REGEXP_REPLACE(trim(REGEXP_REPLACE(lower(p.price), r"[^a-zA-Z\d\s:]", '')), ' ', '_') as price,
        single_low_rate,
        single_high_rate,
        double_low_rate,
        double_high_rate,
        suite_low_rate,
        suite_high_rate
    from `{project_id}.{dataset_id}.{t1}` as t
    join `{project_id}.{dataset_id}.{t2}` as m
        using (market)
    join `{project_id}.{dataset_id}.{t3}` as p
        using (hotel_code, dp_id)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


def mm_booking_dataset_t3_hotel(dataset_id):
    """
    -- Resource: Hcpu 0.030 Cpu 12.125/14.220 Dread 0.311/0.318 Fpga 0.633/0.736 Dwrite 0.365/0.390 Fabric 2.679/2.724

    -- 16561428 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_hotel
    """

    table_name = 'mm_booking_dataset_t3_hotel'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_hotel'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        date_sub(stay_date, interval (cast(floor(date_diff(stay_date, statistic_date, day) / 7.0) * 7 as INT64)) day)
            as statistic_week,

        max(ifnull(h.hotel_code_searches, 0)) as max_hotel_code_searches,
        min(ifnull(h.hotel_code_searches, 0)) as min_hotel_code_searches,
        avg(ifnull(h.hotel_code_searches, 0)) as avg_hotel_code_searches,

        max(ifnull(h.hotel_market_searches, 0)) as max_hotel_market_searches,
        min(ifnull(h.hotel_market_searches, 0)) as min_hotel_market_searches,
        avg(ifnull(h.hotel_market_searches, 0)) as avg_hotel_market_searches,

        max(ifnull(h.hotel_competitive_market_searches, 0)) as max_hotel_competitive_market_searches,
        min(ifnull(h.hotel_competitive_market_searches, 0)) as min_hotel_competitive_market_searches,
        avg(ifnull(h.hotel_competitive_market_searches, 0)) as avg_hotel_competitive_market_searches,

        max(ifnull(h.hotel_code_bookings, 0)) as max_hotel_code_bookings,
        min(ifnull(h.hotel_code_bookings, 0)) as min_hotel_code_bookings,
        avg(ifnull(h.hotel_code_bookings, 0)) as avg_hotel_code_bookings,

        max(ifnull(h.hotel_market_bookings, 0)) as max_hotel_market_bookings,
        min(ifnull(h.hotel_market_bookings, 0)) as min_hotel_market_bookings,
        avg(ifnull(h.hotel_market_bookings, 0)) as avg_hotel_market_bookings,

        max(ifnull(h.hotel_competitive_market_bookings, 0)) as max_hotel_competitive_market_bookings,
        min(ifnull(h.hotel_competitive_market_bookings, 0)) as min_hotel_competitive_market_bookings,
        avg(ifnull(h.hotel_competitive_market_bookings, 0)) as avg_hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as h
    where stay_date >= date('2016-01-01')
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset_t4(dataset_id):
    """
    -- Resource: Hcpu 0.020 Cpu 3.484/4.360 Dread 0.251/0.282 Fpga 0.439/0.462 Dwrite 0.872/0.910

    -- 16112644 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_t2
    - mm_booking_dataset_t3_hotel
    """

    table_name = 'mm_booking_dataset_t4'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t2'
    t2 = 'mm_booking_dataset_t3_hotel'

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}`
    left outer join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_t5_flight(dataset_id):
    """
    -- Resource: Hcpu 0.027 Cpu 9.311/10.900 Dread 0.282/0.287 Fpga 0.512/0.544 Dwrite 0.359/0.380 Fabric 1.892/1.925

    -- 16561428 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_flight
    """

    table_name = 'mm_booking_dataset_t5_flight'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_flight'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        date_sub(stay_date, interval (cast(floor(date_diff(stay_date, statistic_date, day) / 7.0) * 7 as INT64)) day)
            as statistic_week,

        max(ifnull(f.flight_market_searches, 0)) as max_flight_market_searches,
        min(ifnull(f.flight_market_searches, 0)) as min_flight_market_searches,
        avg(ifnull(f.flight_market_searches, 0)) as avg_flight_market_searches,

        max(ifnull(f.flight_competitive_market_searches, 0)) as max_flight_competitive_market_searches,
        min(ifnull(f.flight_competitive_market_searches, 0)) as min_flight_competitive_market_searches,
        avg(ifnull(f.flight_competitive_market_searches, 0)) as avg_flight_competitive_market_searches,

        max(ifnull(f.flight_market_bookings, 0)) as max_flight_market_bookings,
        min(ifnull(f.flight_market_bookings, 0)) as min_flight_market_bookings,
        avg(ifnull(f.flight_market_bookings, 0)) as avg_flight_market_bookings,

        max(ifnull(f.flight_competitive_market_bookings, 0)) as max_flight_competitive_market_bookings,
        min(ifnull(f.flight_competitive_market_bookings, 0)) as min_flight_competitive_market_bookings,
        avg(ifnull(f.flight_competitive_market_bookings, 0)) as avg_flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}` as f
    where stay_date >= date('2016-01-01')
    group by 1,2,3,4,5
    having count(statistic_date) = 7
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_booking_dataset(dataset_id):
    """
    -- Resource: Hcpu 0.018 Cpu 4.187/5.520 Dread 0.303/0.346 Fpga 0.494/0.520 Dwrite 1.082/1.140

    -- 16112644 rows affected (Tampa)

    Input tables:
    - mm_booking_dataset_t4
    - mm_booking_dataset_t5_flight
    """

    table_name = 'mm_booking_dataset'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_t4'
    t2 = 'mm_booking_dataset_t5_flight'

    query_string = """
    select hotel_code, dp_id, market, stay_date, statistic_week, cumulative_bookings_target,
        total_bookings_target, stay_window, stay_month, stay_dow, min_statistic_month,
        max_statistic_month, min_days_since_holiday, max_days_since_holiday,
        avg_days_since_holiday, min_days_until_holiday, max_days_until_holiday,
        avg_days_until_holiday,
        number_of_rooms, room_share_market, min_weeks_open, max_weeks_open,
        avg_weeks_open, stddev_weeks_open, chain_scale, floors, location, has_indoor_corridors,
        has_restaurant, has_convention, has_conference, has_spa, has_single_meeting_space,
        largest_meeting_space, total_meeting_space, is_resort, is_ski_resort, is_golf_resort,
        is_all_suites, is_casino, price, single_low_rate, single_high_rate, double_low_rate,
        double_high_rate, suite_low_rate, suite_high_rate,
        max_hotel_code_searches, min_hotel_code_searches, avg_hotel_code_searches,
        max_hotel_market_searches, min_hotel_market_searches, avg_hotel_market_searches,
        max_hotel_competitive_market_searches, min_hotel_competitive_market_searches,
        avg_hotel_competitive_market_searches,
        max_hotel_code_bookings, min_hotel_code_bookings, avg_hotel_code_bookings,
        max_hotel_market_bookings, min_hotel_market_bookings, avg_hotel_market_bookings,
        max_hotel_competitive_market_bookings, min_hotel_competitive_market_bookings,
        avg_hotel_competitive_market_bookings,
        max_flight_market_searches, min_flight_market_searches,
        avg_flight_market_searches, max_flight_competitive_market_searches,
        min_flight_competitive_market_searches, avg_flight_competitive_market_searches,
        max_flight_market_bookings, min_flight_market_bookings, avg_flight_market_bookings,
        max_flight_competitive_market_bookings, min_flight_competitive_market_bookings,
        avg_flight_competitive_market_bookings
    from `{project_id}.{dataset_id}.{t1}`
    left outer join `{project_id}.{dataset_id}.{t2}`
        using (hotel_code, dp_id, market, stay_date, statistic_week)
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_booking_dataset_full(dataset_id):
    """
    -- final dataset used as input to model training, with addition of 'market_total_rooms' from 'mm_markets' table

    Input tables:
    - mm_booking_dataset
    - mm_markets
    """

    table_name = 'mm_booking_dataset_full'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset'
    t2 = 'mm_markets'

    query_string = """
    select a.*,
        b.NUMBER_OF_ROOMS as market_total_rooms
    from `{project_id}.{dataset_id}.{t1}` as a,
        `{project_id}.{dataset_id}.{t2}` as b
    where a.market = b.market
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


##############################
# *** Multiplier queries *** #
##############################


def mm_data_partner_hotel_stay_info(dataset_id):
    table_name = 'mm_data_partner_hotel_stay_info'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_booking_dataset_base'

    query_string = """
    select hotel_code,
        dp_id,
        market,
        stay_date,
        date_sub(stay_date, interval cast(floor(DATE_DIFF(stay_date, statistic_date, day) / 7.0) * 7 as INT64) day )
            as statistic_week,
        max(extract(month from stay_date)) as stay_month,
        -- target (response) variables  
        max(cumulative_bookings_target) as cumulative_bookings_target,
        max(total_bookings_target) as total_bookings_target
    from `{project_id}.{dataset_id}.{t1}`
    where stay_date >= date('2016-01-01')
        and stay_date <= date_sub(current_date, interval extract(day from current_date) day)
    group by 1, 2, 3, 4, 5
    having count(statistic_date) = 7
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_data_partner_hotel_stay_info_total_bookings(dataset_id):
    """
    -- To remove duplicated hotel total bookings
    """
    table_name = 'mm_data_partner_hotel_stay_info_total_bookings'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_data_partner_hotel_stay_info'

    query_string = """
    select hotel_code, 
        dp_id,
        market,
        stay_date,
        stay_month,
        extract(year from stay_date) as stay_year,
        max(total_bookings_target) as total_bookings_target
    from `{project_id}.{dataset_id}.{t1}`
    group by 1, 2, 3, 4, 5, 6
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_hotel_month_pct_bookings(dataset_id):
    """
    -- In hotel, stay_month level
    -- bookings percentage is ranked
    -- cumul bookings percentage
    """
    table_name = 'mm_hotel_month_pct_bookings'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_data_partner_hotel_stay_info_total_bookings'

    query_string = """
    with mkt_monthly_bkings as(
        select market,
            extract(year from stay_date) as stay_year,
            extract(month from stay_date) as stay_month,
            sum(case 
                    when total_bookings_target is null then 0
                    else total_bookings_target
                end) as market_monthly_bookings
        from `{project_id}.{dataset_id}.{t1}`
        group by 1, 2, 3
    ),
    htl_monthly_bkings as (
        select market,
            dp_id,
            hotel_code,
            extract(year from stay_date) as stay_year,
            extract(month from stay_date) as stay_month,
            sum(case
                    when total_bookings_target is null then 0
                    else total_bookings_target
                end) as hotel_monthly_bookings
        from `{project_id}.{dataset_id}.{t1}`
        group by 1, 2, 3, 4, 5
    ),
    t1 as (
        select h.market,
            h.dp_id,
            h.hotel_code,
            h.stay_year,
            h.stay_month,
            h.hotel_monthly_bookings,
            case
                when market_monthly_bookings = 0 then 0
                else 1.0 * hotel_monthly_bookings/market_monthly_bookings
            end as hotel_monthly_pct_bookings
        from htl_monthly_bkings h 
        join mkt_monthly_bkings m
            on h.market = m.market and h.stay_year = m.stay_year and h.stay_month = m.stay_month
    ),
    t2 as (
        select *, 
        sum(t.hotel_monthly_pct_bookings) over(partition by market, stay_year, stay_month
            order by t.hotel_monthly_pct_bookings) as cumul_hotel_monthly_pct_bookings
        from t1 as t
    )
    select *,
        case
            when cumul_hotel_monthly_pct_bookings <= 0.1 then true
            else false
        end cumul_outage
    from t2
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1
    )

    return query_string, table_ref


def mm_occupancy_rate_stats(dataset_id):
    """
    """
    table_name = 'mm_occupancy_rate_stats'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_hotel_month_pct_bookings'
    t2 = 'mm_properties'

    query_string = """
    with occ_rate_by_hotel_month as (
        select b.market, b.dp_id, b.hotel_code,
            stay_year, stay_month,
            hotel_monthly_bookings,
            cumul_outage,
            p.number_of_rooms,
            ifnull(hotel_monthly_bookings / (p.number_of_rooms*30), 0) as occ_rate
        from `{project_id}.{dataset_id}.{t1}` as b
        join `{project_id}.{dataset_id}.{t2}` p
            using (market, dp_id, hotel_code)
    ),
    occ_stats_by_hotel_month as (
        select *,
            round(
                avg(occ_rate) over (
                    partition by market, dp_id, hotel_code
                    order by stay_year, stay_month
                    rows between 12 preceding and 1 preceding),
            3) as avg_occ_rate,
            round(
                stddev(occ_rate) over (
                    partition by market, dp_id, hotel_code
                    order by stay_year, stay_month
                    rows between 12 preceding and 1 preceding),
            5) as std_occ_rate,
            sum(1) over (
                partition by market, dp_id, hotel_code
                order by stay_year, stay_month
                rows between 12 preceding and 1 preceding)
            as prior_months
        from occ_rate_by_hotel_month
    ),
    t1 as (
        select *,
            case
                when prior_months < 12 then false
                else true
            end as has_data
        from occ_stats_by_hotel_month
    )
    select *,
        case
            when occ_rate < (avg_occ_rate - 0.75 * std_occ_rate) and has_data then true
            else false
        end as outage_std_0_75,
        case
            when occ_rate < (avg_occ_rate - 1.0 * std_occ_rate) and has_data then true
            else false
        end as outage_std_1_0,
        case
            when occ_rate < (avg_occ_rate - 1.5 * std_occ_rate) and has_data then true
            else false
        end as outage_std_1_5
    from t1
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2
    )

    return query_string, table_ref


def mm_occupancy_rate_multipliers(dataset_id):
    """
    """
    table_name = 'mm_occupancy_rate_multipliers'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_occupancy_rate_stats'
    t2 = 'mm_markets'
    t3 = 'search_book'

    query_string = """
    with multipliers_occ_rate as (
        select s.market,
            s.stay_year,
            s.stay_month,
            -- rooms in overall market (fixed across time)
            m.number_of_rooms as rooms_market,
            -- rooms in observed market (varying by month)
            sum(s.number_of_rooms) as rooms_training,
            -- rooms in observed market, excluding outage hotels
            -- based on (avg - 0.75*stddev) threshold: varies by month
            sum(
                case 
                    when not outage_std_0_75 or not cumul_outage then s.number_of_rooms 
                    else 0 
                end
            ) as rooms_trunc_0_75,
            -- rooms in observed market, excluding outage hotels
            -- based on (avg - 1.0*stddev) threshold: varies by month
            sum(
                case 
                    when not outage_std_1_0 or not cumul_outage then s.number_of_rooms 
                    else 0 
                end
            ) as rooms_trunc_1_0,
            -- rooms in observed market, excluding outage hotels
            -- based on (avg - 1.5*stddev) threshold: varies by month
            sum(
                case 
                    when not outage_std_1_5 or not cumul_outage then s.number_of_rooms 
                    else 0 
                end
            ) as rooms_trunc_1_5
        from `{project_id}.{dataset_id}.{t1}` as s
        join `{project_id}.{dataset_id}.{t2}` as m
            on s.market = m.market
        group by 1, 2, 3, 4
    ),
    t1 as (
        select *,
            round(rooms_market / nullif(rooms_training, 0), 3) as mult_var,
            rooms_market / nullif(rooms_trunc_0_75, 0) as mult_var_trunc_0_75,
            rooms_market / nullif(rooms_trunc_1_0, 0) as mult_var_trunc_1_0,
            rooms_market / nullif(rooms_trunc_1_5, 0) as mult_var_trunc_1_5
        from multipliers_occ_rate
    )
    select m.market,
        m.stay_year,
        m.stay_month,
        rooms_market,
        rooms_training,
        rooms_trunc_0_75,
        rooms_trunc_1_0,
        rooms_trunc_1_5,
        mult_var,
        mult_var_trunc_0_75,
        mult_var_trunc_1_0,
        mult_var_trunc_1_5
    from t1 as m
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3
    )

    return query_string, table_ref


def mm_occupancy_rate_scaled_bookings(dataset_id):
    """
    -- calculate truncated bookings by excluding those for hotels
    -- flagged as having potential data outages
    """
    table_name = 'mm_occupancy_rate_scaled_bookings'
    table_ref = client.dataset(dataset_id).table(table_name)
    t1 = 'mm_data_partner_hotel_stay_info_total_bookings'
    t2 = 'mm_occupancy_rate_stats'
    t3 = 'search_book'
    t4 = 'mm_occupancy_rate_multipliers'

    query_string = """
    with bookings_truncated as (
        select b.market, b.stay_year, b.stay_month, b.stay_date,
            -- sum up bookings over non-outage hotels only using
            -- flag based on (avg-0.75*stddev)
            sum(
                case 
                    when not ors.outage_std_0_75 or not cumul_outage then total_bookings_target
                    else 0 
                end
            ) as total_bookings_target_0_75,
            -- sum up bookings over non-outage hotels only using
            -- flag based on (avg-1.0*stddev)
            sum(
                case 
                    when not ors.outage_std_1_0 or not cumul_outage then total_bookings_target
                    else 0 
                end
            ) as total_bookings_target_1_0,
            -- sum up bookings over non-outage hotels only using
            -- flag based on (avg-1.5*stddev)
            sum(
                case 
                    when not ors.outage_std_1_5 or not cumul_outage then total_bookings_target
                    else 0 
                end
            ) as total_bookings_target_1_5
        from `{project_id}.{dataset_id}.{t1}` as b
        join `{project_id}.{dataset_id}.{t2}` as ors
            on b.market = ors.market
                and b.dp_id = ors.dp_id
                and b.hotel_code = ors.hotel_code
                and b.stay_year = ors.stay_year
                and b.stay_month = ors.stay_month
        group by b.market, b.stay_year, b.stay_month, b.stay_date
    )
    select b.market,
        b.stay_year, b.stay_month, b.stay_date,
        -- different variations of multipliers
        m.mult_var,
        m.mult_var_trunc_0_75,
        m.mult_var_trunc_1_0,
        m.mult_var_trunc_1_5,
        -- different variations of actual & predicted bookings
        b.total_bookings_target_0_75,
        b.total_bookings_target_1_0,
        b.total_bookings_target_1_5,
        -- actuals scaled by fixed multiplier, all training hotels
        -- actuals scaled by monthly multiplier, all training hotels
        -- actuals scaled by monthly multiplier, excluding outages
        round(m.mult_var_trunc_0_75 * b.total_bookings_target_0_75, 3) as target_v_trunc_0_75,
        -- actuals scaled by monthly multiplier, excluding outages
        round(m.mult_var_trunc_1_0 * b.total_bookings_target_1_0, 3) as target_v_trunc_1_0
        -- actuals scaled by monthly multiplier, excluding outages
        -- predictions scaled by fixed multiplier, all training hotels
        -- predictions scaled by monthly multiplier, all training hotels
    from bookings_truncated as b
    join `{project_id}.{dataset_id}.{t4}` as m
        on b.market = m.market
            and b.stay_year = m.stay_year
            and b.stay_month = m.stay_month
    """.format(
        project_id=project_id,
        dataset_id=dataset_id,
        t1=t1,
        t2=t2,
        t3=t3,
        t4=t4
    )

    return query_string, table_ref
