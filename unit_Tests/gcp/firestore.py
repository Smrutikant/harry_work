import logging

import firebase_admin
from firebase_admin import firestore

logger = logging.getLogger(__name__)

firebase_admin.initialize_app()

db = firestore.client()


class Account(object):
    collection_name = u'accounts'
    jobs_sub_col_name = u'jobs'

    def __init__(self, name, doc_id=None):
        self.doc_id = doc_id
        self.name = name

    @staticmethod
    def from_dict(source, doc_id=None):
        account = Account(source[u'name'], doc_id=doc_id)
        return account

    def to_dict(self):
        return {
            u'name': self.name,
        }

    def __repr__(self):
        return u'Account(name={})'.format(self.name)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id


class Job(object):
    collection_name = u'jobs'

    def __init__(self, name, status, run_day_of_week, doc_id=None, acc_id=None):
        self.doc_id = doc_id
        self.acc_id = acc_id
        self.name = name
        self.status = status
        self.run_day_of_week = run_day_of_week

    @staticmethod
    def from_dict(source, doc_id=None):
        job = Job(source[u'name'],
                  source[u'status'],
                  source[u'run_day_of_week'],
                  doc_id=doc_id)
        return job

    def to_dict(self):
        dest = {
            u'name': self.name,
            u'status': self.status,
            u'run_day_of_week': self.run_day_of_week,
            u'acc_id': self.acc_id,
        }
        return dest

    def __repr__(self):
        return u'Job(name={}, status={}, run_day_of_week={})'.format(
            self.name, self.status, self.run_day_of_week)

    def set_doc_id(self, doc_id):
        self.doc_id = doc_id

    def get_doc_id(self):
        return self.doc_id

    def set_account_id(self, account_id):
        self.acc_id = account_id

    def get_account_id(self):
        return self.acc_id


def fetch_jobs():
    """
    Function which connects to firebase and extracts the account jobs which are active

    :return: A list of the jobs that are active and candidates for running
    """

    logger.info(u'Fetching Active Market Monitor Jobs...')
    jobs = []

    # Fetch all accounts
    logger.info(Account.collection_name)

    # acc_docs -> generator for all the documents inside the 'accounts' collection
    acc_docs = db.collection(Account.collection_name).get()

    for acc_doc in acc_docs:
        # traverse through acc_docs - acc_doc is a class
        # -acc_doc.to_dict() gives us all the info for the doc
        # -example {u'status': u'A', u'id': u'245', u'name': u'Visit Tampa Bay'}

        # fetch active jobs within each account
        # -going deeper to get the collection 'jobs' for that document
        jobs_sub_collection = acc_doc.reference.collection(Account.jobs_sub_col_name)

        # Get only active (A) jobs
        # active_job_docs -> generator for all the documents inside that collection
        active_job_docs = jobs_sub_collection.where(u'status', u'==', u'A').get()

        for job_doc in active_job_docs:
            # -example {u'status': u'A', u'run_day_of_week': 0L, u'name': u'test'}
            job = Job.from_dict(job_doc.to_dict(), doc_id=job_doc.id)

            # attach reference to parent account to the job object
            job.set_account_id(acc_doc.id)

            jobs.append(job.to_dict())
            logger.info(u'%s => %s', job_doc.id, job.to_dict())

    return jobs
