import gzip
import io
import logging
import math

from google.cloud import bigquery
from google.cloud import storage

from market_monitor import settings
from .pubsub import publish_search_book, publish_origin_market

logger = logging.getLogger(__name__)

# Get global variables
client = settings.get_client()
project_id = settings.project_id
output_sb_bucket_id = settings.output_sb_bucket_id
sb_table_id = settings.sb_table_id
output_om_bucket_id = settings.output_om_bucket_id
om_table_id = settings.om_table_id

# Max number of files GCS can merge at a time
max_num_merge_file = 32


def get_files_with_name_format(prefix, bucket):
    """
    Function to return from a GCS bucket a list of blobs which represent the files with a specific prefix

    :param prefix: prefix for the returned files
    :param bucket: bucket where we search for the files
    :return: a list of blobs
    """

    logger.info(u'Getting files from bucket with specific format...')

    blobs = bucket.list_blobs(prefix=prefix)

    list_blobs = list(blobs)
    # list_blobs looks like:
    # [<Blob: mmsbm-qa, input/acct_382__ts_20190607t223814-000000000000.csv.gz>,
    #   <Blob: mmsbm-qa, input/acct_382__ts_20190607t223814-000000000001.csv.gz>, ...]

    return list_blobs


def delete_files(bucket, del_prefix, header_blob_name):
    logger.info(u'Deleting chunks and header file...')

    # delete chunks
    blobs = bucket.list_blobs(prefix=del_prefix)
    for blob in blobs:
        blob.delete()

    # delete header file
    blob = bucket.blob(header_blob_name)
    blob.delete()


def merge_files_on_gcs_driver(chunk_number, dataset_id, bucket):
    """
    Function which checks if one or multiple rounds of merging are needed and calls the appropriate functions

    :param chunk_number: the chunk number that GCS has initially exported the data
    :param dataset_id: dataset ID for which we run
    :param bucket: bucket where we have the exported data and where we will do the merging
    :return: a list of file names
    """

    logger.info(u'Determine how many merge rounds should be done for account {}...'.format(dataset_id))

    temp_name_list = []
    round = 1

    # we check equality because in the end we need to merge with the header info,
    # so we need to have after this step (max_num_merge_file - 1) files
    if chunk_number >= max_num_merge_file:

        while chunk_number >= max_num_merge_file:
            logger.info(u'Chunk_number = {}'.format(chunk_number))

            if round == 1:
                # first merge round - files have names created by GCS
                prefix = 'input/{}'.format(dataset_id)
                files_to_merge = get_files_with_name_format(prefix, bucket)
                files_to_merge.sort()
            else:
                # 2+ merge round - file names have round to determine their origin
                prefix = 'input/{}-round_{}'.format(dataset_id, str(round - 1))
                files_to_merge = get_files_with_name_format(prefix, bucket)
                files_to_merge.sort()

            # chunk_number DIV max_num_merge_file
            for i in range(chunk_number // max_num_merge_file):
                temp_name_list.clear()

                # ex.: getting files 0-31, 32-63, ...
                temp_name_list = files_to_merge[(max_num_merge_file * i): (max_num_merge_file * (i + 1))]

                merged_file_name = 'input/{}-round_{}_{}.csv.gz'.format(dataset_id, str(round), i)

                # merge_files_on_gcs(temp_name_list, merged_file_name, dataset_id)
                # create new blob for merging
                merged_blob = bucket.blob(merged_file_name)
                merged_blob.content_type = 'application/octet-stream'

                # merge blobs
                merged_blob.compose(temp_name_list)

                merged_destination_uri = 'gs://{}/{}'.format(output_sb_bucket_id, merged_file_name)
                logger.info(u'Exported {}:{}.{} to {}'.
                            format(project_id, dataset_id, sb_table_id, merged_destination_uri))

            temp_name_list.clear()

            # we need to take also the files from (chunk_number MOD max_num_merge_file)
            temp_name_list = files_to_merge[(max_num_merge_file * (chunk_number // max_num_merge_file)): chunk_number]

            if temp_name_list:
                merged_file_name = 'input/{}-round_{}_{}.csv.gz'.format(dataset_id, str(round),
                                                                        (chunk_number // max_num_merge_file))

                # merge_files_on_gcs(temp_name_list, merged_file_name, dataset_id)
                # create new blob for merging
                merged_blob = bucket.blob(merged_file_name)
                merged_blob.content_type = 'application/octet-stream'

                # merge blobs
                merged_blob.compose(temp_name_list)

                merged_destination_uri = 'gs://{}/{}'.format(output_sb_bucket_id, merged_file_name)
                logger.info(u'Exported {}:{}.{} to {}'.
                            format(project_id, dataset_id, sb_table_id, merged_destination_uri))

            # determine new file number we need to merge
            chunk_number = math.ceil(chunk_number / max_num_merge_file)
            round += 1

        logger.info(u'Final chunk_number = {}'.format(chunk_number))

    else:
        # no multiple rounds for merging
        logger.info(u'Simple merging for {}'.format(dataset_id))

    if round == 1:
        # we did NOT have multiple merging
        prefix = 'input/{}'.format(dataset_id)
    else:
        # we had multiple merging
        prefix = 'input/{}-round_{}'.format(dataset_id, str(round - 1))
    files_to_merge = get_files_with_name_format(prefix, bucket)
    return files_to_merge


def export_sb_to_gcs(job_details):
    """
    Function to export the final results of the search book query

    :param job_details: dict with information related of the run for which we will export the results
    :return: No return value
            After the end of this function we will have a csv.gz with the search book results
    """

    logger.info(u'Exporting sb data to GCS for account {}...'.format(job_details['dataset_id']))

    # id of the dataset where the to_be exported table is
    dataset_id = job_details['dataset_id']

    # Extract table to chunk files
    logger.info(u'Exporting table to chunk files...')
    destination_uri = 'gs://{}/input/{}-*.csv.gz'.format(output_sb_bucket_id, dataset_id)

    dataset_ref = client.dataset(dataset_id, project=project_id)

    table_ref = dataset_ref.table(sb_table_id)

    # prepare configurations for extraction
    job_config = bigquery.job.ExtractJobConfig()
    job_config.compression = bigquery.Compression.GZIP
    job_config.field_delimiter = '|'
    job_config.print_header = False

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        # Location must match that of the source table
        location='US',
        job_config=job_config
    )  # API request

    extract_job.result()  # Waits for job to complete
    chunk_number = extract_job.destination_uri_file_counts  # value in form of list
    logger.info(u'Search book data exported in {} chunks.'.format(chunk_number))

    storage_client = storage.Client()

    bucket = storage_client.get_bucket(output_sb_bucket_id)  # API request

    # Determine the data files will be merged
    files_to_merge = merge_files_on_gcs_driver(chunk_number[0], dataset_id, bucket)

    # Create GZIP file with column header names
    logger.info(u'Creating GZIP file with column header names...')

    table = client.get_table(table_ref)  # API Request

    col_names = ["{}".format(schema.name) for schema in table.schema]

    csv_header = '|'.join(col_names)
    csv_header = csv_header + '\n'

    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode='w') as fo:
        fo.write(csv_header.encode())

    bytes_obj = out.getvalue()

    header_blob_name = 'input/{}_{}_header.csv.gz'.format(dataset_id, sb_table_id)
    blob = bucket.blob(header_blob_name)

    blob.upload_from_string(
        data=bytes_obj,
        content_type='application/octet-stream'
    )

    # Merge chunks and header files
    logger.info(u'Merging chunks and header files...')
    files_to_merge.insert(0, blob)

    merged_file_name = 'input/{}.csv.gz'.format(dataset_id)

    # create new blob for merging
    merged_blob = bucket.blob(merged_file_name)
    merged_blob.content_type = 'application/octet-stream'

    # merge blobs
    merged_blob.compose(files_to_merge)

    merged_destination_uri = 'gs://{}/{}'.format(output_sb_bucket_id, merged_file_name)
    logger.info(u'Exported {}:{}.{} to {}'.format(project_id, dataset_id, sb_table_id, merged_destination_uri))

    # Pub/sub msg
    publish_search_book(gcs_path=merged_destination_uri,
                        acc_id=job_details['acc_id'],
                        run_ts=job_details['run_ts'],
                        dataset_id=job_details['dataset_id'],
                        pacing_table_id='mm_pacing_alert_inputs'
                        )

    # Delete files created and not needed more
    # logger.info(u'Deleting chunks and header file...')

    # delete chunks
    del_prefix = 'input/{}-'.format(dataset_id)
    # blobs = bucket.list_blobs(prefix=del_prefix)
    # for blob in blobs:
    #     blob.delete()
    #
    # # delete header file
    # blob = bucket.blob(header_blob_name)
    # blob.delete()

    delete_files(bucket, del_prefix, header_blob_name)


def export_om_to_gcs(job_details):
    """
    Function to export the final results of the origin market query

    :param job_details: dict with information related of the run for which we will export the results
    :return: No return value
            After the end of this function we will have a csv with the origin market results
    """

    logger.info(u'Exporting om data to GCS for account {}...'.format(job_details['dataset_id']))

    # id of the dataset where the to_be exported table is
    dataset_id = job_details['dataset_id']

    # extract table to file
    logger.info(u'Exporting table...')
    destination_uri = 'gs://{}/input/{}.csv'.format(output_om_bucket_id, dataset_id)

    dataset_ref = client.dataset(dataset_id, project=project_id)

    table_ref = dataset_ref.table(om_table_id)

    # prepare configurations for extraction
    job_config = bigquery.job.ExtractJobConfig()
    job_config.field_delimiter = '|'
    job_config.print_header = True

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        # Location must match that of the source table
        location='US',
        job_config=job_config
    )  # API request

    extract_job.result()  # Waits for job to complete

    logger.info(u'Exported {}:{}.{} to {}'.format(project_id, dataset_id, om_table_id, destination_uri))

    # pub/sub msg
    publish_origin_market(gcs_path=destination_uri,
                          acc_id=job_details['acc_id'])
