import logging
from datetime import timedelta, datetime

from google.cloud import pubsub

from market_monitor import settings

max_date_format = '%Y-%m-%d'

# Get global variables
project_id = settings.project_id
sb_topic_id = settings.sb_topic_id
om_topic_id = settings.om_topic_id
output_sb_bucket_id = settings.output_sb_bucket_id

logger = logging.getLogger(__name__)

search_book_topic = 'projects/{project_id}/topics/{topic}'.format(
    project_id=project_id,
    topic=sb_topic_id
)

origin_market_topic = 'projects/{project_id}/topics/{topic}'.format(
    project_id=project_id,
    topic=om_topic_id
)


def get_publisher_client():
    return pubsub.PublisherClient()


def publish_search_book(gcs_path, acc_id, run_ts, dataset_id, pacing_table_id):
    """
    Function to pub/sub msg for the completion of the search book to kick off the prediction model

    :param gcs_path: path where the search book csv.gz is stored
    :param acc_id: ID of the account
    :param run_ts: Timestamp of the job run
    :param dataset_id: ID of the new created dataset
    :param pacing_table_id: Name of the final table created by pacing metrics queries
    :return: No return values
            After the end of this function a pub/sub msg will have been sent
    """

    logger.info(u'Publishing sb msg for account {}...'.format(acc_id))

    msg = 'mmsbm-start'

    output_path = 'gs://{}/output/{}.csv'.format(output_sb_bucket_id, dataset_id)

    max_date = datetime.strptime(run_ts, '%Y-%m-%d %H:%M:%S') + timedelta(days=180)

    publisher = get_publisher_client()

    publisher.publish(search_book_topic,
                      msg.encode('utf-8'),
                      INPUT_PATH=gcs_path.encode('utf-8'),
                      NAME=dataset_id.replace('_', '-'),
                      OUTPUT_PATH=output_path.encode('utf-8'),
                      ACCOUNT_ID=acc_id.encode('utf-8'),
                      MAX_DATE=max_date.strftime(max_date_format),
                      DATASET_ID=dataset_id.encode('utf-8'),
                      PACING_TABLE=pacing_table_id
                      )

    logger.info(u'Published message: {} to {}'.format(msg, search_book_topic))


def publish_origin_market(gcs_path, acc_id):
    """
    Function to pub/sub msg for the completion of the origin market part

    :param gcs_path: path where the origin market csv is stored
    :param acc_id:
    :return: No return values
            After the end of this function a pub/sub msg will have been sent
    """

    logger.info(u'Publishing om msg for account {}...'.format(acc_id))

    msg = 'origin-market'

    publisher = get_publisher_client()

    publisher.publish(origin_market_topic,
                      msg.encode('utf-8'),
                      INPUT_PATH=gcs_path.encode('utf-8')
                      )

    logger.info(u'Published message: {} to {}'.format(msg, origin_market_topic))
