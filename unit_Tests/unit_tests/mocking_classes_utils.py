from google.cloud.bigquery.table import Row

from market_monitor import settings


class MockDatasetReference(object):
    def __init__(self, dataset_id):
        self.dataset_id = dataset_id
        self.project = settings.project_id

    def to_api_repr(self):
        """Construct the API resource representation of this dataset reference

        Returns:
            Dict[str, str]: dataset reference represented as an API resource
        """
        return {"projectId": self.project, "datasetId": self.dataset_id}

    def table(self, table_name):
        table_ref = MockTableReference(table_name)
        table_ref.dataset_id = self.dataset_id
        return table_ref


class MockTableReference(object):
    def __init__(self, table_name):
        self.project = settings.project_id
        self.table_id = table_name
        self.dataset_id = ''
        self.path = '/PATH/TO/{}'.format(table_name)

    def to_api_repr(self):
        """Construct the API resource representation of this table reference.

        Returns:
            Dict[str, object]: Table reference represented as an API resource
        """
        return {
            "projectId": self.project,
            "datasetId": self.dataset_id,
            "tableId": self.table_id,
        }


class MockDataset(object):
    def __init__(self, dataset_id):
        self.dataset_id = dataset_id
        self.dataset_ref = MockDatasetReference(dataset_id)
        self.default_table_expiration_ms = 1000


class MockBqClient(object):
    def __init__(self):
        dt_1 = MockDataset('dataset_id_1')
        dt_2 = MockDataset('dataset_id_2')
        dt_3 = MockDataset('dataset_id_3')
        self.datasets = [dt_1, dt_2, dt_3]

    def add_dataset(self, dataset):
        self.datasets.append(dataset)

    def get_client(self):
        return self

    def list_datasets(self):
        return self.datasets

    def dataset(self, dataset_id):
        dt_ref = MockDatasetReference(dataset_id)
        return dt_ref

    def create_dataset(self, dataset):
        gate = False
        for dt in self.datasets:
            if str(dataset.dataset_id) == dt.dataset_id:
                gate = True
        if not gate:
            dt = MockDataset(str(dataset.dataset_id))
            self.add_dataset(dt)
            return dt

    def update_dataset(self, dataset, fields):
        if fields[0] == 'default_table_expiration_ms':
            for dt in self.datasets:
                if str(dataset.dataset_id) == dt.dataset_id:
                    dt.default_table_expiration_ms = dataset.default_table_expiration_ms

    def query(self, query, job_config=None, job_id=None, job_id_prefix=None,
              location=None, project=None, retry='DEFAULT_RETRY'):
        query_job = MockQueryJob(query, client=self, job_config=job_config)
        return query_job


class MockQueryJob(object):
    def __init__(self, query, client, job_config):
        self.query = query
        self.client = client
        self.job_config = job_config

    def result(self):
        if self.query == 'with ':
            return Row((), ())
        else:
            return (Row((0, 'some text - 0'), {'market_id': 0, 'sql_text': 1}),
                    Row((1, 'some text - 1'), {'market_id': 0, 'sql_text': 1}))
