import pytest

from .mocking_classes_utils import MockBqClient
from .mocking_classes_gcp import *

from market_monitor import settings


def test_get_files_with_name_format(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.gcp import pubsub

    pub_client = MockPublisherClient(client)

    monkeypatch.setattr(pubsub, "get_publisher_client", pub_client.get_publisher_client)

    from market_monitor.gcp.storage import get_files_with_name_format

    test_bucket = MockBucket(client, 'test_bucket', settings.project_id)

    test_bucket.add_blob('test_blob')
    test_bucket.add_blob('blob_test')
    test_bucket.add_blob('tests_blob')
    test_bucket.add_blob('testBlob')
    test_bucket.add_blob('blob')

    # test 1
    prefix = 'test'
    ret_blobs = get_files_with_name_format(prefix, test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)
    correct_blobs = ['test_blob', 'tests_blob', 'testBlob']
    assert ret_blobs_names == correct_blobs

    # test 2
    prefix = 'blob'
    ret_blobs = get_files_with_name_format(prefix, test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)
    correct_blobs = ['blob_test', 'blob']
    assert ret_blobs_names == correct_blobs

    # test 3
    prefix = ''
    ret_blobs = get_files_with_name_format(prefix, test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)
    correct_blobs = ['test_blob', 'blob_test', 'tests_blob', 'testBlob', 'blob']
    assert ret_blobs_names == correct_blobs

    # test 4
    prefix = 'foo'
    ret_blobs = get_files_with_name_format(prefix, test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)
    assert not ret_blobs_names


def create_test_bucket(test_bucket):
    test_bucket.delete_all_blobs()

    test_bucket.add_blob('input/test_dataset-blob_1')
    test_bucket.add_blob('input/test_dataset-blob_2')
    test_bucket.add_blob('input/test_dataset-blob_3')
    test_bucket.add_blob('input/test_dataset-blob_4')
    test_bucket.add_blob('input/test_dataset-blob_5')
    test_bucket.add_blob('input/test_dataset-blob_6')
    test_bucket.add_blob('input/test_dataset-blob_7')
    test_bucket.add_blob('input/test_dataset-blob_8')


def test_merge_files_on_gcs_driver(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.gcp import pubsub

    pub_client = MockPublisherClient(client)

    monkeypatch.setattr(pubsub, "get_publisher_client", pub_client.get_publisher_client)

    test_bucket = MockBucket(client, 'test_bucket', settings.project_id)

    from market_monitor.gcp import storage

    # test 1
    monkeypatch.setattr(storage, "max_num_merge_file", 4)

    create_test_bucket(test_bucket)

    ret_blobs = storage.merge_files_on_gcs_driver(8, 'test_dataset', test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)

    correct_blobs = ['input/test_dataset-round_1_0.csv.gz', 'input/test_dataset-round_1_1.csv.gz']
    assert ret_blobs_names == correct_blobs

    # test 2
    monkeypatch.setattr(storage, "max_num_merge_file", 2)

    create_test_bucket(test_bucket)

    ret_blobs = storage.merge_files_on_gcs_driver(8, 'test_dataset', test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)

    correct_blobs = ['input/test_dataset-round_3_0.csv.gz']
    assert ret_blobs_names == correct_blobs

    # test 3
    monkeypatch.setattr(storage, "max_num_merge_file", 10)

    create_test_bucket(test_bucket)

    ret_blobs = storage.merge_files_on_gcs_driver(8, 'test_dataset', test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)

    correct_blobs = ['input/test_dataset-blob_1', 'input/test_dataset-blob_2', 'input/test_dataset-blob_3',
                     'input/test_dataset-blob_4', 'input/test_dataset-blob_5', 'input/test_dataset-blob_6',
                     'input/test_dataset-blob_7', 'input/test_dataset-blob_8']
    assert ret_blobs_names == correct_blobs

    # test 4
    monkeypatch.setattr(storage, "max_num_merge_file", 3)

    create_test_bucket(test_bucket)

    ret_blobs = storage.merge_files_on_gcs_driver(8, 'test_dataset', test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)

    correct_blobs = ['input/test_dataset-round_2_0.csv.gz']
    assert ret_blobs_names == correct_blobs

    # test 5
    monkeypatch.setattr(storage, "max_num_merge_file", 8)

    test_bucket.delete_all_blobs()

    ret_blobs = storage.merge_files_on_gcs_driver(0, 'test_dataset', test_bucket)
    ret_blobs_names = []
    for blob in ret_blobs:
        ret_blobs_names.append(blob.name)

    assert not ret_blobs_names


def test_delete_files(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.gcp import pubsub

    pub_client = MockPublisherClient(client)

    monkeypatch.setattr(pubsub, "get_publisher_client", pub_client.get_publisher_client)

    test_bucket = MockBucket(client, 'test_bucket', settings.project_id)

    create_test_bucket(test_bucket)

    header_blob_name = 'input/header'

    test_bucket.add_blob(header_blob_name)

    from market_monitor.gcp.storage import delete_files

    del_prefix = 'input/test_dataset'

    delete_files(test_bucket, del_prefix, header_blob_name)

    assert not test_bucket.blobs
