import pytest
from .mocking_classes_utils import *

from datetime import datetime, timedelta

from market_monitor import settings


def test_get_client(monkeypatch):
    monkeypatch.setattr(settings, "get_client", lambda: 'asdf')

    assert settings.get_client() == 'asdf'


def test_dataset_exists(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.utils.query_driver import dataset_exists

    assert dataset_exists('dataset_id_2')  # meaning return value is true
    assert not dataset_exists('dataset_id_4')  # meaning return value is false


def test_create_dataset(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.utils.query_driver import create_dataset

    dataset_id, current_ts = create_dataset('0')

    current_ts_2 = datetime.utcnow()
    ts_diff = current_ts_2 - current_ts

    assert current_ts_2 > current_ts
    assert ts_diff < timedelta(seconds=3)

    assert dataset_id == "acct_0__ts_" + current_ts.strftime('%Y%m%dt%H%M%S')


def test_query_with_return(monkeypatch):
    from google.cloud.bigquery.table import Row

    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.utils.query_driver import query_with_return

    # test with a useless query
    query_string = 'with '
    results_list = query_with_return(query_string)

    assert not results_list

    # test with a more "real" query
    query_string = 'select * from some_table'
    results_list = query_with_return(query_string)

    correct_list = [Row((0, 'some text - 0'), {'market_id': 0, 'sql_text': 1}),
                    Row((1, 'some text - 1'), {'market_id': 0, 'sql_text': 1})]

    assert results_list == correct_list


def test_run_bq_queries(monkeypatch):
    client = MockBqClient()

    monkeypatch.setattr(settings, "get_client", client.get_client)

    from market_monitor.utils.query_driver import run_bq_queries

    dataset_id, current_ts = run_bq_queries('0')

    current_ts_2 = datetime.utcnow()
    ts_diff = current_ts_2 - current_ts

    assert current_ts_2 > current_ts
    assert ts_diff < timedelta(seconds=3)

    assert dataset_id == "acct_0__ts_" + current_ts.strftime('%Y%m%dt%H%M%S')
