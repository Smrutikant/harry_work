class MockBucket(object):
    def __init__(self, client, name=None, user_project=None):
        self.client = client
        self.name = str(name)
        self.project = user_project
        self.blobs = []

    def add_blob(self, name):
        blob = MockBlob(name, self)
        self.blobs.append(blob)

    def delete_all_blobs(self):
        self.blobs.clear()

    def list_blobs(self, prefix=''):
        ret_blobs = []
        for blob in self.blobs:
            if blob.name.startswith(prefix):
                ret_blobs.append(blob)
        return iter(ret_blobs)

    def blob(self, file_name):
        blob = MockBlob(file_name, self)
        return blob


class MockBlob(object):
    def __init__(self, name, bucket, content_type=None):
        self.name = str(name)
        self.bucket = bucket
        self.content_type = content_type

    def compose(self, file_name_list):
        if type(file_name_list) is list:
            self.bucket.blobs.append(self)

    def delete(self):
        for blob in self.bucket.blobs:
            if blob.name == self.name:
                self.bucket.blobs.remove(blob)

    def __lt__(self, other):
        return self.name < other.name


class MockPublisherClient(object):
    def __init__(self, client):
        self.client = client

    def get_publisher_client(self):
        return self
