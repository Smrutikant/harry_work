# Market Monitor Unit tests

### General

- use of Python 3
- use of pytest
- monkeypatch for monking functions
- all unit tests under unit_tests folder


### Setup

Before running the current unit tests, environment variables should be set. To do so, the following commands should be executed:

```python
export PROJECT_ID='PROJECT_ID_NAME'
export DATASET_ID_MM_TABLES='DATASET_ID_MM_TABLES_NAME'
export PROJECT_ID_DATAMASTER='PROJECT_ID_DATAMASTER_NAME'
export DATASET_ID_DATAMASTER='DATASET_ID_DATAMASTER_NAME'
export OUTPUT_SB_BUCKET_ID='OUTPUT_SB_BUCKET_ID_NAME'
export SB_TABLE_ID='SB_TABLE_ID_NAME'
export OUTPUT_OM_BUCKET_ID='OUTPUT_OM_BUCKET_ID_NAME'
export OM_TABLE_ID='OM_TABLE_ID_NAME'
export SB_TOPIC_ID='SB_TOPIC_ID_NAME'
export OM_TOPIC_ID='OM_TOPIC_ID_NAME'
```

Which environment variables should be defined, can be found also at the yaml files

The values of the environment variables does not matter.

Also, the following libraries should be installed:
```python
pip install google-cloud
pip install firebase-admin
```

For pytest and pytest coverage:
```python
pip install pytest
pip install pytest-cov
```


### Running

After making the above setup, execute one of the following:

- For simple execution
```python
pytest -p no::cacheprovider
```

- For provide code coverage report
```python
pytest --cov-report term-missing --cov market_monitor/
```


### Code Coverage

Current code coverage: 93%
