import logging

from airflow import utils as airflow_utils
from airflow.models import DAG
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.latest_only_operator import LatestOnlyOperator

from market_monitor.utils.query_driver import run_bq_queries

from market_monitor.gcp.firestore import fetch_jobs
from market_monitor.gcp.storage import export_sb_to_gcs, export_om_to_gcs


DAG_NAME = 'scheduler'
fetch_jobs_task_id = 'fetch_jobs'
filter_jobs_run_date_task_id = 'filter_jobs_run_date'
run_if_has_active_job_task_id = 'run_if_has_active_job'
no_active_task_id = 'notify_no_active'
accounts_run_task_id = 'run_queries_on_bq'


logger = logging.getLogger(DAG_NAME)


owners = [
    'harry.chasparis@adara.com',
    # 'shuo.yang@adara.com',
    # 'howard.wu@adara.com',
    # 'yifan.wu@adara.com'
]


args = {
    'email': ','.join(owners),
    'owner': ','.join(owners),
    'start_date': airflow_utils.dates.days_ago(10),
    'provide_context': True,
    'email_on_failure': True
}


mm_dag = DAG(
    dag_id=DAG_NAME,
    description='Market Monitor - Job Executor',
    default_args=args,
    schedule_interval='30 0 * * *',  # every day at 00:30
    catchup=False
)


def filter_jobs_run_date(**kwargs):
    logger.info(u'Determining which jobs should run today...')

    ti = kwargs['ti']

    active_jobs = ti.xcom_pull(task_ids=fetch_jobs_task_id)

    execution_date = ti.execution_date
    day_of_week = execution_date.isoweekday() % 7  # so sunday=0, monday=1, etc...

    # create list with the jobs need to be run
    jobs_to_run = list(filter(lambda job: job.get("run_day_of_week") == day_of_week, active_jobs))

    return jobs_to_run


def run_if_has_active_job_callable(**kwargs):
    ti = kwargs['ti']

    active_jobs = ti.xcom_pull(task_ids=filter_jobs_run_date_task_id)

    if len(active_jobs) > 0:
        logger.info(u'Having active job to execute...')
        return accounts_run_task_id
    else:
        logger.info(u'No active job to execute...')
        return no_active_task_id


def accounts_run_callable(**kwargs):
    logger.info(u'Producing results for each account...')

    ti = kwargs['ti']

    jobs = ti.xcom_pull(task_ids=filter_jobs_run_date_task_id)

    job_details = {}
    for job in jobs:
        acc_id = job.get("acc_id")

        dataset_id, current_ts = run_bq_queries(acc_id)

        job_details = {
            'name': job.get("name"),
            'acc_id': job.get("acc_id"),
            'dataset_id': dataset_id,
            'run_ts': current_ts.strftime('%Y-%m-%d %H:%M:%S')
        }

        export_sb_to_gcs(job_details)

        export_om_to_gcs(job_details)

    return job_details


# Skip all the previous days
latest_only = LatestOnlyOperator(task_id='latest_only', dag=mm_dag)


fetch_jobs_op = PythonOperator(
    task_id=fetch_jobs_task_id,
    dag=mm_dag,
    provide_context=False,
    python_callable=fetch_jobs
)

filter_jobs_run_date_op = PythonOperator(
    task_id=filter_jobs_run_date_task_id,
    dag=mm_dag,
    python_callable=filter_jobs_run_date
)

run_if_has_active_job = BranchPythonOperator(
    task_id=run_if_has_active_job_task_id,
    dag=mm_dag,
    python_callable=run_if_has_active_job_callable
)

accounts_run = PythonOperator(
    task_id=accounts_run_task_id,
    dag=mm_dag,
    python_callable=accounts_run_callable
)

email_no_active = EmailOperator(
    task_id=no_active_task_id,
    to=args['email'],
    subject='Market Monitor - Cloud Composer - Airflow DAG skipped',
    html_content='Market Monitor Cloud Composer Airflow DAG was skipped. No active job to execute.',
    dag=mm_dag
)

email_success = EmailOperator(
    task_id='notify_success',
    to=args['email'],
    subject='Market Monitor - Cloud Composer - Airflow DAG finish',
    html_content='Market Monitor Airflow DAG has finished executing.',
    dag=mm_dag
)


latest_only >> fetch_jobs_op >> filter_jobs_run_date_op >> run_if_has_active_job

run_if_has_active_job.set_downstream([accounts_run, email_no_active])

accounts_run >> email_success
