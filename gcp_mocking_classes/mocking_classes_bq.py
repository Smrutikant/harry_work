import pandas as pd

from google.cloud import bigquery
from google.cloud.bigquery.table import Row


class MockBqClient(object):
    def __init__(self, project=None):
        self.project = project
        self.dataset_refs = []

    def get_client(self):
        return self

    def dataset(self, dataset_id):
        dt_ref = MockDatasetReference(project=self.project, dataset_id=dataset_id)
        self.dataset_refs.append(dt_ref)
        return dt_ref

    def get_table(self, table_ref):
        print(self.dataset_refs)
        for dt in self.dataset_refs:
            print("dt = {}".format(dt))
            if table_ref in dt.tables_ref:
                return dt.get_table(table_ref)

    def list_rows(self, table):
        return table.list_rows()

    def create_dataset(self, dataset):
        for dt in self.dataset_refs:
            if str(dataset.dataset_id) == dt.dataset_id:
                return dataset
        self.add_dataset(dataset.dataset_ref)
        return dataset

    def add_dataset(self, dataset_ref):
        self.dataset_refs.append(dataset_ref)

    def list_datasets(self):
        return self.dataset_refs

    def update_dataset(self, dataset, fields):
        if fields[0] == 'default_table_expiration_ms':
            for dt in self.dataset_refs:
                if str(dataset.dataset_id) == dt.dataset_id:
                    dt.dataset.default_table_expiration_ms = dataset.default_table_expiration_ms

    def query(self, query, job_config=None, job_id=None, job_id_prefix=None,
              location=None, project=None, retry='DEFAULT_RETRY'):
        query_job = MockQueryJob(query, client=self, job_config=job_config)
        return query_job


class MockDatasetReference(object):
    def __init__(self, project, dataset_id):
        self.dataset_id = dataset_id
        self.project = project
        self.tables_ref = []
        self.dataset = MockDataset(self)

    def to_api_repr(self):
        """Construct the API resource representation of this dataset reference

        Returns:
            Dict[str, str]: dataset reference represented as an API resource
        """
        return {"projectId": self.project, "datasetId": self.dataset_id}

    def table(self, table_name):
        table_ref = MockTableReference(table_name=table_name, project=self.project)
        table_ref.dataset_id = self.dataset_id
        self.tables_ref.append(table_ref)
        return table_ref

    def get_table(self, table_ref):
        for tb in self.tables_ref:
            if tb.table_id == table_ref.table_id:
                return tb.table


class MockDataset(object):
    def __init__(self, dataset_ref):
        self.dataset_id = dataset_ref.dataset_id
        self.default_table_expiration_ms = 1000
        self.location = None
        self.dataset_ref = dataset_ref


class MockTableReference(object):
    def __init__(self, table_name, project):
        self.project = project
        self.table_id = table_name
        self.dataset_id = ''
        self.path = '/PATH/TO/{}'.format(table_name)
        self.table = MockTable(self, table_name)

    def to_api_repr(self):
        """Construct the API resource representation of this table reference.

        Returns:
            Dict[str, object]: Table reference represented as an API resource
        """
        return {
            "projectId": self.project,
            "datasetId": self.dataset_id,
            "tableId": self.table_id,
        }


class MockTable(object):
    def __init__(self, table_ref, table_name):
        self.table_ref = table_ref
        self.table_name = table_name
        schema = [
            bigquery.SchemaField('col_1', 'INTEGER', mode='REQUIRED'),
            bigquery.SchemaField('col_2', 'INTEGER', mode='REQUIRED'),
        ]
        self.data = MockTableData(schema=schema, context=[])

    def list_rows(self):
        return self.data


class MockTableData(object):
    def __init__(self, schema, context=[]):
        self.schema = schema
        self.context = context

    def to_dataframe(self):
        return pd.DataFrame(self.context)


class MockQueryJob(object):
    def __init__(self, query, client, job_config):
        self.query = query
        self.client = client
        self.job_config = job_config

    def result(self):
        if self.query == 'with ':
            return Row((), ())
        else:
            return (Row((0, 'some text - 0'), {'market_id': 0, 'sql_text': 1}),
                    Row((1, 'some text - 1'), {'market_id': 0, 'sql_text': 1}))