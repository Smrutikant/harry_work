import pytest

import uuid


class MockPublisherClient(object):
    def __init__(self, client=None):
        self.client = client

    def get_client(self):
        return self

    def publish(self, topic, data, **attrs):
        if (isinstance(topic, str)) and (isinstance(data, bytes)) and (attrs is not None):
            response = MockPubSubFuture()
            return response
        else:
            return False


class MockPubSubFuture(object):
    _SENTINEL = uuid.uuid4()

    def __init__(self):
        self._result = self._SENTINEL
        self._exception = self._SENTINEL
        self._callbacks = []
        self._completed = False

    def done(self):
        return self._exception != self._SENTINEL or self._result != self._SENTINEL
