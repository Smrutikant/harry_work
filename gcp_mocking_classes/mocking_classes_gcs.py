class MockStorageClient(object):
    def __init__(self, client=None):
        self.client = client
        self.buckets = []

    def get_client(self):
        return self

    def get_bucket(self, bucket_name):
        for bct in self.buckets:
            if bucket_name == bct.bucket_name:
                return bct

        # have not found bucket with the given name
        bct = MockStorageBucket(client=self.client, bucket_name=bucket_name)
        self.buckets.append(bct)
        return bct


class MockStorageBucket(object):
    def __init__(self, client, bucket_name=None, user_project=None):
        self.client = client
        self.bucket_name = bucket_name
        self.project = user_project
        self.blobs = []

    def add_blob(self, name):
        blob = MockBlob(name, self)
        self.blobs.append(blob)

    def delete_all_blobs(self):
        self.blobs.clear()

    def list_blobs(self, prefix=''):
        ret_blobs = []
        for blob in self.blobs:
            if blob.name.startswith(prefix):
                ret_blobs.append(blob)
        return iter(ret_blobs)

    def blob(self, file_path):
        for bl in self.blobs:
            if file_path == bl.name:
                return bl

        # have not found blob with the file path
        bl = MockBlob(name=file_path, bucket=self)
        self.blobs.append(bl)
        return bl


class MockBlob(object):
    def __init__(self, name, bucket, content_type=None):
        self.name = name
        self.bucket = bucket
        self.content_type = content_type

    def compose(self, file_name_list):
        for blob in self.bucket.blobs:
            if blob.name == self.name:
                if type(file_name_list) is list:
                    return True
        return False

    def delete(self):
        for blob in self.bucket.blobs:
            if blob.name == self.name:
                self.bucket.blobs.remove(blob)

    def __lt__(self, other):
        return self.name < other.name

    def download_to_filename(self, dest):
        print('Blob content to destination path {}'.format(dest))
        return True

    def upload_from_filename(self, filename):
        print('File {} content to blob {}'.format(filename, self.name))
        return True
